package net.ihe.gazelle.mca.contentanalyzer.application.analyzers;


import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.McaConfigDao;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.DocType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.EncodedType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.McaConfigDaoStub;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.archives.ZipStructure;
import org.apache.commons.io.FileUtils;
import org.junit.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.*;

public class MessageContentAnalyzerTest {

    MessageContentAnalyzer m;
    McaConfigDao mcaConfigDao;

    @Before
    public void setUp() {

        m = new ApplicationTestFactory().getMessageContentAnalyzer();
        mcaConfigDao = new McaConfigDaoStub();
    }

    @After
    public void tearDown() {
        m = null;
    }

    @Test
    public void rstrWithoutSAMLXMLValid() {
        final File f = loadFile("/contentanalyzer/RSTRWithoutSAMLXMLValid.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }
        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());
        assertEquals("\n" +
                "[XML analysis] : Start analysis as XML sub-part\n" +
//                "[XML Tag Detection] : Envelope tag found in configuration with namespace : http://www.w3.org/2003/05/soap-envelope\n" +
                "[XML Tag Detection] : Detected SOAP Envelope sub-part", analysisPart.getChildPart().get(0).getPartLog());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(982, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());
        
        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getStartOffset());
        assertEquals(981, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(71, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(606, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getDecodedPart());
        
        assertEquals(611, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(965, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("WSTrust", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getDecodedPart());

        assertEquals(630, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(949, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void rstrWithSAMLXMLValid() {
        final File f = loadFile("/contentanalyzer/RSTRWithSAMLXMLValid.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(6648, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(6648, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(71, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(606, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(611, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(6631, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("WSTrust", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDecodedPart());
        assertEquals("\n" +
                "[XML Tag Detection] : RequestSecurityTokenResponseCollection tag found in configuration with namespace : http://docs.oasis-open.org/ws-sx/ws-trust/200512\n" +
                "[XML Tag Detection] : Detected SAML sub-part", analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getPartLog());

        assertEquals(630, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(6615, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());

        assertEquals("SAML", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(0).getDecodedPart());

        assertEquals(2023, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(5639, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getEndOffset());

    }

    @Test
    public void rstWithoutSAMLXMLValid() {
        final File f = loadFile("/contentanalyzer/RSTWithoutSAMLXMLValid.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(4064, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(4063, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(324, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(3078, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(3083, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(4047, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("WSTrust", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getDecodedPart());

        assertEquals(3154, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(4031, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void rstWithSAMLXMLValid() {
        final File f = loadFile("/contentanalyzer/RSTWithSAMLXMLValid.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(7174 , analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(7173 , analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(324, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(3078 , analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(3083, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(7157 , analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("WSTrust", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(3154, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(7141 , analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());

        assertEquals("SAML", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(0).getDecodedPart());

        assertEquals(3718, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(7073 , analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void rstWithSAMLXMLFaultyIndent() {
        final File f = loadFile("/contentanalyzer/FaultySAMLWithNoIndent.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());

        assertEquals("SAML", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getDecodedPart());

        assertEquals(660, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(6127 , analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getEndOffset());

    }

    @Test
    public void validAssertion() {

        final File f = loadFile("/contentanalyzer/validAssertion.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(339, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SAML", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(339, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validAssertionSimple() {

        final File f = loadFile("/contentanalyzer/validAssertionSimple.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(1728, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(40, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1727, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(186, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(756, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SAML", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(374, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(734, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(761, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(1707, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("XDS", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(784, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(1687, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void testWellFormedXml() {

        final File f = loadFile("/contentanalyzer/testWellFormedXml.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(1378, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(40, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1377, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(240, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(441, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(446, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(1357, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("XDS", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(469, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(1337, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());

    }

    @Test
    public void validAuditMessageXmlFile() {

        final File f = loadFile("/contentanalyzer/validAuditMessageXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(15783, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("AUDIT_MESSAGE", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(56, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(15782, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validBase64DicomFile() {

        final File f = loadFile("/contentanalyzer/validBase64DicomFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("BASE64", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());
        assertEquals("\n" +
                "[Base64 Analyzer] : Start analysis on decoded file\n" +
                "[Message Content Analyzer] : Start analysis\n" +
                "[Mime Type Detection] : No mime type detected for this part\n" +
                "[Content Analysis] : Test if document is of type : DICOM\n" +
                "[Content Analysis] : Try to detect byte pattern : [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 68, 73, 67, 77]\n" +
                "[Content Analysis] : Byte pattern detected, continue type detection\n" +
                "[Content Analysis] : All conditions verified, part detected as type : DICOM", analysisPart.getChildPart().get(0).getPartLog());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(4784, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("DICOM", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.B64_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNotNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(4784, analysisPart.getChildPart().get(0).getEndOffset());

    }

    @Test
    public void validBase64File() {

        final File f = loadFile("/contentanalyzer/validBase64File2.txt");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }


        assertEquals("BASE64", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0)
                .getStartOffset());
        assertEquals(40284, analysisPart.getChildPart().get(0)
                .getEndOffset());

        assertEquals("XML", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.B64_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNotNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getStartOffset());
        assertEquals(30212, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getEndOffset());


        assertEquals("CDA", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.B64_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(56, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(30211, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validBase64Hl7File() {

        final File f = loadFile("/contentanalyzer/validBase64Hl7File.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("BASE64", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(242, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("HL7v2", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getDocType());
        assertEquals(EncodedType.B64_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getEncodedType());
        assertNotNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(168, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validBody() {

        final File f = loadFile("/contentanalyzer/validBody.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(947, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(947, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("XDS", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(76, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(931, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validCdaXmlFile() {

        final File f = loadFile("/contentanalyzer/validCdaXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(34320, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("CDA", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(39, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(34352, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

    }

    @Test
    public void validCertificateFile() {

        final File f = loadFile("/contentanalyzer/validCertificateFile.pem");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("Certificate", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(1139, analysisPart.getChildPart().get(0).getEndOffset());
    }

    @Ignore
    @Test
    public void validDicomFile() {

        final File f = loadFile("/contentanalyzer/validDicomFile.dcm");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(new String(FileUtils.readFileToByteArray(f), StandardCharsets.UTF_8).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("DICOM", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(549756, analysisPart.getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validDsubUnsubscribe() {

        final File f = loadFile("/contentanalyzer/validDsubUnsubscribe.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(1668, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(39, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1667, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(153, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(312, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(317, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(1653, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("DSUB", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(334, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(1639, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());

    }

    @Test
    public void validDsubUnsubscribe2() {

        final File f = loadFile("/contentanalyzer/validDsubUnsubscribe2.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(582, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(39, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(581, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(153, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(312, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(317, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(567, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("DSUB", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(334, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(552, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validDsubWithPrefixXmlFile() {

        final File f = loadFile("/contentanalyzer/validDsubWithPrefixXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(1660, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(39, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1659, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(153, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(312, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(317, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(1645, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("DSUB", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(334, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(1631, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validHeader() {

        final File f = loadFile("/contentanalyzer/validHeader.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(294, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(294, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validHeaderWithAssertion() {

        final File f = loadFile("/contentanalyzer/validHeaderWithAssertion.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(675, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(675, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SAML", analysisPart.getChildPart().get(0).getChildPart().get(0).
                getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).
                getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(281, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(657, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validHl7File() {

        final File f = loadFile("/contentanalyzer/validHl7File.hl7");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("HL7v2", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(908, analysisPart.getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validHl7v3MessageIdXmlFile() {

        final File f = loadFile("/contentanalyzer/validHl7v3MessageIdXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(2640, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("HL7V3", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(2639, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validHpdXmlFile() {

        final File f = loadFile("/contentanalyzer/validHpdXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(658, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("HPD", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(657, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validHttpBody() {

        final File f = loadFile("/contentanalyzer/validHttpBody.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }

        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("MTOM", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());
        assertEquals("\n" +
                "[Message Splitting] : MTOM document is splittable\n" +
                "[Message Splitting] : Split MTOM document\n" +
                "[Message Splitting] : Found 2 sub-parts in MTOM part\n" +
                "[Message Splitting] : Launch analysis of MTOM part 1\n" +
                "[Message Splitting] : Launch analysis of MTOM part 2", analysisPart.getChildPart().get(0).getPartLog());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(19038, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 1", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(18706, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("XML", analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(276, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(18706, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(315, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(18703, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDecodedPart());

        assertEquals(457, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(746, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getDecodedPart());

        assertEquals(747, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(18683, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getEndOffset());

        assertEquals("XDS", analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getDecodedPart());

        assertEquals(766, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getStartOffset());
        assertEquals(18667, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 2", analysisPart.getChildPart().get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(18706, analysisPart.getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(18974, analysisPart.getChildPart().get(0).getChildPart().get(1).getEndOffset());
    }

    @Test
    public void validHttpFile() {

        final File f = loadFile("/contentanalyzer/validHttpFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("HTTP", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(19429, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("Http Header", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(391, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("Http Body", analysisPart.getChildPart().get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(391, analysisPart.getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(19429, analysisPart.getChildPart().get(0).getChildPart().get(1).getEndOffset());

        assertEquals("MTOM", analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getDecodedPart());

        assertEquals(391, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getStartOffset());
        assertEquals(19429, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 1", analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(391, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(19097, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("XML", analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDecodedPart());

        assertEquals(667, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(19097, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getDecodedPart());


        assertEquals(706, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(19094, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(848, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1137, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(1138, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(19074, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getEndOffset());

        assertEquals("XDS", analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getDecodedPart());

        assertEquals(1157, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getStartOffset());
        assertEquals(19058, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 2", analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(19097, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(19365, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getEndOffset());
    }

    @Test
    public void validHttpHeader() {
        final File f = loadFile("/contentanalyzer/validHttpHeader.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("HTTP", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(391, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("Http Header", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(391, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validMtomFile() {

        final File f = loadFile("/contentanalyzer/validMtomFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("MTOM", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(6640, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 1", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1561, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 2", analysisPart.getChildPart().get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(1561, analysisPart.getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(6583, analysisPart.getChildPart().get(0).getChildPart().get(1).getEndOffset());

        assertEquals("BASE64", analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getDecodedPart());

        assertEquals(1790, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getStartOffset());
        assertEquals(6583, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getEndOffset());

        assertEquals("DICOM", analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.B64_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNotNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(3210, analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validSvsXmlFile() {

        final File f = loadFile("/contentanalyzer/validSvsXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(177, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SVS", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(176, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validSvsXmlFileNotSplitted() {

        final File f = loadFile("/contentanalyzer/validSvsXmlFileNotSplitted.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(72, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SVS", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(72, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validSyslogFile() {

        final File f = loadFile("/contentanalyzer/validSyslogFile.syslog");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("SYSLOG", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(2169, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("Syslog Header", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(88, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("Syslog Message", analysisPart.getChildPart().get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(91, analysisPart.getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(2169, analysisPart.getChildPart().get(0).getChildPart().get(1).getEndOffset());

        assertEquals("XML", analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(91, analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getStartOffset());
        assertEquals(2169, analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getEndOffset());

        assertEquals("AUDIT_MESSAGE", analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(147, analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getStartOffset());
        assertEquals(2169, analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getEndOffset());
    }

    @Test
    public void validXACMLWithPrefixXmlFile() {

        final File f = loadFile("/contentanalyzer/validXACMLWithPrefixXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(2687, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(2687, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(318, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(795, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(800, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(2670, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("XACML Response", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(1041, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(2653, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validXdsXmlFile() {

        final File f = loadFile("/contentanalyzer/validXdsXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(1000, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1000, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("XDS", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(11, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(992, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());
    }

    @Test
    public void ppqRequestFile() {

        final File f = loadFile("/contentanalyzer/ppq_request.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals("CH:ADR", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());
    }

    @Test
    public void validXdwXmlFile() {

        final File f = loadFile("/contentanalyzer/validXdwXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(6067, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("XDW", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(39, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(6066, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validXmlFile() {

        final File f = loadFile("/contentanalyzer/validXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(1359, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(40, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1358, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(186, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(387, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getDecodedPart());

        assertEquals(392, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(1338, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("XDS", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        Assert.assertEquals(ValidationType.XDS, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getValidationType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getDecodedPart());

        assertEquals(415, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(1318, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void validXmlFileEnveloppe() {

        final File f = loadFile("/contentanalyzer/validXmlEnveloppeFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(1294, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(40, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1294, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(162, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(347, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(352, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(1282, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("XDS", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(367, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(1270, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());

    }

    //Empty Header is just not detected at all.
    @Test
    public void validXmlFileWithEmptyHeader() {

        final File f = loadFile("/contentanalyzer/validXmlFileWithEmptyHeader.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(1127, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(40, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1126, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(207, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getStartOffset());
        assertEquals(1106, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEndOffset());

        assertEquals("XDS", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDecodedPart());

        assertEquals(228, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(1087, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void wellFormedXmlFile() {

        final File f = loadFile("/contentanalyzer/wellFormedXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(4111, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("XDW", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(39, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(4110, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void wsTrustWithAssertion() {

        final File f = loadFile("/contentanalyzer/WS-Trust_withAssertion.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(3536, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("WSTrust", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(3535, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SAML", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(516, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(3483, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());
    }

    @Test
    public void wsTrustWithAssertionBis() {

        final File f = loadFile("/contentanalyzer/WS-Trust_withAssertion_bis.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(6512, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("WSTrust", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(6511, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SAML", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(516, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(3483, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SAML", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(3492, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(6459, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());
    }

    @Test
    public void wsTrustWithTokenRef() {

        final File f = loadFile("/contentanalyzer/WS-Trust_withTokenRef.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(798, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("WSTrust", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(797, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void xmlNamespaces() {

        final File f = loadFile("/contentanalyzer/xmlNamespaces.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(1594, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1594, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(503, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(662, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(667, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(1580, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("DSUB", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDecodedPart());


        assertEquals(684, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(1566, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void xmlWellFormedSlow() {

        final File f = loadFile("/contentanalyzer/xmlWellFormedSlow.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(5507, analysisPart.getChildPart().get(0).getEndOffset());
    }

    @Test
    public void nestedFolderInZipFile() {

        final File f = loadFile("/contentanalyzer/IHE_XDM.zip");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        m.setMessageName("IHE_XDM.zip".getBytes(StandardCharsets.UTF_8));
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }

        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        Assert.assertEquals(ZipStructure.XDM.getDocType(), analysisPart.getChildPart().get(0).getDocType());
    }

    @Ignore
    @Test
    public void zipFile() {

        final File f = loadFile("/contentanalyzer/zipFile.zip");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        m.setMessageName("zipFile.zip".getBytes(StandardCharsets.UTF_8));
        try {
            analysisPart.setEndOffset(new String (FileUtils.readFileToByteArray(f), StandardCharsets.UTF_8).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("ZIP", analysisPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analysisPart.getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getDecodedPart());
        assertTrue(analysisPart.getChildPart().get(0).getPartLog().startsWith("\n" +
                "[Mime Type Detection] : Extraction of files in ZIP archive\n" +
                "[Mime Type Detection] : Add sub-part for folder/\n" +
                "[Mime Type Detection] : Add sub-part for wado.dcm\n" +
                "[Mime Type Detection] : Add sub-part for goodXmlFile.xml\n" +
                "[Mime Type Detection] : Add sub-part for goodHl7File.hl7"));

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(7686988, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals(MimeTypeDetector.FOLDER_PREFIX + "folder/", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.ZIP_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals(MimeTypeDetector.FILE_PREFIX + "folder/validMtomFileEncodedXML.xml", analysisPart.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.ZIP_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEncodedType());
        assertNotNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getStartOffset());
        assertEquals(42140, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getEndOffset());

        assertEquals("MTOM", analysisPart.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.ZIP_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getStartOffset());
        assertEquals(42140, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 1", analysisPart.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.ZIP_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1561, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 2", analysisPart.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.ZIP_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getChildPart().get(1).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(1).getDecodedPart());

        assertEquals(1561, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(42083, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getEndOffset());

        assertEquals(DocType.BASE64.getValue(), analysisPart.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDocType());
        assertEquals(EncodedType.ZIP_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getDecodedPart());

        assertEquals(1790, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(42083, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getEndOffset());

        assertEquals("XML", analysisPart.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(0).getDocType());
        assertEquals(EncodedType.B64_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(0).getEncodedType());
        assertNotNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getStartOffset());
        assertEquals(30212, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getEndOffset());

        assertEquals("CDA", analysisPart.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.B64_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDecodedPart());

        assertEquals(56, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(30211, analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals(MimeTypeDetector.FILE_PREFIX + "wado.dcm", analysisPart.getChildPart().get(0).getChildPart().get(1).getDocType());
        assertEquals(EncodedType.ZIP_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1).getEncodedType());
        assertNotNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(13123008, analysisPart.getChildPart().get(0).getChildPart().get(1).getEndOffset());

        assertEquals("DICOM", analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getDocType());
        assertEquals(EncodedType.ZIP_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getStartOffset());
        assertEquals(13123008, analysisPart.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0)
                .getEndOffset());

        assertEquals(MimeTypeDetector.FILE_PREFIX + "goodHl7File.hl7", analysisPart.getChildPart().get(0)
                .getChildPart().get(3).getDocType());
        assertEquals(EncodedType.ZIP_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(3).getEncodedType());
        assertNotNull(analysisPart.getChildPart().get(0).getChildPart().get(3).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(3).getStartOffset());
        assertEquals(908, analysisPart.getChildPart().get(0).getChildPart().get(3).getEndOffset());

        assertEquals("HL7v2", analysisPart.getChildPart().get(0)
                .getChildPart().get(3).getChildPart().get(0).getDocType());
        assertEquals(EncodedType.ZIP_ENCODED, analysisPart.getChildPart().get(0).getChildPart().get(3).getChildPart().get(0)
                .getEncodedType());
        assertNull(analysisPart.getChildPart().get(0).getChildPart().get(3).getChildPart().get(0).getDecodedPart());

        assertEquals(0, analysisPart.getChildPart().get(0).getChildPart().get(3).getChildPart().get(0)
                .getStartOffset());
        assertEquals(908, analysisPart.getChildPart().get(0).getChildPart().get(3).getChildPart().get(0)
                .getEndOffset());
    }

    @Test
    public void adrAuthorizationPpqRequest() {

        final File f = loadFile("/contentanalyzer/ADR_AUTHORIZATION_PPQ_request.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("CH:ADR", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getDocType());
    }

    @Test
    public void adrXDSRep() {

        final File f = loadFile("/contentanalyzer/ADR_XDS_REP.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XACML Response", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getDocType());
    }

    @Test
    public void adrXDSRequest() {

        final File f = loadFile("/contentanalyzer/ADR_XDS_REQ.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("CH:ADR", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getDocType());
    }

    @Test
    public void adrDueToPPQResponse() {

        final File f = loadFile("/contentanalyzer/CH_ADR_DUE_TO_PPQ_RESPONSE.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XACML Response", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getDocType());
    }

    @Test
    public void ppqRequest() {

        final File f = loadFile("/contentanalyzer/PPQ-Request.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("CH:PPQ-2", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getDocType());
    }

    @Test
    public void ppqRequest2() {

        final File f = loadFile("/contentanalyzer/PPQ-Request2.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("CH:PPQ-1", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getDocType());
    }

    @Test
    public void ppqRequest3() {

        final File f = loadFile("/contentanalyzer/PPQ-Request3.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("CH:PPQ-1", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getDocType());
    }

    @Test
    public void syslogStructuredData() {

        final File f = loadFile("/contentanalyzer/syslogStructuredData.syslog");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("Syslog Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getDocType());
        assertEquals("Syslog Structured Data", analysisPart.getChildPart().get(0).getChildPart().get(1)
                .getDocType());
        assertEquals(89, analysisPart.getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(158, analysisPart.getChildPart().get(0).getChildPart().get(1).getEndOffset());

        assertEquals("Syslog Message", analysisPart.getChildPart().get(0).getChildPart().get(2)
                .getDocType());

        assertEquals(158, analysisPart.getChildPart().get(0).getChildPart().get(2).getStartOffset());
        assertEquals(2236, analysisPart.getChildPart().get(0).getChildPart().get(2).getEndOffset());
    }

    @Test
    public void XmlFileWithUTF8Bom(){

        final File f = loadFile("/contentanalyzer/validXmlFileUtf8BomEncoded.xml");
        final AnalysisPart analyzedObjectPart = new AnalysisPart(new AnalysisPart(null));
        analyzedObjectPart.setDocType("DOCUMENT");
        analyzedObjectPart.setEncodedType(EncodedType.NOT_ENCODED);
        analyzedObjectPart.setStartOffset(0);
        try {
            analyzedObjectPart.setEndOffset(FileUtils.readFileToString(f).length());
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analyzedObjectPart);
        }catch (IOException e){
            fail("Unable to read file");
        }catch (UnexpectedAnalysisException e){
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analyzedObjectPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analyzedObjectPart.getChildPart().get(0).getEncodedType());
        assertNull(analyzedObjectPart.getChildPart().get(0).getDecodedPart());
    }

    @Test
    public void EmbeddedB64Pdf(){

        final File f = loadFile("/contentanalyzer/CR-BIO_2021.01-V1.xml");
        final AnalysisPart analyzedObjectPart = new AnalysisPart(new AnalysisPart(null));
        analyzedObjectPart.setDocType("DOCUMENT");
        analyzedObjectPart.setEncodedType(EncodedType.NOT_ENCODED);
        analyzedObjectPart.setStartOffset(0);
        try {
            analyzedObjectPart.setEndOffset(FileUtils.readFileToString(f).length());
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analyzedObjectPart);
        }catch (IOException e){
            fail("Unable to read file");
        }catch (UnexpectedAnalysisException e){
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analyzedObjectPart.getChildPart().get(0).getDocType());
        assertEquals(EncodedType.NOT_ENCODED, analyzedObjectPart.getChildPart().get(0).getEncodedType());
        assertNull(analyzedObjectPart.getChildPart().get(0).getDecodedPart());
        assertEquals("B64PDF", analyzedObjectPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDocType());
    }
}
