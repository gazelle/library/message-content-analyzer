package net.ihe.gazelle.mca.contentanalyzer.application.analyzers.xml;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.McaConfigDaoStub;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TagDetectorTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(TagDetectorTest.class);

    TagDetector tagDetector;

    @Before
    public void setUp() {
        tagDetector = new TagDetector(new McaConfigDaoStub(),new ApplicationTestFactory().getMessageContentAnalyzer());
    }

    private AnalysisPart getAnalysisPart(File file) {
        final AnalysisPart parent = new AnalysisPart(new AnalysisPart(null));
        parent.setDocType("XML");
        parent.setStartOffset(0);
        try {
            parent.setEndOffset(FileUtils.readFileToString(file).length());
            tagDetector.detectTags(new String(FileUtils.readFileToByteArray(file), StandardCharsets.UTF_8), parent);
        } catch(IOException | UnexpectedAnalysisException e) {
            LOGGER.error("",e);
            fail("No exception to be thrown");
        }
        return parent;
    }

    @Test
    public void singleLineDocumentTest() {
        final File file = loadFile("/contentanalyzer/CDA_single_line.xml");
        final AnalysisPart parent = getAnalysisPart(file);

        assertEquals(1, parent.getChildPart().size());
        assertEquals("CDA", parent.getChildPart().get(0).getDocType());
        assertEquals(ValidationType.CDA, parent.getChildPart().get(0).getValidationType());
        assertEquals(38, parent.getChildPart().get(0).getStartOffset());
        assertEquals(249335, parent.getChildPart().get(0).getEndOffset());
    }

    @Test
    public void detectTagTest() {

        final File file = loadFile("/contentanalyzer/xmlNamespaces.xml");
        final AnalysisPart parent = getAnalysisPart(file);

        assertEquals(1, parent.getChildPart().size());
        assertEquals("DSUB", parent.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        assertEquals(ValidationType.DSUB, parent.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getValidationType());
        assertEquals(684, parent.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(1566, parent.getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getEndOffset());
    }

    @Test
    public void detectTagTestNoTags() {

        final File file = loadFile("/contentanalyzer/xmlWellFormedSlow.xml");
        final AnalysisPart parent = getAnalysisPart(file);

        assertEquals(0, parent.getChildPart().size());
    }

    @Test
    public void detectTagTestNoTagsWithAssertion() {

        final File file = loadFile("/contentanalyzer/validAssertion.xml");
        final AnalysisPart parent = getAnalysisPart(file);

        assertEquals(1, parent.getChildPart().size());
        assertEquals("SAML", parent.getChildPart().get(0).getDocType());
        assertEquals(ValidationType.SAML, parent.getChildPart().get(0).getValidationType());
        assertEquals(0, parent.getChildPart().get(0).getStartOffset());
        assertEquals(339, parent.getChildPart().get(0).getEndOffset());
    }

    @Test
    public void detectTagTestPPQ() {

        final File file = loadFile("/contentanalyzer/ppq_request.xml");
        final AnalysisPart parent = getAnalysisPart(file);

        assertEquals(1, parent.getChildPart().size());
        assertEquals("CH:ADR", parent.getChildPart().get(0).getDocType());
        assertEquals(ValidationType.XML, parent.getChildPart().get(0).getValidationType());
    }

    @Test
    public void detectTagCdaWithLongContentTest() {

        final File file = loadFile("/contentanalyzer/validCdaXmlFileWithLongText.xml");
        final AnalysisPart parent = getAnalysisPart(file);

        assertEquals(1, parent.getChildPart().size());
        assertEquals("CDA", parent.getChildPart().get(0).getDocType());
        assertEquals(ValidationType.CDA, parent.getChildPart().get(0).getValidationType());
        assertEquals(39, parent.getChildPart().get(0).getStartOffset());
        assertEquals(44336, parent.getChildPart().get(0).getEndOffset());
    }
}
