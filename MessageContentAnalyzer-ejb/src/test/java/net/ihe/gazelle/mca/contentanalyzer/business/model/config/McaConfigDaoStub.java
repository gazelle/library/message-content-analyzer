package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.McaConfigDao;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.archives.ZipStructure;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class McaConfigDaoStub implements McaConfigDao {
    private static final long serialVersionUID = -395607461912351155L;

    @Override
    public List<MimeTypeConfigInterface> getAllMimeTypeConfig() {

        ArrayList<MimeTypeConfigInterface> mimeTypeConfigs = new ArrayList<MimeTypeConfigInterface>();
        mimeTypeConfigs.add(new MimeTypeConfigEntity("application/pdf", "PDF", ValidationType.PDF));
        mimeTypeConfigs.add(new MimeTypeConfigEntity("application/zip", "ZIP", null));
        return mimeTypeConfigs;
    }

    public List<MimeTypeConfigEntity> getListOfMimeTypeEntity() {
        ArrayList<MimeTypeConfigEntity> mimeTypeConfigs = new ArrayList<MimeTypeConfigEntity>();
        mimeTypeConfigs.add(new MimeTypeConfigEntity("application/pdf", "PDF", ValidationType.PDF));
        mimeTypeConfigs.add(new MimeTypeConfigEntity("application/zip", "ZIP", null));
        return mimeTypeConfigs;
    }

    @Override
    public List<ContentAnalysisConfigInterface> getAllContentAnalysisConfig() {
        ArrayList<ContentAnalysisConfigInterface> contentAnalysisConfigs = new ArrayList<ContentAnalysisConfigInterface>();
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("DICOM", ValidationType.DICOM, null,
                new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 68, 73, 67, 77}, null, null));
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("HTTP", null,
                Arrays.asList("GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "CONNECT"),
                null, null, null));
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("MTOM", null, null,
                null, "^( )*--([^>].+)", "CERTIFICATE"));
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("HL7v2", ValidationType.HL7V2,
                Arrays.asList("MSH"),null, null, null));
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("SYSLOG", null, Arrays.asList("<"),
                null, "(<[0-9]{2,3}>1 [^<]*)<(\\?xml|AuditMessage)", null));
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("Certificate", ValidationType.TLS,
                Arrays.asList("-----BEGIN CERTIFICATE-----"), null, null, null));
        return contentAnalysisConfigs;
    }

    public List<ContentAnalysisConfigEntity> getListOfContentAnalysisConfigEntity() {
        ArrayList<ContentAnalysisConfigEntity> contentAnalysisConfigs = new ArrayList<ContentAnalysisConfigEntity>();
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("DICOM", ValidationType.DICOM, null,
                new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                        0, 0, 0, 0, 0, 0, 0, 68, 73, 67, 77}, null, null));
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("HTTP", null,
                Arrays.asList("GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "CONNECT"),
                null, null, null));
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("MTOM", null, null,
                null, "^( )*--([^>].+)", "CERTIFICATE"));
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("HL7v2", ValidationType.HL7V2,
                Arrays.asList("MSH"),null, null, null));
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("SYSLOG", null, Arrays.asList("<"),
                null, "(<[0-9]{2,3}>1 [^<]*)<(\\?xml|AuditMessage)", null));
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("Certificate", ValidationType.TLS,
                Arrays.asList("-----BEGIN CERTIFICATE-----"), null, null, null));
        return contentAnalysisConfigs;
    }

    @Override
    public List<XmlTagConfigInterface> getAllTags() {
        return new ArrayList<XmlTagConfigInterface>(Arrays.asList(XmlTagConfigEnum.values()));
    }

    public List<XmlTagConfigEntity> getListOfXmlTagConfigEntity() {

        ArrayList<XmlTagConfigEntity> xmlTagConfigEntities = new ArrayList<>();
        xmlTagConfigEntities.add(new XmlTagConfigEntity("Envelope", "http://www.w3.org/2003/05/soap-envelope","SOAP Envelope", null));
        xmlTagConfigEntities.add(new XmlTagConfigEntity("Body", "http://www.w3.org/2003/05/soap-envelope", "SOAP Body", null));
        xmlTagConfigEntities.add(new XmlTagConfigEntity("Header", "http://www.w3.org/2003/05/soap-envelope", "SOAP Header", null));
        xmlTagConfigEntities.add(new XmlTagConfigEntity("ClinicalDocument", "urn:hl7-org:v3", "CDA", ValidationType.CDA));
        xmlTagConfigEntities.add(new XmlTagConfigEntity("([A-Z]{4}_[A-Z]{2}[0-9]{6})([A-Z]{2}[0-9]{2})", "urn:hl7-org:v3", "HL7V3", ValidationType.HL7V3));
        xmlTagConfigEntities.add(new XmlTagConfigEntity("Subscribe", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB));
        xmlTagConfigEntities.add(new XmlTagConfigEntity("SubscribeResponse", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB));
        xmlTagConfigEntities.add(new XmlTagConfigEntity("Unsubscribe", "http://docs.oasis-open.org/wsn/b-2", "DSUB", ValidationType.DSUB));
        return xmlTagConfigEntities;
    }

    @Override
    public List<MimeTypeConfigInterface> getMimeTypeConfigWithValidationType(ValidationType validationType) {

        ArrayList<MimeTypeConfigInterface> mimeTypeConfigs = new ArrayList<MimeTypeConfigInterface>();
        mimeTypeConfigs.add(new MimeTypeConfigEntity("application/pdf", "PDF", validationType));
        return mimeTypeConfigs;
    }

    @Override
    public List<ContentAnalysisConfigInterface> getContentAnalysisTypeDetectionConfigWithValidationType(ValidationType validationType) {
        ArrayList<ContentAnalysisConfigInterface> contentAnalysisConfigs = new ArrayList<ContentAnalysisConfigInterface>();
        contentAnalysisConfigs.add(new ContentAnalysisConfigEntity("HTTP", validationType,
                Arrays.asList("GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "CONNECT"),
                null, null, null));
        return contentAnalysisConfigs;
    }

    @Override
    public List<XmlTagConfigInterface> getXmlTagsWithValidationType(ValidationType validationType) {

        ArrayList<XmlTagConfigInterface> xmlTagConfigEntities = new ArrayList<XmlTagConfigInterface>();
        xmlTagConfigEntities.add(new XmlTagConfigEntity("Envelope", "http://www.w3.org/2003/05/soap-envelope","SOAP Envelope", validationType));
        return xmlTagConfigEntities;
    }

    @Override
    public List<ZipStructureInterface> getZipStructures() {
        return new ArrayList<ZipStructureInterface>(Arrays.asList(ZipStructure.values()));
    }

    @Override
    public ZipStructureInterface getZipStructureWithDocType(String docType) {
        return ZipStructure.get(docType);
    }

    @Override
    public ConfigsFromDB getConfigsFromDB() {
        ConfigsFromDB configsFromDB = new ConfigsFromDB();
        configsFromDB.setXmlTagConfigEntities(getListOfXmlTagConfigEntity());
        configsFromDB.setContentAnalysisConfigEntities(getListOfContentAnalysisConfigEntity());
        configsFromDB.setMimeTypeConfigEntities(getListOfMimeTypeEntity());
        return configsFromDB;
    }

    @Override
    public int addNewImportedConfiguration(ConfigsFromDB configsToImport, ConfigsFromDB overwrittenConfigs) {
        return 0;
    }

    @Override
    public int removeAllConfigsFromDB() {
        return 0;
    }

    @Override
    public int addAllImportedConfigurationsToDB(ConfigsFromDB configsToImport) {
        return 0;
    }

    @Override
    public void remove(ConfigInterface selectedConfig) {

    }

    @Override
    public void merge(ConfigInterface selectedConfig) {

    }
}
