package net.ihe.gazelle.mca.contentanalyzer.business;

import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.MessageContentAnalyzer;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.FileContentUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.EncodedType;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.*;

public class NamespaceAnalyzeTest {

    private MessageContentAnalyzer m;

    @Before
    public void setUp() {
        m = new ApplicationTestFactory().getMessageContentAnalyzer();
    }

    @Test
    public void testAddMissingNamespaces() {

        final File f = loadFile("/contentanalyzer/xmlNamespaces.xml");

        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }


        try {
            String fileContent = FileUtils.readFileToString(f);
            String fileBodyPart = fileContent.substring(667,1580);
            fileBodyPart = FileContentUtils.xmlAddNamespacesToFileContent(fileBodyPart,
                    analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getNamespaces());
            assertEquals("<s:Body xmlns:s=\"http://www.w3.org/2003/05/soap-envelope\">\n" +
                    "        <wsnt:Subscribe xmlns:wsnt=\"http://docs.oasis-open.org/wsn/b-2\">\n" +
                    "            <wsnt:ConsumerReference>\n" +
                    "                <a:Address xmlns:a=\"http://www.w3.org/2005/08/addressing\">http://abc.com/1123</a:Address>\n" +
                    "            </wsnt:ConsumerReference>\n" +
                    "            <wsnt:Filter>\n" +
                    "                <wsnt:TopicExpression Dialect=\"http://docs.oasis-open.org/wsn/t-1080 1/TopicExpression/Simple\">\n" +
                    "                    ihe:FullDocumentEntry\n" +
                    "                </wsnt:TopicExpression>\n" +
                    "                <rim:AdhocQuery xmlns:rim=\"urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0\" id=\"urn:uuid:aa2332d0-f8fe-11e0-be50-0800200c9a66\">\n" +
                    "                    <rim:Slot name=\"$XDSDocumentEntryPatientId\">\n" +
                    "                        <rim:ValueList>\n" +
                    "                            <rim:Value>'1234'</rim:Value>\n" +
                    "                        </rim:ValueList>\n" +
                    "                    </rim:Slot>\n" +
                    "                </rim:AdhocQuery>\n" +
                    "            </wsnt:Filter>\n" +
                    "            <wsnt:InitialTerminationTime>2014-05-19T16:16:02.7686801+01:00</wsnt:InitialTerminationTime>\n" +
                    "        </wsnt:Subscribe>\n" +
                    "    </s:Body>", fileBodyPart);

        } catch(IOException e) {
            fail("Error while reading the file");
        }
    }

    @Test
    public void testAddMissingNamespacesOnAttribute() {

        final File f = loadFile("/contentanalyzer/PPQ-Request3.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setEncodedType(EncodedType.NOT_ENCODED);
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            m.analyzeMessageContent(FileUtils.readFileToByteArray(f), analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        try {
            String fileContent = FileUtils.readFileToString(f);
            String fileBodyPart = fileContent.substring(324,820);
            fileBodyPart = FileContentUtils.xmlAddNamespacesToFileContent(fileBodyPart,
                    analysisPart.getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getNamespaces());
            assertEquals("<saml:Assertion xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\" Version=\"2.0\" ID=\"_9a71a18c-d7a7-4931-9168-150a8fbc5dc2\" IssueInstant=\"2017-11-16T03:34:53Z\">\n" +
                    "            <saml:Issuer NameQualifier=\"urn:e-health-suisse:community-index\">urn:oid:1.3.6.1.4.1.21367.2017.2.6.2</saml:Issuer>\n" +
                    "            <saml:Statement xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"epr:XACMLPolicySetIdReferenceStatementType\">\n" +
                    "                <xacml:PolicySetIdReference xmlns:xacml=\"urn:oasis:names:tc:xacml:2.0:policy:schema:os\">urn:uuid:356d68d7-44f0-4575-8647-7f4ea1499122</xacml:PolicySetIdReference>\n" +
                    "            </saml:Statement>\n" +
                    "         </saml:Assertion>", fileBodyPart);

        } catch(IOException e) {
            fail("Error while reading the file");
        }
    }
}
