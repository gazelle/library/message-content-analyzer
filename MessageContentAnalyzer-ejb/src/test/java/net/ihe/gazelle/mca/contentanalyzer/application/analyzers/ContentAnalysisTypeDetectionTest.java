package net.ihe.gazelle.mca.contentanalyzer.application.analyzers;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.McaConfigDaoStub;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ContentAnalysisTypeDetectionTest {

    ContentAnalysisTypeDetector contentAnalysisTypeDetector;
    MessageContentAnalyzer m;

    @Before
    public void setUp() {
        contentAnalysisTypeDetector = new ApplicationTestFactory().getContentAnalysisTypeDetector();
        m = new ApplicationTestFactory().getMessageContentAnalyzer();
    }

    @Test
    public void detectTypeTest() {

        final File file = loadFile("/contentanalyzer/validCertificateFile.pem");
        final AnalysisPart parent = new AnalysisPart(new AnalysisPart(null));
        parent.setDocType("DOCUMENT");
        parent.setStartOffset(0);
        try {
            parent.setEndOffset(FileUtils.readFileToString(file).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }

        AnalysisPart child;

        try {
            m.setMessageByteContent(FileUtils.readFileToByteArray(file));
            child = contentAnalysisTypeDetector.detectType(m, parent, new McaConfigDaoStub());
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("Certificate", parent.getChildPart().get(0).getDocType());
        Assert.assertEquals(ValidationType.TLS, parent.getChildPart().get(0).getValidationType());

        assertEquals(0, parent.getChildPart().get(0).getStartOffset());
        assertEquals(1139, parent.getChildPart().get(0).getEndOffset());
    }
}
