package net.ihe.gazelle.mca.contentanalyzer.business.model.config.archives;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FileInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FolderInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ZipStructureInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum ZipStructure implements ZipStructureInterface {

    XDM("XDM ZIP", null, "IHE_XDM.zip", Arrays.asList(new File[]{}), Arrays.asList(Folder.IHE_XDM_1), "1.3.6.1.4.1.12559.11.36.3.3.2");

    private final String docType;
    private final ValidationType validationType;
    private final String archiveName;
    private final List<File> fileChildren;
    private final List<Folder> folderChildren;
    private final String xValidatorOid;

    ZipStructure(final String docType, final ValidationType validationType, final String archiveName, List<File> fileChildren, List<Folder> folderChildren, String xValidatorOid) {
        this.docType = docType;
        this.validationType = validationType;
        this.archiveName = archiveName;
        this.fileChildren = fileChildren;
        this.folderChildren = folderChildren;
        this.xValidatorOid = xValidatorOid;
    }

    public String getDocType() {
        return docType;
    }

    public ValidationType getValidationType() {
        return validationType;
    }

    public String getArchiveName() {
        return archiveName;
    }

    public List<FileInterface> getFileChildren() {
        return new ArrayList<FileInterface>(fileChildren);
    }

    public List<FolderInterface> getFolderChildren() {
        return new ArrayList<FolderInterface>(folderChildren);
    }

    public static ZipStructure get(String docType) {
        for (ZipStructure zipStructure : ZipStructure.values()) {
            if (zipStructure.getDocType().equals(docType)) {
                return zipStructure;
            }
        }
        return null;
    }

    public String getXValidatorOid() {
        return xValidatorOid;
    }
}
