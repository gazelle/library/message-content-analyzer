package net.ihe.gazelle.mca.contentanalyzer.business.model.config.archives;

import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FileInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FolderInterface;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum Folder implements FolderInterface {

    SUBSET01("SUBSET01",  Arrays.asList(File.METADATA_XML, File.DOC), Arrays.asList(new Folder[]{})),
    IHE_XDM_2("IHE_XDM", Arrays.asList(new File[]{}), Arrays.asList(SUBSET01)),
    IHE_XDM_1("IHE_XDM", Arrays.asList(File.INDEX_HTM, File.README_TXT), Arrays.asList(Folder.IHE_XDM_2));

    private final String folderName;
    private final List<File> fileChildren;
    private final List<Folder> folderChildren;

    Folder(final String folderName, List<File> fileChildren, List<Folder> folderChildren) {
        this.folderName = folderName;
        this.fileChildren = fileChildren;
        this.folderChildren = folderChildren;
    }

    public String getFolderName() {
        return folderName;
    }

    public List<FileInterface> getFileChildren() {
        return new ArrayList<FileInterface>(fileChildren);
    }

    public List<FolderInterface> getFolderChildren() {
        return new ArrayList<FolderInterface>(folderChildren);
    }
}
