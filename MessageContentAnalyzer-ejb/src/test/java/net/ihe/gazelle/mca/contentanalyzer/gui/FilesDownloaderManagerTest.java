package net.ihe.gazelle.mca.contentanalyzer.gui;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationFactory;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisPartManager;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.DicomToTxtConverter;
import net.ihe.gazelle.mca.contentanalyzer.application.files.FilesDownloaderManager;
import net.ihe.gazelle.mca.contentanalyzer.application.files.TmpFilesManager;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.AnalysisPartDao;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.DocType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.ContentAnalysisConfigEnum;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.MimeTypeConfigEnum;
import org.apache.commons.io.FileUtils;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.mockito.Mockito.verify;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ FacesMessages.class, Component.class, FacesContext.class, ExternalContext.class })
@PowerMockIgnore({ "org.xml.*", "org.w3c.*", "javax.xml.*" })
public class FilesDownloaderManagerTest {

    private FacesContext facesContext;

    private HttpServletResponse httpServletResponse;
    private ServletOutputStream servletOutputStream;
    private byte[] buf;
    private byte[] buf2;

    @Mock
    ApplicationPreferenceManager applicationPreferenceManager;

    @Mock
    AnalysisPartDao analysisPartDao;

    AnalysisPartManager analysisPartManager = new AnalysisPartManager(analysisPartDao);

    private Analysis analysis;

    private FilesDownloaderManager filesDownloaderManager;

    @Before
    public void setUp() throws Exception {
        analysis = new Analysis(new HandledObject(new byte[1], "description"), null);

        mockApplicationPreferenceManager();
        filesDownloaderManager = new FilesDownloaderManager(analysisPartManager,
                new DicomToTxtConverter(applicationPreferenceManager, new TmpFilesManager()));

        buf = new byte[4096];
        byte[] contentBytes= "contentToDownload".getBytes(StandardCharsets.UTF_8);
        System.arraycopy(contentBytes, 0, buf, 0, 17);
        mockFacesMessages();
        buf2 = new byte[4096];
        final File f = loadFile("/contentanalyzer/validXmlFile.xml");
        byte[] fileBytesContent = FileUtils.readFileToByteArray(f);
        System.arraycopy(fileBytesContent, 0, buf2, 0, fileBytesContent.length);
    }

    private void mockApplicationPreferenceManager() {
        applicationPreferenceManager = Mockito.mock(ApplicationPreferenceManager.class);
        Mockito.when(applicationPreferenceManager.getStringValue("path_to_dcmDump")).thenReturn("/usr/bin/dcmdump");
    }

    private void mockFacesMessages() throws Exception {
        FacesMessages facesMessageMock = PowerMockito.mock(FacesMessages.class);
        PowerMockito.mockStatic(FacesContext.class);
        PowerMockito.mockStatic(Component.class);
        PowerMockito.mockStatic(ExternalContext.class);

        facesContext =  PowerMockito.mock(FacesContext.class);
        ExternalContext externalContext = PowerMockito.mock(ExternalContext.class);
        httpServletResponse = PowerMockito.mock(HttpServletResponse.class);
        servletOutputStream = PowerMockito.mock(ServletOutputStream.class);

        PowerMockito.when(FacesContext.getCurrentInstance()).thenReturn(facesContext);
        PowerMockito.when(facesContext.getExternalContext()).thenReturn(externalContext);
        PowerMockito.when(externalContext.getResponse()).thenReturn(httpServletResponse);
        PowerMockito.when(httpServletResponse.getOutputStream()).thenReturn(servletOutputStream);
        PowerMockito.when(FacesMessages.instance()).thenReturn(facesMessageMock);
    }

    @Test
    public void downLoadFileTestRandomDocType() throws IOException {
        AnalysisPart analysisPart = new AnalysisPart();
        analysisPart.setDocType("DocType");
        filesDownloaderManager.downloadFile("contentToDownload", analysisPart, analysis, facesContext);
        verify(httpServletResponse).setHeader("Content-Disposition","attachment; filename=\"description_DocType.txt\"");
        verify(httpServletResponse).setContentType("text/plain");
        verify(httpServletResponse).setContentLength(17);

        verify(servletOutputStream).write(buf , 0, 17);
    }

    @Test
    public void downLoadFileTestXML() throws IOException {
        AnalysisPart analysisPart = new AnalysisPart();
        analysisPart.setDocType("XML");
        HashMap<String, String> listNamespaces = new HashMap<String, String>();
        listNamespaces.put("Name", "Space");
        analysisPart.setNamespaces(listNamespaces);
        filesDownloaderManager.downloadFile("contentToDownload", analysisPart, analysis, facesContext);
        verify(httpServletResponse).setHeader("Content-Disposition","attachment; filename=\"description_XML.xml\"");
        verify(httpServletResponse).setContentType("text/xml");
        verify(httpServletResponse).setContentLength(17);

        verify(servletOutputStream).write(buf , 0, 17);
    }

    @Test
    public void downLoadFileTestPDF() throws IOException {
        AnalysisPart analysisPart = new AnalysisPart();
        analysisPart.setDocType("PDF");
        filesDownloaderManager.downloadFile("contentToDownload", analysisPart, analysis, facesContext);
        verify(httpServletResponse).setHeader("Content-Disposition","attachment; filename=\"description_"+
                "PDF"+".pdf\"");
        verify(httpServletResponse).setContentType("application/pdf");
        verify(httpServletResponse).setContentLength(17);

        verify(servletOutputStream).write(buf , 0, 17);
    }

    @Test
    public void downLoadFileTestHL7() throws IOException {
        AnalysisPart analysisPart = new AnalysisPart();
        analysisPart.setDocType("HL7v2");
        filesDownloaderManager.downloadFile("contentToDownload", analysisPart, analysis, facesContext);
        verify(httpServletResponse).setHeader("Content-Disposition","attachment; filename=\"description_"
                + "HL7v2"+".hl7\"");
        verify(httpServletResponse).setContentType("application/hl7-v2");
        verify(httpServletResponse).setContentLength(17);

        verify(servletOutputStream).write(buf , 0, 17);
    }

    @Test
    public void downLoadFileTestB64() throws IOException {
        AnalysisPart analysisPart = new AnalysisPart();
        analysisPart.setDocType(DocType.BASE64.getValue());
        filesDownloaderManager.downloadFile("contentToDownload", analysisPart, analysis, facesContext);
        verify(httpServletResponse).setHeader("Content-Disposition","attachment; filename=\"description_"
                + DocType.BASE64.getValue() +".txt\"");
        verify(httpServletResponse).setContentType("application/base64");
        verify(httpServletResponse).setContentLength(17);

        verify(servletOutputStream).write(buf , 0, 17);
    }

    @Test
    public void downLoadFileTestSyslog() throws IOException {
        AnalysisPart analysisPart = new AnalysisPart();
        analysisPart.setDocType(ContentAnalysisConfigEnum.SYSLOG.getDocType());
        filesDownloaderManager.downloadFile("contentToDownload", analysisPart, analysis, facesContext);
        verify(httpServletResponse).setHeader("Content-Disposition","attachment; filename=\"description_"
                + ContentAnalysisConfigEnum.SYSLOG.getDocType()+".syslog\"");
        verify(httpServletResponse).setContentType("text/plain");
        verify(httpServletResponse).setContentLength(17);

        verify(servletOutputStream).write(buf , 0, 17);
    }

    @Test
    public void downLoadFileTestCertificate() throws IOException {
        AnalysisPart analysisPart = new AnalysisPart();
        analysisPart.setDocType("Certificate");
        filesDownloaderManager.downloadFile("contentToDownload", analysisPart, analysis, facesContext);
        verify(httpServletResponse).setHeader("Content-Disposition","attachment; filename=\"description_"
                + "Certificate"+".pem\"");
        verify(httpServletResponse).setContentType("application/x-pem-file");
        verify(httpServletResponse).setContentLength(17);

        verify(servletOutputStream).write(buf , 0, 17);
    }

    @Test
    public void downLoadFileTestDocumentChildPDF() throws IOException {
        AnalysisPart analysisPart = new AnalysisPart();
        analysisPart.setDocType("DOCUMENT");
        AnalysisPart child = new AnalysisPart();
        child.setDocType("PDF");
        analysisPart.getChildPart().add(child);
        filesDownloaderManager.downloadFile("contentToDownload", analysisPart, analysis, facesContext);
        verify(httpServletResponse).setHeader("Content-Disposition","attachment; filename=\"description_"
                +"DOCUMENT"+".pdf\"");
        verify(httpServletResponse).setContentType("application/pdf");
        verify(httpServletResponse).setContentLength(17);

        verify(servletOutputStream).write(buf , 0, 17);
    }

    @Test
    public void downloadDICOMFile() {

        final File f = loadFile("/contentanalyzer/validDicomFile.dcm");
        byte[] bytesContent;
        try {
            bytesContent = FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            Assert.fail("Problem reading file");
            return;
        }

        AnalysisPart analysisPart = new AnalysisPart();
        analysisPart.setDocType("DICOM");
        try {
            filesDownloaderManager.downloadDICOMFile(bytesContent, analysisPart, analysis, facesContext);
        } catch (UnexpectedAnalysisException e) {
            fail("No exception should be raised here");
        }

        verify(httpServletResponse).setHeader("Content-Disposition","attachment; filename=\"description_"
                + "DICOM"+".dcm\"");
        verify(httpServletResponse).setContentType("application/dicom");
    }

    @Test
    public void downloadZipFile() throws IOException {
        final File f = loadFile("/contentanalyzer/validXmlFile.xml");

        AnalysisPart analysisPart = new AnalysisPart();
        analysisPart.setDocType(MimeTypeConfigEnum.ZIP.getDocType());

        filesDownloaderManager.downloadZipFile(f.getAbsolutePath(), analysisPart, analysis, facesContext);
        verify(httpServletResponse).setHeader("Content-Disposition","attachment; filename=\"description_"
                + MimeTypeConfigEnum.ZIP.getDocType()+".zip\"");
        verify(httpServletResponse).setContentType("application/zip");

        verify(servletOutputStream).write(buf2 , 0, 1359);
    }

    @Test
    public void getDescriptionForDownload() {
        AnalysisPart amp = new AnalysisPart();
        amp.setDocType("DocType");
        assertEquals("description_DocType", filesDownloaderManager.getDescriptionForDownload(amp, analysis));

    }
}
