package net.ihe.gazelle.mca.contentanalyzer.adapters.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.dao.AnalysisDaoImpl;
import net.ihe.gazelle.mca.contentanalyzer.adapters.config.dao.McaConfigDaoImpl;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManagerFactory;

@Name("mcaApplicationFactory")
@Scope(ScopeType.APPLICATION)
public class DaoTestFactory {

    public McaConfigDaoImpl getMcaConfigDao(EntityManagerFactory entityManagerFactory) {
        return new McaConfigDaoImpl(entityManagerFactory);
    }

    public AnalysisDaoImpl getAnalysisDaoImpl(EntityManagerFactory entityManagerFactory, ApplicationPreferenceManager applicationPreferenceManager) {
        return new AnalysisDaoImpl(entityManagerFactory, applicationPreferenceManager);
    }
}
