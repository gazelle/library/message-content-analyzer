package net.ihe.gazelle.mca.contentanalyzer.application;

import net.ihe.gazelle.mca.contentanalyzer.application.converters.ConfigToXmlConverter;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.ConfigsFromDB;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.McaConfigDaoStub;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ConfigInterface;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.fail;

public class ConfigToXmlTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigToXmlTest.class);

    ConfigToXmlConverter configToXmlConverter =  new ConfigToXmlConverter();

    @Test
    public void testConfigToXml() throws IOException {

        String xmlConfig;
        try {
            McaConfigDaoStub mcaConfigDAO = new McaConfigDaoStub();
            List<ConfigInterface> interfaceList = new ArrayList<>();
            interfaceList.addAll(mcaConfigDAO.getListOfContentAnalysisConfigEntity());
            interfaceList.addAll(mcaConfigDAO.getListOfMimeTypeEntity());
            interfaceList.addAll(mcaConfigDAO.getListOfXmlTagConfigEntity());
            xmlConfig = configToXmlConverter.configToXml(interfaceList);
        } catch (JAXBException e) {
            LOGGER.error("", e);
            fail();
            return;
        }
        final File f = loadFile("/contentanalyzer/exportedConfigs.xml");
        String fileContent = new String(FileUtils.readFileToByteArray(f), StandardCharsets.UTF_8);

        assertEquals(fileContent, xmlConfig);
    }

    @Test
    public void testXmlToConfig() throws IOException{

        final File f = loadFile("/contentanalyzer/exportedConfigs.xml");
        String fileContent = new String(FileUtils.readFileToByteArray(f), StandardCharsets.UTF_8);
        ConfigsFromDB configsFromDB;
        try {
            configsFromDB = configToXmlConverter.xmlToConfig(fileContent);
        } catch (JAXBException e) {
            LOGGER.error("", e);
            fail();
            return;
        }

        assertEquals(2, configsFromDB.getMimeTypeConfigEntities().size());
        assertEquals(6, configsFromDB.getContentAnalysisConfigEntities().size());
        assertEquals(8, configsFromDB.getXmlTagConfigEntities().size());
    }
}
