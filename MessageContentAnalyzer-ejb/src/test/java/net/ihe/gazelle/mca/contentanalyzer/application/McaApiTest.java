package net.ihe.gazelle.mca.contentanalyzer.application;


import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.AnalysisUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.metadata.ws.VersionController;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class McaApiTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(McaApiTest.class);

    @Mock
    private VersionController versionController;

    private McaApi mcaApi;

    @Before
    public void setUp() {

        ApplicationTestFactory applicationTestFactory = new ApplicationTestFactory();
        mcaApi = new McaApi(applicationTestFactory.getAnalysisManager(), applicationTestFactory.getMessageContentAnalyzer(),
                applicationTestFactory.getDicomToTxtConverter(), applicationTestFactory.getBase64Converter());

        MockitoAnnotations.initMocks(this);

        versionController = Mockito.mock(VersionController.class);
        Mockito.when(versionController.getVersion()).thenReturn("mcaVersion");
    }

    @Test
    public void analyzeTest() {

        final File f = loadFile("/contentanalyzer/validXmlFile.xml");
        byte[] fileBytesContent = null;
        try {
            fileBytesContent = FileUtils.readFileToByteArray(f);
        } catch(IOException e) {
            LOGGER.error("Error reading bytes from file : {}",e.getMessage());
        }
        HandledObject handledObject = new HandledObject(fileBytesContent, "validXmlFile.xml");
        Analysis analysis = new Analysis(handledObject, null);
        AnalysisUtils.addRootAnalysisPart(handledObject, analysis);
        try {
            analysis = mcaApi.analyze(analysis);
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be thrown : " + e.getMessage());
        }

        assertEquals("XML", analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getDocType());

        assertEquals(0, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1359, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getDocType());

        assertEquals(40, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1358, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());

        assertEquals(186, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(387, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());

        assertEquals(392, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(1338, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("XDS", analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        Assert.assertEquals(ValidationType.XDS, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getValidationType());


        assertEquals(415, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(1318, analysis.getRootAnalysisPart().getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());

        assertEquals("XML XDS ", AnalysisUtils.getValidationTypeListAsString(analysis.getValidationTypes()));
    }

    @Test
    public void analyzeCertificateTest() {

        final File f = loadFile("/contentanalyzer/validCertificateFile.pem");
        byte[] fileBytesContent = null;
        try {
            fileBytesContent = FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            LOGGER.error("Error reading bytes from file : {}", e.getMessage());
        }
        HandledObject handledObject = new HandledObject(fileBytesContent, "validXmlFile.xml");
        Analysis analysis = new Analysis(handledObject, null);
        AnalysisUtils.addRootAnalysisPart(handledObject, analysis);
        try {
            analysis = mcaApi.analyze(analysis);
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be thrown : " + e.getMessage());
        }

        assertEquals("TLS ", AnalysisUtils.getValidationTypeListAsString(analysis.getValidationTypes()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void handledObjectNullContent() {
        new HandledObject(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void analyzeEmptyBytesContent() throws UnexpectedAnalysisException {
        new HandledObject(null, null);
    }
}
