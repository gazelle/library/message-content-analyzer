package net.ihe.gazelle.mca.contentanalyzer.gui;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.DicomToTxtConverter;
import net.ihe.gazelle.mca.contentanalyzer.application.files.TmpFilesManager;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.*;

public class DicomToTxtConverterTest {

    @Mock
    ApplicationPreferenceManager applicationPreferenceManager;

    DicomToTxtConverter dicomToTxtConverter;

    @Before
    public void setUp() {
        mockApplicationPreferenceManager();
        dicomToTxtConverter = new DicomToTxtConverter(applicationPreferenceManager, new TmpFilesManager());
    }

    private void mockApplicationPreferenceManager() {
        applicationPreferenceManager = Mockito.mock(ApplicationPreferenceManager.class);
        Mockito.when(applicationPreferenceManager.getStringValue("path_to_dcmDump")).thenReturn("/usr/bin/dcmdump");
    }


    @Test
    public void testDcmToTXT() {
        final File f = loadFile("/contentanalyzer/validDicomFile.dcm");
        byte[] bytesContent;
        try {
            bytesContent = FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            fail("Problem reading file");
            return;
        }
        File macDcmDump = new File("/opt/local/bin/dcmdump");
        File dcmDump = new File("/usr/bin/dcmdump");

        byte[] decodedBytesContent;
        try {
            decodedBytesContent = dicomToTxtConverter.dicom2txt(bytesContent);
        } catch (UnexpectedAnalysisException e) {
            if (!macDcmDump.exists() && !dcmDump.exists()) {
                assertEquals("Error in the transformation of DICOM to text !", e.getMessage());
                return;
            }
            fail("No exception is supposed to be thrown there");
            return;
        }
        assertNotNull(decodedBytesContent);
        try {
            assertNotSame(new String(FileUtils.readFileToByteArray(loadFile("/contentanalyzer/validDicomFile.dcm")), StandardCharsets.UTF_8),
                    new String(decodedBytesContent, StandardCharsets.UTF_8));
        } catch (IOException e) {
            fail("Failed to read decoded file");
        }

    }

}
