package net.ihe.gazelle.mca.contentanalyzer.application.analyzers.xml;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.MessageContentAnalyzer;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class XMLAnalyzerTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(XMLAnalyzerTest.class);

    MessageContentAnalyzer m;
    XmlAnalyzer xmlAnalyzer = new ApplicationTestFactory().getXmlAnalyzer();

    @Before
    public void setUp() {
        m = new ApplicationTestFactory().getMessageContentAnalyzer();
    }

    @Test
    public void analyzeXmlTest() {

        final File f = loadFile("/contentanalyzer/validXmlFile.xml");
        final AnalysisPart analysisPart = new AnalysisPart(new AnalysisPart(null));
        analysisPart.setDocType("DOCUMENT");
        analysisPart.setStartOffset(0);
        try {
            analysisPart.setEndOffset(FileUtils.readFileToString(f).length());
        } catch(IOException e) {
            LOGGER.error("Unexpected Exception", e);
            fail();
        }
        final AnalysisPart childFileToValidate = new AnalysisPart(analysisPart);
        try {
            m.setMessageByteContent(FileUtils.readFileToByteArray(f));
            xmlAnalyzer.analyzeXml(m, childFileToValidate, analysisPart);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("XML", analysisPart.getChildPart().get(0).getDocType());

        assertEquals(0, analysisPart.getChildPart().get(0).getStartOffset());
        assertEquals(1359, analysisPart.getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", analysisPart.getChildPart().get(0).getChildPart().get(0).getDocType());

        assertEquals(40, analysisPart.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1358, analysisPart.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDocType());

        assertEquals(186, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(387, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getDocType());

        assertEquals(392, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getStartOffset());
        assertEquals(1338, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getEndOffset());

        assertEquals("XDS", analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getDocType());
        Assert.assertEquals(ValidationType.XDS, analysisPart.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(1).getChildPart().get(0).getValidationType());

        assertEquals(415, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getStartOffset());
        assertEquals(1318, analysisPart.getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(1).getChildPart().get(0).getEndOffset());
    }
}
