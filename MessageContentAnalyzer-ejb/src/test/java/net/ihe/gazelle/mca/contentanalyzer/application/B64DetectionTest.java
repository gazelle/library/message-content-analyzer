package net.ihe.gazelle.mca.contentanalyzer.application;

import net.ihe.gazelle.mca.contentanalyzer.application.converters.Base64Converter;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.testng.AssertJUnit.*;

public class B64DetectionTest {

    Base64Converter base64Converter;

    @Before
    public void setUp() {
        base64Converter = new Base64Converter();
    }

    @Test
    public void detectB64FalseTest() throws IOException {
        final File f = loadFile("/contentanalyzer/validXACMLWithPrefixXmlFile.xml");
        String fileContent = new String(FileUtils.readFileToByteArray(f), StandardCharsets.UTF_8);
        assertFalse(base64Converter.base64Detection(fileContent));
    }

    @Test
    public void detectB64TrueTest() throws IOException {
        final File f = loadFile("/contentanalyzer/validBase64File.txt");
        String fileContent = new String(FileUtils.readFileToByteArray(f), StandardCharsets.UTF_8);
        assertTrue(base64Converter.base64Detection(fileContent));
    }

    @Test
    public void decodeBase64Test() throws IOException {
        final File f = loadFile("/contentanalyzer/validBase64File.txt");
        String fileContent = new String(FileUtils.readFileToByteArray(f), StandardCharsets.UTF_8);
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
                "<ClinicalDocument xmlns=\"urn:hl7-org:v3\" xmlns:cda=\"urn:hl7-org:v3\" xmlns:epsos=\"urn:epsos-org:ep:medication\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"urn:hl7-org:v3 ..\\schema\\CDA_extended.xsd\">\n" +
                "  <typeId extension=\"POCD_HD000040\" root=\"2.16.840.1.113883.1.3\"/>\n" +
                "  <templateId root=\"1.3.6.1.4.1.12559.11.10.1.3.1.1.3\"/>\n" +
                "  <id assigningAuthorityName=\"Regione Lombardia\" displayable=\"true\" extension=\"030308.RRCSLS.REF030308P1622.60591-5.PDF\" root=\"2.16.840.1.113883.2.9.2.30\"/>\n" +
                "  <code code=\"60591-5\" codeSystem=\"2.16.840.1.113883.6.1\" codeSystemName=\"LOINC\" codeSystemVersion=\"2.34\" displayName=\"Patient Summary\"/>\n" +
                "  <title>Patient Summary</title>\n" +
                "  <effectiveTime value=\"20111007115442+0100\"/>\n" +
                "  <confidentialityCode code=\"N\" codeSystem=\"2.16.840.1.113883.5.25\" codeSystemName=\"Confidentiality\" codeSystemVersion=\"913-20091020\" displayName=\"normal\">\n" +
                "    <translation displayName=\"Normale\"/>\n" +
                "  </confidentialityCode>\n" +
                "  <languageCode code=\"it-IT\"/>\n" +
                "  <setId assigningAuthorityName=\"Regione Lombardia\" displayable=\"true\" extension=\"030308.RRCSLS.REF030308P1622.60591-5.PDF\" root=\"2.16.840.1.113883.2.9.2.30\"/>\n" +
                "  <versionNumber value=\"1\"/>\n" +
                "  <recordTarget>\n" +
                "    <patientRole classCode=\"PAT\">\n" +
                "      <id assigningAuthorityName=\"MEF\" displayable=\"true\" extension=\"NTSCTN80L58F952H\" root=\"2.16.840.1.113883.2.9.4.3.2\"/>\n" +
                "      <addr use=\"HP\">\n" +
                "        <state>RM</state>\n" +
                "        <city>ROMA</city>\n" +
                "        <country>IT</country>\n" +
                "        <postalCode>00100</postalCode>\n" +
                "        <streetAddressLine>V. C/O LOMBARDIA INFORMATICA VIA, MINZONI 24</streetAddressLine>\n" +
                "      </addr>\n" +
                "      <telecom nullFlavor=\"NI\"/>\n" +
                "      <patient>\n" +
                "        <name>\n" +
                "          <family>INTSISS</family>\n" +
                "          <given>CENTONOVANTANOVE</given>\n" +
                "        </name>\n" +
                "        <administrativeGenderCode code=\"F\" codeSystem=\"2.16.840.1.113883.5.1\" codeSystemName=\"AdministrativeGender\" codeSystemVersion=\"913-20091020\" displayName=\"Female\">\n" +
                "          <translation displayName=\"Femmina\"/>\n" +
                "        </administrativeGenderCode>\n" +
                "        <birthTime value=\"19800718\"/>\n" +
                "        <languageCommunication>\n" +
                "          <languageCode code=\"it-IT\"/>\n" +
                "        </languageCommunication>\n" +
                "      </patient>\n" +
                "    </patientRole>\n" +
                "  </recordTarget>\n" +
                "  <author>\n" +
                "    <functionCode code=\"221\" codeSystem=\"2.16.840.1.113883.2.9.6.2.7\" codeSystemName=\"ISCO\" codeSystemVersion=\"2008\" displayName=\"Medical doctors\">\n" +
                "      <translation displayName=\"Medico di Medicina Generale\"/>\n" +
                "    </functionCode>\n" +
                "    <time value=\"20111007115442+0100\"/>\n" +
                "    <assignedAuthor classCode=\"ASSIGNED\">\n" +
                "      <id assigningAuthorityName=\"MEF\" displayable=\"true\" extension=\"CNPMMG60L01F205V\" root=\"2.16.840.1.113883.2.9.4.3.2\"/>\n" +
                "      <addr nullFlavor=\"NI\"/>\n" +
                "      <telecom nullFlavor=\"NI\"/>\n" +
                "      <assignedAuthoringDevice>\n" +
                "        <code code=\"CAPTURE\" codeSystem=\"1.2.840.10008.2.16.4\" displayName=\"Image Capture\"/>\n" +
                "        <manufacturerModelName>Generato automaticamente</manufacturerModelName>\n" +
                "        <softwareName>CRS-SISS</softwareName>\n" +
                "      </assignedAuthoringDevice>\n" +
                "      <representedOrganization>\n" +
                "        <id assigningAuthorityName=\"Regione Lombardia\" extension=\"030308\" root=\"2.16.840.1.113883.2.9.4.1.1\"/>\n" +
                "        <name nullFlavor=\"NI\"/>\n" +
                "        <telecom nullFlavor=\"NI\"/>\n" +
                "        <addr nullFlavor=\"NI\"/>\n" +
                "      </representedOrganization>\n" +
                "    </assignedAuthor>\n" +
                "  </author>\n" +
                "  <custodian>\n" +
                "    <assignedCustodian>\n" +
                "      <representedCustodianOrganization>\n" +
                "        <id assigningAuthorityName=\"Regione Lombardia\" extension=\"030308\" root=\"2.16.840.1.113883.2.9.4.1.1\"/>\n" +
                "        <name nullFlavor=\"NI\"/>\n" +
                "        <telecom nullFlavor=\"NI\"/>\n" +
                "        <addr>\n" +
                "          <country nullFlavor=\"NI\"/>\n" +
                "        </addr>\n" +
                "      </representedCustodianOrganization>\n" +
                "    </assignedCustodian>\n" +
                "  </custodian>\n" +
                "  <legalAuthenticator>\n" +
                "    <time value=\"20111007115442+0100\"/>\n" +
                "    <signatureCode code=\"S\"/>\n" +
                "    <assignedEntity>\n" +
                "      <id assigningAuthorityName=\"MEF\" displayable=\"false\" extension=\"CNPMMG60L01F205V\" root=\"2.16.840.1.113883.2.9.4.3.2\"/>\n" +
                "      <addr nullFlavor=\"NI\"/>\n" +
                "      <telecom nullFlavor=\"NI\"/>\n" +
                "      <assignedPerson>\n" +
                "        <name>\n" +
                "          <family>CNIPAVII</family>\n" +
                "          <given>MMG</given>\n" +
                "        </name>\n" +
                "      </assignedPerson>\n" +
                "      <representedOrganization classCode=\"ORG\" determinerCode=\"INSTANCE\">\n" +
                "        <id assigningAuthorityName=\"Regione Lombardia\" extension=\"030308\" root=\"2.16.840.1.113883.2.9.4.1.1\"/>\n" +
                "        <name nullFlavor=\"NI\"/>\n" +
                "        <telecom nullFlavor=\"NI\"/>\n" +
                "        <addr nullFlavor=\"NI\"/>\n" +
                "      </representedOrganization>\n" +
                "    </assignedEntity>\n" +
                "  </legalAuthenticator>\n" +
                "  <participant typeCode=\"IND\">\n" +
                "    <functionCode code=\"PCP\" codeSystem=\"2.16.840.1.113883.5.88\"/>\n" +
                "    <associatedEntity classCode=\"PRS\">\n" +
                "      <addr nullFlavor=\"NI\"/>\n" +
                "      <telecom nullFlavor=\"NI\"/>\n" +
                "      <associatedPerson>\n" +
                "        <name nullFlavor=\"NI\"/>\n" +
                "      </associatedPerson>\n" +
                "    </associatedEntity>\n" +
                "  </participant>\n" +
                "  <documentationOf>\n" +
                "    <serviceEvent classCode=\"PCPR\">\n" +
                "      <effectiveTime>\n" +
                "        <low nullFlavor=\"NI\"/>\n" +
                "        <high value=\"20111007115442+0100\"/>\n" +
                "      </effectiveTime>\n" +
                "      <performer typeCode=\"PRF\">\n" +
                "        <functionCode code=\"221\" codeSystem=\"2.16.840.1.113883.2.9.6.2.7\" codeSystemName=\"ISCO\" codeSystemVersion=\"2008\" displayName=\"Medical doctors\">\n" +
                "          <translation displayName=\"Medico di Medicina Generale\"/>\n" +
                "        </functionCode>\n" +
                "        <assignedEntity>\n" +
                "          <id assigningAuthorityName=\"MEF\" displayable=\"false\" extension=\"CNPMMG60L01F205V\" root=\"2.16.840.1.113883.2.9.4.3.2\"/>\n" +
                "          <addr nullFlavor=\"NI\"/>\n" +
                "          <telecom nullFlavor=\"NI\"/>\n" +
                "          <assignedPerson>\n" +
                "            <name>\n" +
                "              <family>CNIPAVII</family>\n" +
                "              <given>MMG</given>\n" +
                "            </name>\n" +
                "          </assignedPerson>\n" +
                "          <representedOrganization>\n" +
                "            <id assigningAuthorityName=\"Regione Lombardia\" extension=\"030308\" root=\"2.16.840.1.113883.2.9.4.1.1\"/>\n" +
                "            <name nullFlavor=\"NI\"/>\n" +
                "            <telecom nullFlavor=\"NI\"/>\n" +
                "            <addr nullFlavor=\"NI\"/>\n" +
                "          </representedOrganization>\n" +
                "        </assignedEntity>\n" +
                "      </performer>\n" +
                "    </serviceEvent>\n" +
                "  </documentationOf>\n" +
                "  <relatedDocument typeCode=\"XFRM\">\n" +
                "    <parentDocument>\n" +
                "      <id extension=\"030308.RRCSLS.REF030308X1475\" root=\"2.16.840.1.113883.2.9.2.30\"/>\n" +
                "    </parentDocument>\n" +
                "  </relatedDocument>\n" +
                "  <component>\n" +
                "    <nonXMLBody>\n" +
                "      <text mediaType=\"application/pdf\" representation=\"B64\">JVBERi0xLjQKMSAwIG9iago8PC9UeXBlIC9QYWdlcwovS2lkcyBbMyAwIFIKNSAwIFIKXQovQ291bnQgMgo+PgplbmRvYmoKMyAwIG9iago8PC9UeXBlIC9QYWdlCi9QYXJlbnQgMSAwIFIKL01lZGlhQm94IFswIDAgNTk1LjI4IDg0MS44OV0KL1Jlc291cmNlcyAyIDAgUgovUm90YXRlIDAKL0R1ciAzCi9Db250ZW50cyA0IDAgUj4+CmVuZG9iago0IDAgb2JqCjw8L0xlbmd0aCA4NDU1Pj4Kc3RyZWFtCjIgSgowLjU3IHcKMC44IDEgMC44IHJnCkJUIC9GMSAxNi4wMCBUZiBFVAowIFRyCi9HUzAgZ3MKMCBUdyAwIFRjIDEwMCBUeiAwIFRMIApxIDAgMCAwIHJnIEJUIDIxOS44NiA3OTQuNTcgVGQgKFBBVElFTlQgU1VNTUFSWSkgVGogRVQgUQovR1MxIGdzCnEgNTYuNjkgMCAwIDU2LjY5IDE0LjE3IDc3MS4wMiBjbQovSTEgRG8gUQowLjggMSAwLjggcmcKQlQgL0YyIDEwLjAwIFRmIEVUCjE0LjE3IDcyOC41MCA1NjYuOTMgLTE0LjE3IHJlIGYgcSAwIDAgMCByZyBCVCAxNy4wMSA3MTguNDIgVGQgKERhdGkgUGF6aWVudGUpIFRqIEVUIFEKQlQgL0YyIDguMDAgVGYgRVQKcSAwIDAgMCByZyBCVCAxNy4wMSA3MDIuMDEgVGQgKE5vbWU6KSBUaiBFVCBRCnEgMCAwIDAgcmcgQlQgMTAyLjA1IDcwMi4wMSBUZCAoQ0VOVE9OT1ZBTlRBTk9WRSkgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDMwMC40NyA3MDIuMDEgVGQgKERhdGEgTmFzY2l0YTopIFRqIEVUIFEKcSAwIDAgMCByZyBCVCAzODUuNTEgNzAyLjAxIFRkICgxOC8wNy8xOTgwKSBUaiBFVCBRCnEgMCAwIDAgcmcgQlQgMTcuMDEgNjg3Ljg0IFRkIChDb2dub21lOikgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDEwMi4wNSA2ODcuODQgVGQgKElOVFNJU1MpIFRqIEVUIFEKcSAwIDAgMCByZyBCVCAzMDAuNDcgNjg3Ljg0IFRkIChTZXNzbzopIFRqIEVUIFEKcSAwIDAgMCByZyBCVCAzODUuNTEgNjg3Ljg0IFRkIChGZW1taW5hKSBUaiBFVCBRCnEgMCAwIDAgcmcgQlQgMTcuMDEgNjczLjY2IFRkIChDb2RpY2UgRmlzY2FsZTopIFRqIEVUIFEKcSAwIDAgMCByZyBCVCAxMDIuMDUgNjczLjY2IFRkIChOVFNDVE44MEw1OEY5NTJIKSBUaiBFVCBRCjAuOCAxIDAuOCByZwpCVCAvRjIgMTAuMDAgVGYgRVQKMTQuMTcgNjU0LjgwIDU2Ni45MyAtMTQuMTcgcmUgZiBxIDAgMCAwIHJnIEJUIDE3LjAxIDY0NC43MiBUZCAoQWxsZXJnaWUgXChmYXJtYWNvbG9naWNoZVwpIGUgcmVhemlvbmkgYXZ2ZXJzZSkgVGogRVQgUQpCVCAvRjIgOC4wMCBUZiBFVAowIFRyCi9HUzIgZ3MKMCBUdyAwIFRjIDEwMCBUeiAwIFRMIAowLjkwMTk2MDc4NDMxMzcyNTUgMC44OTQxMTc2NDcwNTg4MjM2IDAuODk0MTE3NjQ3MDU4ODIzNiByZwoxNC4xNyA2MzcuODAgMjgzLjQ2IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMTcuMDEgNjI4LjMxIFRkIChUaXBvIEFsbGVyZ2lhKSBUaiBFVCBRCjI5Ny42NCA2MzcuODAgMjgzLjQ2IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMzAwLjQ3IDYyOC4zMSBUZCAoVGlwbyBSZWF6aW9uZSkgVGogRVQgUQowIFRyCi9HUzMgZ3MKMCBUdyAwIFRjIDEwMCBUeiAwIFRMIAoxNC4xNyA2MjMuNjIgMjgzLjQ2IC00Mi41MiByZSBTCnEgMCAwIDAgcmcgQlQgMTcuMDEgNjE0LjE0IFRkICg5OTUuMC82MyAgU0hPQ0sgQU5BRklMQVRUSUNPIERBIEZSVVRUSSBPIFZFR0VUQUxJKSBUaiBFVCBRCnEgMCAwIDAgcmcgQlQgMTcuMDEgNTk5Ljk2IFRkIChcKEZyYWdvbGVcKTk5NS4wLzA2ICBJTlRPTExFUkFOWkEgQUxJTUVOVEFSRSBcKEZyYWdvbGVcKTk5NS4wLzY5ICkgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDE3LjAxIDU4NS43OSBUZCAoQU5BRklMQVNTSSBDT04gUFJPUFJJRVRBJyBcKEZyYWdvbGVcKSkgVGogRVQgUQoyOTcuNjQgNjIzLjYyIDI4My40NiAtNDIuNTIgcmUgUwowLjggMSAwLjggcmcKQlQgL0YyIDEwLjAwIFRmIEVUCjE0LjE3IDU2Ni45MyA1NjYuOTMgLTE0LjE3IHJlIGYgcSAwIDAgMCByZyBCVCAxNy4wMSA1NTYuODQgVGQgKExpc3RhIHByb2JsZW1pIHJpbGV2YW50aSBlIGRpYWdub3NpIGNvZGlmaWNhdGUpIFRqIEVUIFEKQlQgL0YyIDguMDAgVGYgRVQKMCBUcgovR1M0IGdzCjAgVHcgMCBUYyAxMDAgVHogMCBUTCAKMC45MDE5NjA3ODQzMTM3MjU1IDAuODk0MTE3NjQ3MDU4ODIzNiAwLjg5NDExNzY0NzA1ODgyMzYgcmcKMTQuMTcgNTQ5LjkyIDExMy4zOSAtMTQuMTcgcmUgYgpxIDAgMCAwIHJnIEJUIDE3LjAxIDU0MC40MyBUZCAoRGVzY3JpemlvbmUgUHJvYmxlbWEpIFRqIEVUIFEKMTI3LjU2IDU0OS45MiAxMTMuMzkgLTE0LjE3IHJlIGIKcSAwIDAgMCByZyBCVCAxMzAuMzkgNTQwLjQzIFRkIChEYXRhIEluc29yZ2VuemEpIFRqIEVUIFEKMjQwLjk0IDU0OS45MiAxMTMuMzkgLTE0LjE3IHJlIGIKcSAwIDAgMCByZyBCVCAyNDMuNzggNTQwLjQzIFRkIChEaWFnbm9zaSkgVGogRVQgUQozNTQuMzMgNTQ5LjkyIDExMy4zOSAtMTQuMTcgcmUgYgpxIDAgMCAwIHJnIEJUIDM1Ny4xNyA1NDAuNDMgVGQgKFN0YXRvKSBUaiBFVCBRCjQ2Ny43MiA1NDkuOTIgMTEzLjM5IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgNDcwLjU1IDU0MC40MyBUZCAoRGF0YSBldmVudHVhbGUgU29sdXppb25lKSBUaiBFVCBRCjAgVHIKL0dTNSBncwowIFR3IDAgVGMgMTAwIFR6IDAgVEwgCjE0LjE3IDUzNS43NSAxMTMuMzkgLTI4LjM1IHJlIFMKMTI3LjU2IDUzNS43NSAxMTMuMzkgLTI4LjM1IHJlIFMKcSAwIDAgMCByZyBCVCAxMzAuMzkgNTI2LjI2IFRkICgwMS8wMi8yMDEwKSBUaiBFVCBRCjI0MC45NCA1MzUuNzUgMTEzLjM5IC0yOC4zNSByZSBTCnEgMCAwIDAgcmcgQlQgMjQzLjc4IDUyNi4yNiBUZCAoVFVNT1JJIE1BTElHTkkgREVMTEEpIFRqIEVUIFEKcSAwIDAgMCByZyBCVCAyNDMuNzggNTEyLjA5IFRkIChHSElBTkRPTEEgVElST0lERSkgVGogRVQgUQozNTQuMzMgNTM1Ljc1IDExMy4zOSAtMjguMzUgcmUgUwpxIDAgMCAwIHJnIEJUIDM1Ny4xNyA1MjYuMjYgVGQgKEF0dGl2bykgVGogRVQgUQo0NjcuNzIgNTM1Ljc1IDExMy4zOSAtMjguMzUgcmUgUwowLjggMSAwLjggcmcKQlQgL0YyIDEwLjAwIFRmIEVUCjE0LjE3IDQ5My4yMyA1NjYuOTMgLTE0LjE3IHJlIGYgcSAwIDAgMCByZyBCVCAxNy4wMSA0ODMuMTQgVGQgKFRlcmFwaWUgZmFybWFjb2xvZ2ljaGUgY3JvbmljaGUgbyBhdHR1YWxpIHJpbGV2YW50aSkgVGogRVQgUQpCVCAvRjIgOC4wMCBUZiBFVAowIFRyCi9HUzYgZ3MKMCBUdyAwIFRjIDEwMCBUeiAwIFRMIAowLjkwMTk2MDc4NDMxMzcyNTUgMC44OTQxMTc2NDcwNTg4MjM2IDAuODk0MTE3NjQ3MDU4ODIzNiByZwoxNC4xNyA0NzYuMjIgMTEzLjM5IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMTcuMDEgNDY2LjczIFRkIChOb21lIEZhcm1hY28pIFRqIEVUIFEKMTI3LjU2IDQ3Ni4yMiAxMTMuMzkgLTE0LjE3IHJlIGIKcSAwIDAgMCByZyBCVCAxMzAuMzkgNDY2LjczIFRkIChQcmluY2lwaW8gQXR0aXZvKSBUaiBFVCBRCjI0MC45NCA0NzYuMjIgMTEzLjM5IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMjQzLjc4IDQ2Ni43MyBUZCAoRG9zYWdnaW8pIFRqIEVUIFEKMzU0LjMzIDQ3Ni4yMiAxMTMuMzkgLTE0LjE3IHJlIGIKcSAwIDAgMCByZyBCVCAzNTcuMTcgNDY2LjczIFRkIChQb3NvbG9naWEpIFRqIEVUIFEKNDY3LjcyIDQ3Ni4yMiAxMTMuMzkgLTE0LjE3IHJlIGIKcSAwIDAgMCByZyBCVCA0NzAuNTUgNDY2LjczIFRkIChWaWEgZGkgU29tbWluaXN0cmF6aW9uZSkgVGogRVQgUQowIFRyCi9HUzcgZ3MKMCBUdyAwIFRjIDEwMCBUeiAwIFRMIAoxNC4xNyA0NjIuMDUgMTEzLjM5IC0yOC4zNSByZSBTCnEgMCAwIDAgcmcgQlQgMTcuMDEgNDUyLjU2IFRkIChMRVZPVElST1gqNTAgQ1BTIDEwMCkgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDE3LjAxIDQzOC4zOSBUZCAoTUNHKSBUaiBFVCBRCjEyNy41NiA0NjIuMDUgMTEzLjM5IC0yOC4zNSByZSBTCnEgMCAwIDAgcmcgQlQgMTMwLjM5IDQ1Mi41NiBUZCAoTEVWT1RJUk9YSU5BIFNPRElDQSkgVGogRVQgUQoyNDAuOTQgNDYyLjA1IDExMy4zOSAtMjguMzUgcmUgUwpxIDAgMCAwIHJnIEJUIDI0My43OCA0NTIuNTYgVGQgKDEwMCkgVGogRVQgUQozNTQuMzMgNDYyLjA1IDExMy4zOSAtMjguMzUgcmUgUwpxIDAgMCAwIHJnIEJUIDM1Ny4xNyA0NTIuNTYgVGQgKDEgY2Fwc3VsYSBwcmltYSBkaSBjb2xhemlvbmUpIFRqIEVUIFEKcSAwIDAgMCByZyBCVCAzNTcuMTcgNDM4LjM5IFRkIChvZ25pIDI0IG9yZSkgVGogRVQgUQo0NjcuNzIgNDYyLjA1IDExMy4zOSAtMjguMzUgcmUgUwowLjggMSAwLjggcmcKQlQgL0YyIDEwLjAwIFRmIEVUCjE0LjE3IDQxOS41MyA1NjYuOTMgLTE0LjE3IHJlIGYgcSAwIDAgMCByZyBCVCAxNy4wMSA0MDkuNDQgVGQgKFN0YXRvIGNvcnJlbnRlIGRlbCBwYXppZW50ZSkgVGogRVQgUQpCVCAvRjIgOC4wMCBUZiBFVApxIDAgMCAwIHJnIEJUIDE3LjAxIDM5My4wMyBUZCAoYWxsbyBzdGF0byBhdHR1YWxlIG5vbiBlc2lzdG9ubyBmYXR0aSBpbmVyZW50aSBxdWVzdGEgcHJvYmxlbWF0aWNhKSBUaiBFVCBRCnEgMCAwIDAgcmcgQlQgMTcuMDEgMzc2LjAzIFRkIChhbGxvIHN0YXRvIGF0dHVhbGUgbm9uIGVzaXN0b25vIGZhdHRpIGluZXJlbnRpIHF1ZXN0YSBwcm9ibGVtYXRpY2EpIFRqIEVUIFEKcSAwIDAgMCByZyBCVCAxNy4wMSAzNTkuMDIgVGQgKGFsbG8gc3RhdG8gYXR0dWFsZSBub24gZXNpc3Rvbm8gZmF0dGkgaW5lcmVudGkgcXVlc3RhIHByb2JsZW1hdGljYSkgVGogRVQgUQowLjggMSAwLjggcmcKQlQgL0YyIDEwLjAwIFRmIEVUCjE0LjE3IDM0MC4xNiA1NjYuOTMgLTE0LjE3IHJlIGYgcSAwIDAgMCByZyBCVCAxNy4wMSAzMzAuMDcgVGQgKFBhcmFtZXRyaSBkaSBNb25pdG9yYWdnaW8pIFRqIEVUIFEKQlQgL0YyIDguMDAgVGYgRVQKMCBUcgovR1M4IGdzCjAgVHcgMCBUYyAxMDAgVHogMCBUTCAKMC45MDE5NjA3ODQzMTM3MjU1IDAuODk0MTE3NjQ3MDU4ODIzNiAwLjg5NDExNzY0NzA1ODgyMzYgcmcKMTQuMTcgMzIzLjE1IDU2Ni45MyAtMTQuMTcgcmUgYgpxIDAgMCAwIHJnIEJUIDE3LjAxIDMxMy42NiBUZCAoQW5hbW5lc2kgRmFtaWdsaWFyZSwgcHJvc3NpbWEgZSByZW1vdGEpIFRqIEVUIFEKMCBUcgovR1M5IGdzCjAgVHcgMCBUYyAxMDAgVHogMCBUTCAKMTQuMTcgMzA4Ljk4IDU2Ni45MyAtMjguMzUgcmUgUwpxIDAgMCAwIHJnIEJUIDE3LjAxIDI5OS40OSBUZCAoMjk2LjIvMDAgRGVwcmVzc2lvbmUgbWFnZ2lvcmUgZXBpc29kaW8gc2luZ29sbyBub24gc3BlY2lmaWNhdG8gZGFsIDYvMDEvMjAwNyBhbCA4LzA0LzIwMDc0NTUuMC8wMCBFbW9ycm9pZGkgaW50ZXJuZSBzZW56YSBtZW56aW9uZSBkaSkgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDE3LjAxIDI4NS4zMiBUZCAoY29tcGxpY2F6aW9uaVZBQ0NJTkFaSU9OSTotICBWYWNjaW5vIGFudGkgRXBhdGl0ZSBCIDIwMDgtICBWYWNjaW5vIGFudGkgcGFwaWxsb21hIHZpcnUgdW1hbm8gMjAwOSkgVGogRVQgUQowIFRyCi9HUzEwIGdzCjAgVHcgMCBUYyAxMDAgVHogMCBUTCAKMC45MDE5NjA3ODQzMTM3MjU1IDAuODk0MTE3NjQ3MDU4ODIzNiAwLjg5NDExNzY0NzA1ODgyMzYgcmcKMTQuMTcgMjc3LjgwIDE0MS43MyAtMTQuMTcgcmUgYgpxIDAgMCAwIHJnIEJUIDE3LjAxIDI2OC4zMSBUZCAoUGVzbyBcKEtnXCkpIFRqIEVUIFEKMTU1LjkxIDI3Ny44MCAxNDEuNzMgLTE0LjE3IHJlIGIKcSAwIDAgMCByZyBCVCAxNTguNzQgMjY4LjMxIFRkIChBbHRlenphIFwoY21cKSkgVGogRVQgUQoyOTcuNjQgMjc3LjgwIDE0MS43MyAtMTQuMTcgcmUgYgpxIDAgMCAwIHJnIEJUIDMwMC40NyAyNjguMzEgVGQgKEluZGljZSBNYXNzYSBDb3Jwb3JlYSkgVGogRVQgUQo0MzkuMzcgMjc3LjgwIDE0MS43MyAtMTQuMTcgcmUgYgpxIDAgMCAwIHJnIEJUIDQ0Mi4yMSAyNjguMzEgVGQgKEZ1bnppb25hbGl04CBQb2xtb25hcmkpIFRqIEVUIFEKMCBUcgovR1MxMSBncwowIFR3IDAgVGMgMTAwIFR6IDAgVEwgCjE0LjE3IDI2My42MiAxNDEuNzMgLTE0LjE3IHJlIFMKMTU1LjkxIDI2My42MiAxNDEuNzMgLTE0LjE3IHJlIFMKMjk3LjY0IDI2My42MiAxNDEuNzMgLTE0LjE3IHJlIFMKcSAwIDAgMCByZyBCVCAzMDAuNDcgMjU0LjE0IFRkICg8MTgsNSkgVGogRVQgUQo0MzkuMzcgMjYzLjYyIDE0MS43MyAtMTQuMTcgcmUgUwowIFRyCi9HUzEyIGdzCjAgVHcgMCBUYyAxMDAgVHogMCBUTCAKMC45MDE5NjA3ODQzMTM3MjU1IDAuODk0MTE3NjQ3MDU4ODIzNiAwLjg5NDExNzY0NzA1ODgyMzYgcmcKMTQuMTcgMjQ2LjYxIDE0MS43MyAtMjguMzUgcmUgYgpxIDAgMCAwIHJnIEJUIDE3LjAxIDIzNy4xMyBUZCAoUHJlc3Npb25lIEFydGVyaW9zYSBTaXN0b2xpY2EpIFRqIEVUIFEKcSAwIDAgMCByZyBCVCAxNy4wMSAyMjIuOTUgVGQgKFwobW1bSGddXCkpIFRqIEVUIFEKMTU1LjkxIDI0Ni42MSAxNDEuNzMgLTI4LjM1IHJlIGIKcSAwIDAgMCByZyBCVCAxNTguNzQgMjM3LjEzIFRkIChQcmVzc2lvbmUgQXJ0ZXJpb3NhIERpYXN0b2xpY2EpIFRqIEVUIFEKcSAwIDAgMCByZyBCVCAxNTguNzQgMjIyLjk1IFRkIChcKG1tW0hnXVwpKSBUaiBFVCBRCjI5Ny42NCAyNDYuNjEgMTQxLjczIC0yOC4zNSByZSBiCnEgMCAwIDAgcmcgQlQgMzAwLjQ3IDIzNy4xMyBUZCAoUHJlc3Npb25lIEFydGVyaW9zYSBNb2RhbGl04CBkaSkgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDMwMC40NyAyMjIuOTUgVGQgKE1pc3VyYXppb25lKSBUaiBFVCBRCjQzOS4zNyAyNDYuNjEgMTQxLjczIC0yOC4zNSByZSBiCnEgMCAwIDAgcmcgQlQgNDQyLjIxIDIzNy4xMyBUZCAoRnJlcXVlbnphIGRpIFJlc3BpcmF6aW9uZSkgVGogRVQgUQowIFRyCi9HUzEzIGdzCjAgVHcgMCBUYyAxMDAgVHogMCBUTCAKMTQuMTcgMjE4LjI3IDE0MS43MyAtMTQuMTcgcmUgUwoxNTUuOTEgMjE4LjI3IDE0MS43MyAtMTQuMTcgcmUgUwoyOTcuNjQgMjE4LjI3IDE0MS43MyAtMTQuMTcgcmUgUwo0MzkuMzcgMjE4LjI3IDE0MS43MyAtMTQuMTcgcmUgUwowLjggMSAwLjggcmcKQlQgL0YyIDEwLjAwIFRmIEVUCjE0LjE3IDE4OS45MiA1NjYuOTMgLTE0LjE3IHJlIGYgcSAwIDAgMCByZyBCVCAxNy4wMSAxNzkuODMgVGQgKFZpdGEgc29jaWFsZSkgVGogRVQgUQpCVCAvRjIgOC4wMCBUZiBFVAowIFRyCi9HUzE0IGdzCjAgVHcgMCBUYyAxMDAgVHogMCBUTCAKMC45MDE5NjA3ODQzMTM3MjU1IDAuODk0MTE3NjQ3MDU4ODIzNiAwLjg5NDExNzY0NzA1ODgyMzYgcmcKMTQuMTcgMTcyLjkxIDI4My40NiAtMTQuMTcgcmUgYgpxIDAgMCAwIHJnIEJUIDE3LjAxIDE2My40MyBUZCAoRWxlbWVudG8pIFRqIEVUIFEKMjk3LjY0IDE3Mi45MSAyODMuNDYgLTE0LjE3IHJlIGIKcSAwIDAgMCByZyBCVCAzMDAuNDcgMTYzLjQzIFRkIChEZXNjcml6aW9uZSkgVGogRVQgUQowIFRyCi9HUzE1IGdzCjAgVHcgMCBUYyAxMDAgVHogMCBUTCAKMTQuMTcgMTU4Ljc0IDI4My40NiAtMTQuMTcgcmUgUwpxIDAgMCAwIHJnIEJUIDE3LjAxIDE0OS4yNSBUZCAoRnVtbykgVGogRVQgUQoyOTcuNjQgMTU4Ljc0IDI4My40NiAtMTQuMTcgcmUgUwpxIDAgMCAwIHJnIEJUIDMwMC40NyAxNDkuMjUgVGQgKDIwKSBUaiBFVCBRCjAgVHIKL0dTMTYgZ3MKMCBUdyAwIFRjIDEwMCBUeiAwIFRMIAowLjk1Mjk0MTE3NjQ3MDU4ODIgMSAwLjk4MDM5MjE1Njg2Mjc0NTEgcmcKMTQuMTcgMTQ0LjU3IDI4My40NiAtMTQuMTcgcmUgYgpxIDAgMCAwIHJnIEJUIDE3LjAxIDEzNS4wOCBUZCAoQWxjb29sKSBUaiBFVCBRCjI5Ny42NCAxNDQuNTcgMjgzLjQ2IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMzAwLjQ3IDEzNS4wOCBUZCAoMikgVGogRVQgUQowIFRyCi9HUzE3IGdzCjAgVHcgMCBUYyAxMDAgVHogMCBUTCAKMTQuMTcgMTMwLjM5IDI4My40NiAtMTQuMTcgcmUgUwpxIDAgMCAwIHJnIEJUIDE3LjAxIDEyMC45MSBUZCAoRGlldGUgaW4gYXR0bykgVGogRVQgUQoyOTcuNjQgMTMwLjM5IDI4My40NiAtMTQuMTcgcmUgUwowIFRyCi9HUzE4IGdzCjAgVHcgMCBUYyAxMDAgVHogMCBUTCAKMC45NTI5NDExNzY0NzA1ODgyIDEgMC45ODAzOTIxNTY4NjI3NDUxIHJnCjE0LjE3IDExNi4yMiAyODMuNDYgLTE0LjE3IHJlIGIKcSAwIDAgMCByZyBCVCAxNy4wMSAxMDYuNzMgVGQgKEF0dGl2aXRhIEZpc2ljYSkgVGogRVQgUQoyOTcuNjQgMTE2LjIyIDI4My40NiAtMTQuMTcgcmUgYgowIFRyCi9HUzE5IGdzCjAgVHcgMCBUYyAxMDAgVHogMCBUTCAKMTQuMTcgMTAyLjA1IDI4My40NiAtMTQuMTcgcmUgUwpxIDAgMCAwIHJnIEJUIDE3LjAxIDkyLjU2IFRkIChQcm9mZXNzaW9uZSkgVGogRVQgUQoyOTcuNjQgMTAyLjA1IDI4My40NiAtMTQuMTcgcmUgUwpxIDAgMCAwIHJnIEJUIDMwMC40NyA5Mi41NiBUZCAoSW1waWVnYXRpIGEgY29udGF0dG8gZGlyZXR0byBjb24gaWwgcHViYmxpY28pIFRqIEVUIFEKMCBUcgovR1MyMCBncwowIFR3IDAgVGMgMTAwIFR6IDAgVEwgCjAuOTUyOTQxMTc2NDcwNTg4MiAxIDAuOTgwMzkyMTU2ODYyNzQ1MSByZwoxNC4xNyA4Ny44NyAyODMuNDYgLTE0LjE3IHJlIGIKcSAwIDAgMCByZyBCVCAxNy4wMSA3OC4zOSBUZCAoRXNwb3NpemlvbmUgQWdlbnRpIFRvc3NpY2kpIFRqIEVUIFEKMjk3LjY0IDg3Ljg3IDI4My40NiAtMTQuMTcgcmUgYgpxIDAgMCAwIHJnIEJUIDMwMC40NyA3OC4zOSBUZCAoVXRpbGl6em8gZWQgZXNwb3NpemlvbmUgYWwgdGFiYWNjbykgVGogRVQgUQplbmRzdHJlYW0KZW5kb2JqCjUgMCBvYmoKPDwvVHlwZSAvUGFnZQovUGFyZW50IDEgMCBSCi9NZWRpYUJveCBbMCAwIDU5NS4yOCA4NDEuODldCi9SZXNvdXJjZXMgMiAwIFIKL1JvdGF0ZSAwCi9EdXIgMwovQ29udGVudHMgNiAwIFI+PgplbmRvYmoKNiAwIG9iago8PC9MZW5ndGggMjcxMT4+CnN0cmVhbQoyIEoKMC41NyB3CkJUIC9GMiA4LjAwIFRmIEVUCjAuOCAxIDAuOCByZwpCVCAvRjIgMTAuMDAgVGYgRVQKMTQuMTcgODEzLjU0IDU2Ni45MyAtMTQuMTcgcmUgZiBxIDAgMCAwIHJnIEJUIDE3LjAxIDgwMy40NiBUZCAoR3JhdmlkYW56ZSBlIHBhcnRvKSBUaiBFVCBRCkJUIC9GMiA4LjAwIFRmIEVUCjAgVHIKL0dTMjEgZ3MKMCBUdyAwIFRjIDEwMCBUeiAwIFRMIAowLjkwMTk2MDc4NDMxMzcyNTUgMC44OTQxMTc2NDcwNTg4MjM2IDAuODk0MTE3NjQ3MDU4ODIzNiByZwoxNC4xNyA3OTYuNTQgMTg3LjA5IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMTcuMDEgNzg3LjA1IFRkIChNZXNlL0Fubm8gXChtbS9hYVwpKSBUaiBFVCBRCjIwMS4yNiA3OTYuNTQgMTg3LjA5IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMjA0LjA5IDc4Ny4wNSBUZCAoRXNpdG8pIFRqIEVUIFEKMzg4LjM1IDc5Ni41NCAxODcuMDkgLTE0LjE3IHJlIGIKcSAwIDAgMCByZyBCVCAzOTEuMTggNzg3LjA1IFRkIChDb21wbGljYXppb25pKSBUaiBFVCBRCjAgVHIKL0dTMjIgZ3MKMCBUdyAwIFRjIDEwMCBUeiAwIFRMIAoxNC4xNyA3ODIuMzYgMTg3LjA5IC0xNC4xNyByZSBTCjIwMS4yNiA3ODIuMzYgMTg3LjA5IC0xNC4xNyByZSBTCjM4OC4zNSA3ODIuMzYgMTg3LjA5IC0xNC4xNyByZSBTCjAuOCAxIDAuOCByZwpCVCAvRjIgMTAuMDAgVGYgRVQKMTQuMTcgNzU0LjAyIDU2Ni45MyAtMTQuMTcgcmUgZiBxIDAgMCAwIHJnIEJUIDE3LjAxIDc0My45MyBUZCAoSW5kYWdpbmkgRGlhZ25vc3RpY2hlKSBUaiBFVCBRCkJUIC9GMiA4LjAwIFRmIEVUCjAgVHIKL0dTMjMgZ3MKMCBUdyAwIFRjIDEwMCBUeiAwIFRMIAowLjkwMTk2MDc4NDMxMzcyNTUgMC44OTQxMTc2NDcwNTg4MjM2IDAuODk0MTE3NjQ3MDU4ODIzNiByZwoxNC4xNyA3MzcuMDEgMTg3LjA5IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMTcuMDEgNzI3LjUyIFRkIChEYXRhIEluZGFnaW5lKSBUaiBFVCBRCjIwMS4yNiA3MzcuMDEgMTg3LjA5IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMjA0LjA5IDcyNy41MiBUZCAoVGlwb2xvZ2lhIGRpIEluZGFnaW5lKSBUaiBFVCBRCjM4OC4zNSA3MzcuMDEgMTg3LjA5IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMzkxLjE4IDcyNy41MiBUZCAoRXNpdG8pIFRqIEVUIFEKMCBUcgovR1MyNCBncwowIFR3IDAgVGMgMTAwIFR6IDAgVEwgCjE0LjE3IDcyMi44MyAxODcuMDkgLTE0LjE3IHJlIFMKMjAxLjI2IDcyMi44MyAxODcuMDkgLTE0LjE3IHJlIFMKMzg4LjM1IDcyMi44MyAxODcuMDkgLTE0LjE3IHJlIFMKMC44IDEgMC44IHJnCkJUIC9GMiAxMC4wMCBUZiBFVAoxNC4xNyA2OTQuNDkgNTY2LjkzIC0xNC4xNyByZSBmIHEgMCAwIDAgcmcgQlQgMTcuMDEgNjg0LjQwIFRkIChQcm9jZWR1cmUgRGlhZ25vc3RpY2hlKSBUaiBFVCBRCkJUIC9GMiA4LjAwIFRmIEVUCjAgVHIKL0dTMjUgZ3MKMCBUdyAwIFRjIDEwMCBUeiAwIFRMIAowLjkwMTk2MDc4NDMxMzcyNTUgMC44OTQxMTc2NDcwNTg4MjM2IDAuODk0MTE3NjQ3MDU4ODIzNiByZwoxNC4xNyA2NzcuNDggMTg3LjA5IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMTcuMDEgNjY3Ljk5IFRkIChEYXRhKSBUaiBFVCBRCjIwMS4yNiA2NzcuNDggMTg3LjA5IC0xNC4xNyByZSBiCnEgMCAwIDAgcmcgQlQgMjA0LjA5IDY2Ny45OSBUZCAoVGlwbyBQcm9jZWR1cmEvSW50ZXJ2ZW50bykgVGogRVQgUQozODguMzUgNjc3LjQ4IDE4Ny4wOSAtMTQuMTcgcmUgYgpxIDAgMCAwIHJnIEJUIDM5MS4xOCA2NjcuOTkgVGQgKEVzaXRvKSBUaiBFVCBRCjAgVHIKL0dTMjYgZ3MKMCBUdyAwIFRjIDEwMCBUeiAwIFRMIAoxNC4xNyA2NjMuMzEgMTg3LjA5IC0xNC4xNyByZSBTCjIwMS4yNiA2NjMuMzEgMTg3LjA5IC0xNC4xNyByZSBTCjM4OC4zNSA2NjMuMzEgMTg3LjA5IC0xNC4xNyByZSBTCjAuOCAxIDAuOCByZwpCVCAvRjIgMTAuMDAgVGYgRVQKMTQuMTcgNjM0Ljk2IDU2Ni45MyAtMTQuMTcgcmUgZiBxIDAgMCAwIHJnIEJUIDE3LjAxIDYyNC44NyBUZCAoRGF0aSBGaXJtYXRhcmlvKSBUaiBFVCBRCkJUIC9GMiA4LjAwIFRmIEVUCnEgMCAwIDAgcmcgQlQgMTcuMDEgNjA4LjQ3IFRkIChOb21lOikgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDEwMi4wNSA2MDguNDcgVGQgKE1NRykgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDMwMC40NyA2MDguNDcgVGQgKE9yZ2FuaXp6YXppb25lOikgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDM4NS41MSA2MDguNDcgVGQgKFJlZ2lvbmUgTG9tYmFyZGlhKSBUaiBFVCBRCnEgMCAwIDAgcmcgQlQgMTcuMDEgNTk0LjI5IFRkIChDb2dub21lOikgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDEwMi4wNSA1OTQuMjkgVGQgKENOSVBBVklJKSBUaiBFVCBRCnEgMCAwIDAgcmcgQlQgMzAwLjQ3IDU5NC4yOSBUZCAoRGVzYyBTdHJ1dHR1cmE6KSBUaiBFVCBRCnEgMCAwIDAgcmcgQlQgMzg1LjUxIDU5NC4yOSBUZCAoQVNMIERJIE1JTEFOTykgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDE3LjAxIDU4MC4xMiBUZCAoQ29kaWNlIEZpc2NhbGU6KSBUaiBFVCBRCnEgMCAwIDAgcmcgQlQgMTAyLjA1IDU4MC4xMiBUZCAoQ05QTU1HNjBMMDFGMjA1VikgVGogRVQgUQpxIDAgMCAwIHJnIEJUIDMwMC40NyA1ODAuMTIgVGQgKENvZGljZSBTdHJ1dHR1cmE6KSBUaiBFVCBRCnEgMCAwIDAgcmcgQlQgMzg1LjUxIDU4MC4xMiBUZCAoMDMwMzA4KSBUaiBFVCBRCmVuZHN0cmVhbQplbmRvYmoKNyAwIG9iago8PC9UeXBlIC9FeHRHU3RhdGUKL0JNIC9Ob3JtYWwKL24gNwovU0EgdHJ1ZQovQ0EgMQovY2EgMQo+PgplbmRvYmoKOCAwIG9iago8PC9UeXBlIC9FeHRHU3RhdGUKL0JNIC9Ob3JtYWwKL24gOAovU0EgdHJ1ZQovQ0EgMQovY2EgMQo+PgplbmRvYmoKOSAwIG9iago8PC9UeXBlIC9FeHRHU3RhdGUKL0JNIC9Ob3JtYWwKL24gOQovU0EgdHJ1ZQovQ0EgMQovY2EgMQo+PgplbmRvYmoKMTAgMCBvYmoKPDwvVHlwZSAvRXh0R1N0YXRlCi9CTSAvTm9ybWFsCi9uIDEwCi9TQSB0cnVlCi9DQSAxCi9jYSAxCj4+CmVuZG9iagoxMSAwIG9iago8PC9UeXBlIC9FeHRHU3RhdGUKL0JNIC9Ob3JtYWwKL24gMTEKL1NBIHRydWUKL0NBIDEKL2NhIDEKPj4KZW5kb2JqCjEyIDAgb2JqCjw8L1R5cGUgL0V4dEdTdGF0ZQovQk0gL05vcm1hbAovbiAxMgovU0EgdHJ1ZQovQ0EgMQovY2EgMQo+PgplbmRvYmoKMTMgMCBvYmoKPDwvVHlwZSAvRXh0R1N0YXRlCi9CTSAvTm9ybWFsCi9uIDEzCi9TQSB0cnVlCi9DQSAxCi9jYSAxCj4+CmVuZG9iagoxNCAwIG9iago8PC9UeXBlIC9FeHRHU3RhdGUKL0JNIC9Ob3JtYWwKL24gMTQKL1NBIHRydWUKL0NBIDEKL2NhIDEKPj4KZW5kb2JqCjE1IDAgb2JqCjw8L1R5cGUgL0V4dEdTdGF0ZQovQk0gL05vcm1hbAovbiAxNQovU0EgdHJ1ZQovQ0EgMQovY2EgMQo+PgplbmRvYmoKMTYgMCBvYmoKPDwvVHlwZSAvRXh0R1N0YXRlCi9CTSAvTm9ybWFsCi9uIDE2Ci9TQSB0cnVlCi9DQSAxCi9jYSAxCj4+CmVuZG9iagoxNyAwIG9iago8PC9UeXBlIC9FeHRHU3RhdGUKL0JNIC9Ob3JtYWwKL24gMTcKL1NBIHRydWUKL0NBIDEKL2NhIDEKPj4KZW5kb2JqCjE4IDAgb2JqCjw8L1R5cGUgL0V4dEdTdGF0ZQovQk0gL05vcm1hbAovbiAxOAovU0EgdHJ1ZQovQ0EgMQovY2EgMQo+PgplbmRvYmoKMTkgMCBvYmoKPDwvVHlwZSAvRXh0R1N0YXRlCi9CTSAvTm9ybWFsCi9uIDE5Ci9TQSB0cnVlCi9DQSAxCi9jYSAxCj4+CmVuZG9iagoyMCAwIG9iago8PC9UeXBlIC9FeHRHU3RhdGUKL0JNIC9Ob3JtYWwKL24gMjAKL1NBIHRydWUKL0NBIDEKL2NhIDEKPj4KZW5kb2JqCjIxIDAgb2JqCjw8L1R5cGUgL0V4dEdTdGF0ZQovQk0gL05vcm1hbAovbiAyMQovU0EgdHJ1ZQovQ0EgMQovY2EgMQo+PgplbmRvYmoKMjIgMCBvYmoKPDwvVHlwZSAvRXh0R1N0YXRlCi9CTSAvTm9ybWFsCi9uIDIyCi9TQSB0cnVlCi9DQSAxCi9jYSAxCj4+CmVuZG9iagoyMyAwIG9iago8PC9UeXBlIC9FeHRHU3RhdGUKL0JNIC9Ob3JtYWwKL24gMjMKL1NBIHRydWUKL0NBIDEKL2NhIDEKPj4KZW5kb2JqCjI0IDAgb2JqCjw8L1R5cGUgL0V4dEdTdGF0ZQovQk0gL05vcm1hbAovbiAyNAovU0EgdHJ1ZQovQ0EgMQovY2EgMQo+PgplbmRvYmoKMjUgMCBvYmoKPDwvVHlwZSAvRXh0R1N0YXRlCi9CTSAvTm9ybWFsCi9uIDI1Ci9TQSB0cnVlCi9DQSAxCi9jYSAxCj4+CmVuZG9iagoyNiAwIG9iago8PC9UeXBlIC9FeHRHU3RhdGUKL0JNIC9Ob3JtYWwKL24gMjYKL1NBIHRydWUKL0NBIDEKL2NhIDEKPj4KZW5kb2JqCjI3IDAgb2JqCjw8L1R5cGUgL0V4dEdTdGF0ZQovQk0gL05vcm1hbAovbiAyNwovU0EgdHJ1ZQovQ0EgMQovY2EgMQo+PgplbmRvYmoKMjggMCBvYmoKPDwvVHlwZSAvRXh0R1N0YXRlCi9CTSAvTm9ybWFsCi9uIDI4Ci9TQSB0cnVlCi9DQSAxCi9jYSAxCj4+CmVuZG9iagoyOSAwIG9iago8PC9UeXBlIC9FeHRHU3RhdGUKL0JNIC9Ob3JtYWwKL24gMjkKL1NBIHRydWUKL0NBIDEKL2NhIDEKPj4KZW5kb2JqCjMwIDAgb2JqCjw8L1R5cGUgL0V4dEdTdGF0ZQovQk0gL05vcm1hbAovbiAzMAovU0EgdHJ1ZQovQ0EgMQovY2EgMQo+PgplbmRvYmoKMzEgMCBvYmoKPDwvVHlwZSAvRXh0R1N0YXRlCi9CTSAvTm9ybWFsCi9uIDMxCi9TQSB0cnVlCi9DQSAxCi9jYSAxCj4+CmVuZG9iagozMiAwIG9iago8PC9UeXBlIC9FeHRHU3RhdGUKL0JNIC9Ob3JtYWwKL24gMzIKL1NBIHRydWUKL0NBIDEKL2NhIDEKPj4KZW5kb2JqCjMzIDAgb2JqCjw8L1R5cGUgL0V4dEdTdGF0ZQovQk0gL05vcm1hbAovbiAzMwovU0EgdHJ1ZQovQ0EgMQovY2EgMQo+PgplbmRvYmoKMzQgMCBvYmoKPDwvVHlwZSAvRm9udAovQmFzZUZvbnQgL0hlbHZldGljYQovU3VidHlwZSAvVHlwZTEKL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcKPj4KZW5kb2JqCjM1IDAgb2JqCjw8L1R5cGUgL0ZvbnQKL0Jhc2VGb250IC9IZWx2ZXRpY2EtQm9sZAovU3VidHlwZSAvVHlwZTEKL0VuY29kaW5nIC9XaW5BbnNpRW5jb2RpbmcKPj4KZW5kb2JqCjM2IDAgb2JqCjw8L1R5cGUgL1hPYmplY3QKL1N1YnR5cGUgL0ltYWdlCi9XaWR0aCAxMjQKL0hlaWdodCAxMjQKL0NvbG9yU3BhY2UgL0RldmljZVJHQgovQml0c1BlckNvbXBvbmVudCA4Ci9GaWx0ZXIgL0RDVERlY29kZQovTGVuZ3RoIDIxNjM+PgpzdHJlYW0K/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJx8fLT0tMTU3Ojo6Iys/RD84QzQ5Ojf/2wBDAQoKCg0MDRoPDxo3JR8lNzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzc3Nzf/wAARCAB8AHwDASIAAhEBAxEB/8QAGwABAAMBAQEBAAAAAAAAAAAAAAEEBwYFAgP/xAA3EAABAwICBgkDAwQDAAAAAAABAAIDBBEFBhIhMkFRcQcTFDEzYXKBkSKh0RVSsSNCQ/BiwcL/xAAaAQACAwEBAAAAAAAAAAAAAAAAAQIEBQYD/8QAIxEAAgICAgMAAwEBAAAAAAAAAAECAxExBBIFIUETUXEikf/aAAwDAQACEQMRAD8AoyeI71FfKmTxHeoqFypwL2ERECCIiQBERABERABERABERAwrNNsHmqys02weaktkobK8niO9RUKZPEd6ioSIvZClESEFClEDCIvZyjhbMXxyCnmF4W3klHFo3e5sPdThFzkor6Tqrdk1CO2feCZUxTGIxNDGyGA90sxsHcgNZ/hepU9HuJxRl0FRTTuH9gJaTyuLfwtOYxrGhrWhrQAAALABfS2Y+OqUcPZ0sPD8dRxLLf7MEqqaaknfBUxPilYbOY8WIX5LUekXCI6rCDXsYBUUtvqA2mE2IPK9/nisuWXyaHTPqYPN4r41vTa+BERVyqFZptg81WVmm2DzUlslDZXk8R3qKhTJ4jvUVCRF7C9XCMu4pi7dOjpiYr26150W+xPf7L9MpYS3Gcaip5b9QwGSW29o3e5IHutkijZFG2ONoYxoAa1osAOAV7icNXLtLRq+P8cuQnObxEyWsyRjlNGXiCOcAXIhk0j8G1/Zc69rmOLXgtcDYgixBW/rhukbAWy036tSx/1oyBPojab+4+Y48OS9eTwFCHav4WOb4mNdbsqb9fDOF0/R1Usp8yMY/wDzxOiB89Tv/K5he9kiilrMx0hjadCB3WyO/aB+TYKjx21bHH7MrhuS5EOu8o2IIoHcvKx3MFDgcTXVjyZH7ETBdzvwPMro5SjBZk8I7Sc4wj2k8Iq56qo6bLNWHn6pgImDiSfwCfZY+vYzJmCpx6qD5R1cEd+qhBvo+ZO8rx1gcu9XWZjpHJeR5UeRdmOl6CIiqlAKzTbB5qsrNNsHmpLZKGz8JPEd6ivlTJ4jvUVCRF7Ou6M52RY7LE8gGanIZ5kEG3wD8LUtywSkqZqSpiqKd5ZLE4OY4bitPwTPOG1kLW17xR1AFnB99Bx4g7uR+61eByIRh+OTwdB4nmVxr/FN4fw6xQ5ocCHAEHvB3rxarNmB00Ze7EYZP+MJ0yfhVqHO+C1k4h62SBzjYGdmi0+9yB72Wg76k8dka75VCl1c1n+nzV5GwSomMohlhublkMlm/Bvb2Xs4VhVFhUHU0MDYmk3cRrLjxJOsq4DcXClONNcX2ikmOHHqhLtGKTIPcsYzfUy1OZK90pJ0JTG0cGt1D8+62hcLnTKE9dVuxDCw18rwOthJALiNVwTq7u8Ktz6p2V/5+FLytFltK6e8PRnKL3aXKGO1Euh2B8Q3ulIaB+fZXpsgYzHEXsdSyuA2GSEH7gD7rHXHtayos52PD5EllQf/AA5RF+tVSz0c7oKqJ8UrNpjxYhfkvJrHplZpp4YVmm2DzVZWabYPNNbJQ2V5PEd6ioUyeI71FQkRewiIkIIivYVhFdi03V0NO6TXZz+5rOZ3KUYuTwkThGU31iss0zo+rZazLsYncXOgkdEHHe0WI+Abey6Zebl/CmYNhUNGx2m5gJe/u0nHWSv0xrEosJw2atmF2xjU0d7idQHyujrzCpd/i9naU5qoj+R6Xsu3CaljVfmvGq2d0hrpYG3+mOBxY1vxrPuugydnCrdXRUGKy9dHMdCOZ20124E7we7iq8OfXOfUpVeWoss6Yaz9NGslkRXjVOYz7g0eIYNLUtYBU0jDIxwGstGtzfi55rJltuZahlLgNfLIbAQPaPMkWA+SFiSxfIxirE1tnM+ahFXRa217Cs02wearKzTbB5qgtmTDZXk8R3qKhTJ4jvUVCRF7CIiQi9geHOxbFaeia7R6131O/a0C5PwCtqoaOCgpo6akjbHDGLNaP91nzWV9H0zIszQB9v6kb2NPna//AEtcWz42Eejl9ydL4WuKqc/ucBc10h00lRlqYx/4XtkcOIBsf5v7LpV5GbZmQ5bxF0hFjA5ovvLtQ+5Cu3pOqSf6NPkxUqZp/pmLICQQQbEbwm9FzJxBouXM+QGFlPjRdHK0W7Q1t2v83AawftyXvT5vwGKMv/UGP1amsa5xPtZY4ivw8hbGONmpX5fkQh1eH/TpM25pkx1zYIGOiomOuGk/U88Xfhc2iKnZZKyXaWzPuundNzm8sKzTbB5qsrNNsHmktkYbK8niO9RUKZPEd6ioSIvYRESEfcEskEzJoXFkkbg5rh3gjuK1LL+dqCugZHiErKSqAs7TNmOPEHdyP3WVIrFHInS8xLnE5lnGbcdP4bdU49hNPEZJcRpQ3ylDieQGsrOc5Zp/WnNpaMOZRRu0vq1GR3EjcOA/0cui9b+bO2PXSPfk+Utvh0xhBERUjNCIiBBERAwrNNsHmqys02weaktkobPwk8R3qK+VbfTM6x2t3eV89mZxcjqwcXkrIrPZmcXJ2ZnFyMMOrKyKz2ZnFydmZxcjDDqysitdmZxco7Mzi5GGHVlZFZ7Mzi5OzM4uRhh1ZWRWezM4uTszOLkYYdWVkVnszOLk7Mzi5GGHVlZWabYPNT2ZnFys01MzQOt3enGPslCLyf/ZCmVuZHN0cmVhbQplbmRvYmoKMiAwIG9iago8PAovUHJvY1NldCBbL1BERiAvVGV4dCAvSW1hZ2VCIC9JbWFnZUMgL0ltYWdlSV0KL0ZvbnQgPDwKL0YyIDM0IDAgUgovRjEgMzUgMCBSCj4+Ci9YT2JqZWN0IDw8Ci9JMSAzNiAwIFIKPj4KL0V4dEdTdGF0ZSA8PAovR1MwIDcgMCBSCi9HUzEgOCAwIFIKL0dTMiA5IDAgUgovR1MzIDEwIDAgUgovR1M0IDExIDAgUgovR1M1IDEyIDAgUgovR1M2IDEzIDAgUgovR1M3IDE0IDAgUgovR1M4IDE1IDAgUgovR1M5IDE2IDAgUgovR1MxMCAxNyAwIFIKL0dTMTEgMTggMCBSCi9HUzEyIDE5IDAgUgovR1MxMyAyMCAwIFIKL0dTMTQgMjEgMCBSCi9HUzE1IDIyIDAgUgovR1MxNiAyMyAwIFIKL0dTMTcgMjQgMCBSCi9HUzE4IDI1IDAgUgovR1MxOSAyNiAwIFIKL0dTMjAgMjcgMCBSCi9HUzIxIDI4IDAgUgovR1MyMiAyOSAwIFIKL0dTMjMgMzAgMCBSCi9HUzI0IDMxIDAgUgovR1MyNSAzMiAwIFIKL0dTMjYgMzMgMCBSCj4+Cj4+CmVuZG9iagozNyAwIG9iago8PAovUHJvZHVjZXIgKEFsaXZlIFBERiAwLjEuNC44KQovVGl0bGUgKFNjaGVkYSBQYXRpZW50IFN1bW1hcnkpCi9TdWJqZWN0IChTY2hlZGEgUGF0aWVudCBTdW1tYXJ5KQovQXV0aG9yIChNTUcgQ05JUEFWSUkpCi9DcmVhdGlvbkRhdGUgKEQ6MjAxMTAxMDcxMTU0KQo+PgplbmRvYmoKMzggMCBvYmoKPDwKL1R5cGUgL0NhdGFsb2cKL1BhZ2VzIDEgMCBSCi9PcGVuQWN0aW9uIFszIDAgUiAvRml0SCBudWxsXQovUGFnZUxheW91dCAvU2luZ2xlUGFnZQo+PgplbmRvYmoKeHJlZgowIDM5CjAwMDAwMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDAwOSAwMDAwMCBuIAowMDAwMDE2MTg4IDAwMDAwIG4gCjAwMDAwMDAwNzIgMDAwMDAgbiAKMDAwMDAwMDE5NyAwMDAwMCBuIAowMDAwMDA4NzAxIDAwMDAwIG4gCjAwMDAwMDg4MjYgMDAwMDAgbiAKMDAwMDAxMTU4NiAwMDAwMCBuIAowMDAwMDExNjYxIDAwMDAwIG4gCjAwMDAwMTE3MzYgMDAwMDAgbiAKMDAwMDAxMTgxMSAwMDAwMCBuIAowMDAwMDExODg4IDAwMDAwIG4gCjAwMDAwMTE5NjUgMDAwMDAgbiAKMDAwMDAxMjA0MiAwMDAwMCBuIAowMDAwMDEyMTE5IDAwMDAwIG4gCjAwMDAwMTIxOTYgMDAwMDAgbiAKMDAwMDAxMjI3MyAwMDAwMCBuIAowMDAwMDEyMzUwIDAwMDAwIG4gCjAwMDAwMTI0MjcgMDAwMDAgbiAKMDAwMDAxMjUwNCAwMDAwMCBuIAowMDAwMDEyNTgxIDAwMDAwIG4gCjAwMDAwMTI2NTggMDAwMDAgbiAKMDAwMDAxMjczNSAwMDAwMCBuIAowMDAwMDEyODEyIDAwMDAwIG4gCjAwMDAwMTI4ODkgMDAwMDAgbiAKMDAwMDAxMjk2NiAwMDAwMCBuIAowMDAwMDEzMDQzIDAwMDAwIG4gCjAwMDAwMTMxMjAgMDAwMDAgbiAKMDAwMDAxMzE5NyAwMDAwMCBuIAowMDAwMDEzMjc0IDAwMDAwIG4gCjAwMDAwMTMzNTEgMDAwMDAgbiAKMDAwMDAxMzQyOCAwMDAwMCBuIAowMDAwMDEzNTA1IDAwMDAwIG4gCjAwMDAwMTM1ODIgMDAwMDAgbiAKMDAwMDAxMzY1OSAwMDAwMCBuIAowMDAwMDEzNzU2IDAwMDAwIG4gCjAwMDAwMTM4NTggMDAwMDAgbiAKMDAwMDAxNjY3MCAwMDAwMCBuIAowMDAwMDE2ODQyIDAwMDAwIG4gCnRyYWlsZXIKPDwKL1NpemUgMzkKL1Jvb3QgMzggMCBSCi9JbmZvIDM3IDAgUgo+PgpzdGFydHhyZWYKMTY5NDcKJSVFT0YK</text>\n" +
                "    </nonXMLBody>\n" +
                "  </component>\n" +
                "</ClinicalDocument>\n", new String(base64Converter.base64Decoding(fileContent),StandardCharsets.UTF_8));
    }
}
