package net.ihe.gazelle.mca.contentanalyzer.application.analyzers;


import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class MessageSplitterTest {

    MessageSplitter messageSplitter;
    MessageContentAnalyzer m;

    @Before
    public void setUp() {
        messageSplitter = new ApplicationTestFactory().getMessageSplitter();
        m = new ApplicationTestFactory().getMessageContentAnalyzer();
    }

    @Test
    public void splitMTOMFile() {

        final File file = loadFile("/contentanalyzer/validHttpBody.xml");
        final AnalysisPart parent = new AnalysisPart(new AnalysisPart(null));
        parent.setDocType("MTOM");
        parent.setStartOffset(0);
        try {
            parent.setEndOffset(FileUtils.readFileToString(file).length());
            m.setMessageByteContent(FileUtils.readFileToByteArray(file));
            messageSplitter.split(m, parent);
        } catch(IOException e) {
            System.out.println(e.getMessage());
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }


        assertEquals("MTOM part 1", parent.getChildPart().get(0).getDocType());

        assertEquals(0, parent.getChildPart().get(0).getStartOffset());
        assertEquals(18706, parent.getChildPart().get(0).getEndOffset());

        assertEquals("XML", parent.getChildPart().get(0).getChildPart().get(0).getDocType());

        assertEquals(276, parent.getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(18706, parent.getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", parent.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getDocType());

        assertEquals(315, parent.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(18703, parent.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", parent.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(0).getDocType());

        assertEquals(457, parent.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(746, parent.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", parent.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getDocType());

        assertEquals(747, parent.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(18683, parent.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getEndOffset());

        assertEquals("XDS", parent.getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getDocType());

        assertEquals(766, parent.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getStartOffset());
        assertEquals(18667, parent.getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 2", parent.getChildPart().get(1).getDocType());

        assertEquals(18706, parent.getChildPart().get(1).getStartOffset());
        assertEquals(18974, parent.getChildPart().get(1).getEndOffset());
    }

    @Test
    public void splitMTOMSubpart() {

        final File file = loadFile("/contentanalyzer/validMtomSubpart.xml");
        final AnalysisPart parent = new AnalysisPart(new AnalysisPart(null));
        parent.setDocType("MTOM");
        parent.setStartOffset(0);
        try {
            parent.setEndOffset(FileUtils.readFileToString(file).length());
            m.setMessageByteContent(FileUtils.readFileToByteArray(file));
            messageSplitter.split(m, parent);
        } catch(IOException e) {
            System.out.println(e.getMessage());
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("MTOM part 1", parent.getChildPart().get(0).getDocType());

        assertEquals(0, parent.getChildPart().get(0).getStartOffset());
        assertEquals(1549, parent.getChildPart().get(0).getEndOffset());
    }

    @Test
    public void splitMTOMMultipleSubpart() {

        final File file = loadFile("/contentanalyzer/validMtomMultipleSubpart.xml");
        final AnalysisPart parent = new AnalysisPart(new AnalysisPart(null));
        parent.setDocType("MTOM");
        parent.setStartOffset(0);
        try {
            parent.setEndOffset(FileUtils.readFileToString(file).length());
            m.setMessageByteContent(FileUtils.readFileToByteArray(file));
            messageSplitter.split(m, parent);
        } catch(IOException e) {
            System.out.println(e.getMessage());
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("MTOM part 1", parent.getChildPart().get(0).getDocType());

        assertEquals(0, parent.getChildPart().get(0).getStartOffset());
        assertEquals(1561, parent.getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 2", parent.getChildPart().get(1).getDocType());

        assertEquals(1561, parent.getChildPart().get(1).getStartOffset());
        assertEquals(6574, parent.getChildPart().get(1).getEndOffset());
    }

    @Test
    public void splitHttpFile() {

        final File file = loadFile("/contentanalyzer/validHttpFile.xml");
        final AnalysisPart parent = new AnalysisPart(new AnalysisPart(null));
        parent.setDocType("HTTP");
        parent.setStartOffset(0);
        try {
            parent.setEndOffset(FileUtils.readFileToString(file).length());
            m.setMessageByteContent(FileUtils.readFileToByteArray(file));
            messageSplitter.split(m, parent);
        } catch(IOException e) {
            System.out.println(e.getMessage());
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }


        assertEquals("Http Header", parent.getChildPart().get(0).getDocType());

        assertEquals(0, parent.getChildPart().get(0).getStartOffset());
        assertEquals(391, parent.getChildPart().get(0).getEndOffset());

        assertEquals("Http Body", parent.getChildPart().get(1).getDocType());

        assertEquals(391, parent.getChildPart().get(1).getStartOffset());
        assertEquals(19429, parent.getChildPart().get(1).getEndOffset());

        assertEquals("MTOM", parent.getChildPart().get(1)
                .getChildPart().get(0).getDocType());

        assertEquals(391, parent.getChildPart().get(1)
                .getChildPart().get(0).getStartOffset());
        assertEquals(19429, parent.getChildPart().get(1)
                .getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 1", parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getDocType());

        assertEquals(391, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(19097, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("XML", parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getDocType());

        assertEquals(667, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getStartOffset());
        assertEquals(19097, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Envelope", parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getDocType());

        assertEquals(706, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(19094, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Header", parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(0).getDocType());

        assertEquals(848, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getStartOffset());
        assertEquals(1137, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getEndOffset());

        assertEquals("SOAP Body", parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getDocType());

        assertEquals(1138, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(19074, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getEndOffset());

        assertEquals("XDS", parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0).getChildPart()
                .get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getDocType());

        assertEquals(1157, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getStartOffset());
        assertEquals(19058, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1)
                .getChildPart().get(0).getEndOffset());

        assertEquals("MTOM part 2", parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getDocType());

        assertEquals(19097, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getStartOffset());
        assertEquals(19365, parent.getChildPart().get(1)
                .getChildPart().get(0).getChildPart().get(1).getEndOffset());
    }

    @Test
    public void splitSyslogFile() {

        final File file = loadFile("/contentanalyzer/validSyslogFile.syslog");
        final AnalysisPart parent = new AnalysisPart(new AnalysisPart(null));
        parent.setDocType("SYSLOG");
        parent.setStartOffset(0);
        try {
            parent.setEndOffset(FileUtils.readFileToString(file).length());
            m.setMessageByteContent(FileUtils.readFileToByteArray(file));
            messageSplitter.split(m, parent);
        } catch(IOException e) {
            System.out.println(e.getMessage());
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals("Syslog Header", parent.getChildPart().get(0).getDocType());

        assertEquals(0, parent.getChildPart().get(0).getStartOffset());
        assertEquals(88, parent.getChildPart().get(0).getEndOffset());

        assertEquals("Syslog Message", parent.getChildPart().get(1).getDocType());

        assertEquals(91, parent.getChildPart().get(1).getStartOffset());
        assertEquals(2169, parent.getChildPart().get(1).getEndOffset());
    }
}
