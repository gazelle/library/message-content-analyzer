package net.ihe.gazelle.mca.contentanalyzer.application.analyzers;

import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.EncodedType;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.*;

public class MimeTypeDetectorTest {

    MessageContentAnalyzer m;
    MimeTypeDetector mimeTypeDetector;

    @Before
    public void setUp() {
        m = new ApplicationTestFactory().getMessageContentAnalyzer();
        mimeTypeDetector = new ApplicationTestFactory().getMimeTypeDetector();
    }

    @Test
    public void mimeTypeDetectorTest() {

        File file = loadFile("/contentanalyzer/validXmlFile.xml");
        final AnalysisPart parent = new AnalysisPart(new AnalysisPart(null));
        parent.setDocType("DOCUMENT");
        parent.setStartOffset(0);
        try {
            parent.setEndOffset(FileUtils.readFileToString(file).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        final AnalysisPart child = new AnalysisPart(null);
        try {
            assertFalse(mimeTypeDetector.detectMimeType(FileUtils.readFileToByteArray(file), child, parent));
        } catch (IOException e) {
            assertNotNull(null);
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }
    }

    @Test
    public void mimeTypeAnalyzeTestPDF() {

        final File file = loadFile("/contentanalyzer/zipFile.zip");
        final AnalysisPart parent = new AnalysisPart(new AnalysisPart(null));
        parent.setDocType("DOCUMENT");
        parent.setStartOffset(0);
        try {
            parent.setEndOffset(FileUtils.readFileToString(file).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        final AnalysisPart child = new AnalysisPart(parent);
        child.setDocType("PDF");
        child.setValidationType(null);

        try {
            m.setMessageByteContent(FileUtils.readFileToByteArray(file));
            mimeTypeDetector.mimeTypeAnalyze(m, child, parent);
        } catch (UnexpectedAnalysisException | IOException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertEquals(parent.getStartOffset(), child.getStartOffset());
        assertEquals(parent.getEndOffset(), child.getEndOffset());

        assertEquals(child, parent.getChildPart().get(0));
    }

    @Test
    public void mimeTypeAnalyzeTestZIP() {

        final File file = loadFile("/contentanalyzer/zipFile.zip");
        final AnalysisPart parent = new AnalysisPart(new AnalysisPart(null));
        parent.setDocType("DOCUMENT");
        parent.setEncodedType(EncodedType.NOT_ENCODED);
        parent.setStartOffset(0);
        try {
            parent.setEndOffset(FileUtils.readFileToString(file).length());
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        final AnalysisPart child = new AnalysisPart(parent);
        child.setDocType("ZIP");
        child.setValidationType(null);

        try {
            m.setMessageName("zipFile.zip".getBytes(StandardCharsets.UTF_8));
            m.setMessageByteContent(FileUtils.readFileToByteArray(file));
            mimeTypeDetector.mimeTypeAnalyze(m, child, parent);
        } catch (IOException e) {
            fail("Unable to read file");
        } catch (UnexpectedAnalysisException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }
        assertEquals(parent.getStartOffset(), child.getStartOffset());
        assertEquals(parent.getEndOffset(), child.getEndOffset());

        assertEquals("ZIP", child.getDocType());
        assertEquals("Folder : folder/", child.getChildPart().get(0).getDocType());

    }
}
