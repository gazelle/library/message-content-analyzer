package net.ihe.gazelle.mca.contentanalyzer.business.model.config.archives;

import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FileInterface;

public enum File implements FileInterface {

    INDEX_HTM("INDEX.HTM", false, null),
    README_TXT("README.TXT", false, null),
    METADATA_XML("METADATA.XML", false, "MANIFEST"),
    DOC("([A-Z]|[0-9]){1,8}\\.XML", true, "ATTACHMENT");

    private final String fileName;
    private final boolean regex;
    private final String xValInputType;

    File(final String fileName, final boolean regex, final String xValInputType) {
        this.fileName = fileName;
        this.regex = regex;
        this.xValInputType = xValInputType;
    }

    public String getFileName() {
        return fileName;
    }

    public boolean isRegex() {
        return regex;
    }

    public String getXValInputType() {
        return xValInputType;
    }
}
