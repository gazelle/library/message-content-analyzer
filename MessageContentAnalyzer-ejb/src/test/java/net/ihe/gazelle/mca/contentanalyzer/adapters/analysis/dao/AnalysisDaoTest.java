package net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.OIDGeneratorManagerImpl;
import net.ihe.gazelle.evsclient.application.interfaces.ProcessingCacheManager;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.interlay.dao.HandledObjectFile;
import net.ihe.gazelle.evsclient.interlay.dao.Repository;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.ApplicationTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.adapters.factory.DaoTestFactory;
import net.ihe.gazelle.mca.contentanalyzer.application.McaApi;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.MessageContentAnalyzer;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.DecodedPartNotSavedException;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.FileContentUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.DocType;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.metadata.ws.VersionController;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static net.ihe.gazelle.files.FilesUtils.loadFile;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ EntityManager.class, EntityManagerService.class })
@PowerMockIgnore({ "org.xml.*", "org.w3c.*", "javax.xml.*" })
public class AnalysisDaoTest {

    private final static int ANALYSIS_ID = 10;

    EntityManagerFactory entityManagerFactory;
    EntityManager entityManager;
    MetadataServiceProvider metadataServiceProvider;
    OIDGeneratorManagerImpl oidGeneratorManager;
    ProcessingCacheManager processingCacheManager;
    ApplicationPreferenceManager applicationPreferenceManager;

    AnalysisDaoImpl analysisDao;

    Analysis analysis;

    McaApi mcaApi;

    AnalysisManager analysisManager;

    MessageContentAnalyzer messageContentAnalyzer;

    @Before
    public void setUp() {
        mockEntityManager();
        mockApplicationPreferenceManager();
        mockVersionController();
        mockOidGeneratorManager();

        analysisManager = new AnalysisManager(analysisDao, applicationPreferenceManager, oidGeneratorManager,
                metadataServiceProvider, processingCacheManager);

        messageContentAnalyzer = new ApplicationTestFactory().getMessageContentAnalyzer();

        mcaApi = new McaApi(analysisManager, messageContentAnalyzer,
                null, null);

        analysisDao = new DaoTestFactory().getAnalysisDaoImpl(entityManagerFactory, applicationPreferenceManager);

        analysisManager = new AnalysisManager(analysisDao, applicationPreferenceManager, oidGeneratorManager,
                metadataServiceProvider, processingCacheManager);
    }

    private void mockApplicationPreferenceManager() {
        applicationPreferenceManager = Mockito.mock(ApplicationPreferenceManager.class);
        when(applicationPreferenceManager.getStringValue("root_oid"))
                .thenReturn("1.3.6.1.4.1.12559.11.1.2.1.4.");
        when(applicationPreferenceManager.getBooleanValue("mca_enable_decoded_storage"))
                .thenReturn(true);
        when(applicationPreferenceManager.getStringValue("object_for_validator_detector_repository"))
                .thenReturn("/opt/EVSClient_prod/validatedObjects/validatorDetector");
        when(applicationPreferenceManager.getStringValue("application_url"))
                .thenReturn("https://gazelle.ihe.net/EU-CAT/");
    }

    private void mockVersionController() {
        metadataServiceProvider = Mockito.mock(MetadataServiceProvider.class);
        when(metadataServiceProvider.getMetadata().getVersion()).thenReturn("EVSVERSION");
    }

    private void mockEntityManager() {
        entityManagerFactory = Mockito.mock(EntityManagerFactory.class);
        when(entityManagerFactory.createEntityManager()).thenReturn(entityManager);
        entityManager = PowerMockito.mock(EntityManager.class);
        PowerMockito.mockStatic(EntityManagerService.class);
        PowerMockito.when(EntityManagerService.provideEntityManager()).thenReturn(entityManager);
        when(entityManager.merge(Mockito.anyObject())).thenAnswer(new Answer<Analysis>() {
            @Override
            public Analysis answer(InvocationOnMock invocation) {
                Object[] args = invocation.getArguments();
                Analysis analysis = (Analysis) args[0];
                analysis.setId(ANALYSIS_ID);
                return analysis;
            }
        });
    }

    private void mockOidGeneratorManager() {
        oidGeneratorManager = Mockito.mock(OIDGeneratorManagerImpl.class);
        when(oidGeneratorManager.getNewOid())
                .thenReturn("1.3.6.1.4.1.12559.11.1.2.1.4.1");
    }

    @Ignore
    @Test
    public void oidAndFilePathSettingTest() {

        final File f = loadFile("/contentanalyzer/validXmlFile.xml");
        byte[] fileContent;
        try {
            fileContent = FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            fail("Unable to read file");
            return;
        }
        try {
            HandledObject handledObject = new HandledObject(fileContent, "validXmlFile.xml");

            analysis = analysisManager.create(handledObject, null, null);
            analysis = mcaApi.analyze(analysis);
            analysisManager.persistAnalysisData(analysis);
        } catch (UnexpectedProcessingException | NotLoadedException | ProcessingNotSavedException | DecodedPartNotSavedException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }

        assertNotNull(((HandledObjectFile)analysis.getObject()).getFilePath());

        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1", analysis.getOid());
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0", analysis.getRootAnalysisPart().getChildPart().get(0).getOid());
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getOid());
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getOid());
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.0", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getOid());
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.1", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getOid());
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.1.0", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getOid());
        assertEquals("/opt/EVSClient_prod/validatedObjects/validatorDetector"+ '/' + Repository.DatePath.build()
                + "/" + Analysis.AO_FILE_PREFIX + ANALYSIS_ID + Analysis.AO_FILE_SUFFIX, ((HandledObjectFile)analysis.getObject()).getFilePath());
    }

    @Ignore
    @Test
    public void editedFilePath() {
        final File f = loadFile("/contentanalyzer/zipFile.zip");
        messageContentAnalyzer.setMessageName("/contentanalyzer/zipFile.zip".getBytes(StandardCharsets.UTF_8));
        byte[] fileContent;
        try {
            fileContent = FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            fail("Unable to read file");
            return;
        }
        try {
            HandledObject handledObject = new HandledObject(fileContent, "zipFile.zip");
            //AnalysisUtils.addRootAnalysisPart(handledObject, analysis);

            analysis = analysisManager.create(handledObject, null, null);
            analysis = mcaApi.analyze(analysis);
            analysisManager.persistAnalysisData(analysis);
        } catch (UnexpectedProcessingException | NotLoadedException | ProcessingNotSavedException | DecodedPartNotSavedException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }


        assertNotNull(((HandledObjectFile)analysis.getObject()).getFilePath());

        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1", analysis.getOid());
        //DOCUMENT
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0", analysis.getRootAnalysisPart().getChildPart().get(0).getOid());

        //ZIP
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getOid());
        assertNull(analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getDecodedPartFilePath());

        //folder/
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getOid());
        assertNull(analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getDecodedPartFilePath());

        //folder/file.txt
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.0", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getOid());
        assertEquals("/opt/EVSClient_prod/validatedObjects/validatorDetector"+ '/' + Repository.DatePath.build()
                + "/" + FileContentUtils.DECODED_FILE_PREFIX + "0_0_0_0_"+ ANALYSIS_ID + Analysis.AO_FILE_SUFFIX,
                analysis.getRootAnalysisPart()
                        .getChildPart().get(0)
                        .getChildPart().get(0)
                        .getChildPart().get(0)
                        .getChildPart().get(0).getDecodedPartFilePath());

        AnalysisPart amp = analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getChildPart().get(0);
        //XML
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.0.0", amp.getOid());
        assertNotNull(amp.getDecodedPartFilePath());

        //SOAP envelope
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.0.0.0", amp.getChildPart().get(0)
                .getOid());
        assertNull(amp.getChildPart().get(0)
                .getDecodedPartFilePath());

        //SOAP body
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.0.0.0.0", amp.getChildPart().get(0)
                .getChildPart().get(0).getOid());
        assertNull(amp.getChildPart().get(0)
                .getChildPart().get(0).getDecodedPartFilePath());

        //XDS
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.0.0.0.1.0", analysis.getRootAnalysisPart()
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(1).getChildPart().get(0).getOid());
        assertNull(amp.getChildPart().get(0)
                .getChildPart().get(0).getDecodedPartFilePath());

        //folder/validMtomFileEncodedXML.xml
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.1", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getOid());
        assertEquals("/opt/EVSClient_prod/validatedObjects/validatorDetector"+ '/' + Repository.DatePath.build()
                + "/" + FileContentUtils.DECODED_FILE_PREFIX + "0_0_0_1_"+ ANALYSIS_ID + Analysis.AO_FILE_SUFFIX , analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(1).getDecodedPartFilePath());


        //folder/validBase64File.txt
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.2", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(2).getOid());
        assertEquals("/opt/EVSClient_prod/validatedObjects/validatorDetector"+ '/' + Repository.DatePath.build()
                + "/" + FileContentUtils.DECODED_FILE_PREFIX + "0_0_0_2_"+ ANALYSIS_ID + Analysis.AO_FILE_SUFFIX , analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(2).getDecodedPartFilePath());

        //BASE64
        Assert.assertEquals(DocType.BASE64.getValue(), analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(2).getChildPart().get(0).getDocType());
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.2.0", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(2).getChildPart().get(0).getOid());
        assertEquals("/opt/EVSClient_prod/validatedObjects/validatorDetector"+ '/' + Repository.DatePath.build()
                + "/" + FileContentUtils.DECODED_FILE_PREFIX + "0_0_0_2_"+ ANALYSIS_ID + Analysis.AO_FILE_SUFFIX , analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(2).getChildPart().get(0).getDecodedPartFilePath());

        //XML
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.2.0.0", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(2).getChildPart().get(0).getChildPart().get(0)
                .getOid());
        assertNotNull(analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(2).getChildPart().get(0).getChildPart().get(0)
                .getDecodedPartFilePath());

        //CDA
        assertEquals("1.3.6.1.4.1.12559.11.1.2.1.4.1.0.0.0.2.0.0.0", analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(2).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getOid());
        assertNull(analysis.getRootAnalysisPart().getChildPart().get(0)
                .getChildPart().get(0).getChildPart().get(0).getChildPart().get(2).getChildPart().get(0).getChildPart().get(0)
                .getChildPart().get(0).getDecodedPartFilePath());
    }
}
