/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.gui;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.evsclient.application.AbstractGuiPermanentLink;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.NotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.connector.api.EVSClientResults;
import net.ihe.gazelle.evsclient.connector.api.EVSClientServletConnector;
import net.ihe.gazelle.evsclient.connector.model.EVSClientCrossValidatedObject;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.domain.validation.ValidationRef;
import net.ihe.gazelle.evsclient.domain.validation.ValidationStatus;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.interlay.dao.HandledObjectFile;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParam;
import net.ihe.gazelle.evsclient.interlay.gui.document.ContentConverter;
import net.ihe.gazelle.evsclient.interlay.gui.document.HandledDocument;
import net.ihe.gazelle.evsclient.interlay.gui.processing.AbstractProcessingBeanGui;
import net.ihe.gazelle.evsclient.interlay.servlet.Upload;
import net.ihe.gazelle.evsclient.interlay.util.SyntaxDetector;
import net.ihe.gazelle.evsclient.interlay.util.SyntaxDetectorTika;
import net.ihe.gazelle.mca.contentanalyzer.adapters.common.gui.McaVueFileNaming;
import net.ihe.gazelle.mca.contentanalyzer.adapters.config.QueryParamMca;
import net.ihe.gazelle.mca.contentanalyzer.application.McaApi;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisPartManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.AnalyzerManager;
import net.ihe.gazelle.mca.contentanalyzer.application.config.McaConfigManager;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.Base64Converter;
import net.ihe.gazelle.mca.contentanalyzer.application.files.FilesDownloaderManager;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.DecodedPartNotSavedException;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.AnalysisUtils;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.DocTypeUtils;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.FileContentUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.*;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.interlay.filter.UserValueFormatter;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.contexts.Lifecycle;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.component.UITree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;


public class AnalysisBeanGui extends AbstractProcessingBeanGui<Analysis, AnalysisManager>
      implements Serializable {

   private static final long serialVersionUID = -7624127335540395705L;

   private static final Logger LOGGER = LoggerFactory.getLogger(AnalysisBeanGui.class);

   private static final String NOT_PERFORMED = "not performed";
   public static final String EDITED_MESSAGE_PART_FROM_REMOTE_VALIDATION = "EditedMessagePartFromRemoteValidation";
   public static final String FAILED = "FAILED";
   public static final String PASSED = "PASSED";

   private final AnalysisPartManager analysisPartManager;

   private final McaConfigManager mcaConfigManager;

   private final McaApi mcaApi;

   private final AnalyzerManager analyzerManager;

   private final Base64Converter base64Converter;

   private final FilesDownloaderManager filesDownloaderManager;

   private boolean editModeEnabled;

   private FilterDataModel<Analysis> dataModel;

   private boolean namespacesActuallyAdded;

   private byte[] messagePart;

   private boolean adviseNodeOpened;

   private boolean selectedMessagePartEdited;

   private AnalysisPart analysisPartToDisplay;

   private boolean showAddedNameSpacesInGui;

   private boolean editMessageInGui;

   private transient WeakHashMap<Analysis,GazelleTreeNodeImpl<AnalysisPart>> nodes;

   private final SyntaxDetector syntaxDetector;

   public AnalysisBeanGui(AnalysisManager analysisManager, AnalysisPartManager analysisPartManager,
                          McaConfigManager mcaConfigManager,
                          McaApi mcaApi, AnalyzerManager analyzerManager, CallerMetadataFactory callerMetadataFactory,
                          Base64Converter base64Converter, FilesDownloaderManager filesDownloaderManager,
                          ApplicationPreferenceManager applicationPreferenceManager,
                          GazelleIdentity gazelleIdentity) {
      super(AnalysisBeanGui.class, analysisManager, callerMetadataFactory, gazelleIdentity, applicationPreferenceManager,
            new AbstractGuiPermanentLink(applicationPreferenceManager) {
               @Override
               public String getResultPageUrl() {
                  return McaVueFileNaming.DETAILED_RESULT.getMenuLink();
               }
            });
      this.analysisPartManager = analysisPartManager;
      this.mcaConfigManager = mcaConfigManager;
      this.mcaApi = mcaApi;
      this.analyzerManager = analyzerManager;
      this.base64Converter = base64Converter;
      this.filesDownloaderManager = filesDownloaderManager;
      this.syntaxDetector = new SyntaxDetectorTika();
      try {
         editModeEnabled = applicationPreferenceManager.getBooleanValue("mca_part_edit_mode_enabled");
      } catch (Exception e) {
         editModeEnabled = false;
      }
      try {
         showAddedNameSpacesInGui = applicationPreferenceManager.getBooleanValue("mca_force_show_namespaces_status");
      } catch (Exception e) {
         showAddedNameSpacesInGui = false;
      }
      try {
         init();
      } catch (UnauthorizedException | ForbiddenAccessException e) {
         GuiMessage.logMessage(StatusMessage.Severity.ERROR, e.getMessage());
      }catch (NotFoundException e){
         LOGGER.error("Error while initializing AnalysisBeanGui: {}", e.getMessage());
      }

   }

   private void init() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
      this.nodes = new WeakHashMap<>();
      super.initFromUrl();
      if (selectedObject != null) {
         final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext()
               .getRequestParameterMap();
         AnalysisPart part = getSelectedAnalysisPart(selectedObject, urlParams);
         recordValidationRef(part, urlParams);
         if (part==null) {
            LOGGER.warn(
                    "net.ihe.gazelle.mca.no-part-use-root-part-instead"
            );
            part = selectedObject.getRootAnalysisPart();
         }
         defineMessagePart(part, selectedObject);
      }
   }

   private void defineMessagePart(AnalysisPart part, Analysis analysis) {
      setAnalysisPartToDisplay(part);
      setMessagePart(
              getAnalysisPartFilePath(part, analysis),
              getAnalysisPart(part));
   }

   private AnalysisPart getAnalysisPart(AnalysisPart part) {
      return part == selectedObject.getRootAnalysisPart()
              ? part.getChildPart().get(0)
              : part;
   }

   private String getAnalysisPartFilePath(AnalysisPart part, Analysis analysis) {
      return part.getDecodedPartFilePath()==null
              ?processingManager.getAnalyzedObjectFilePath(analysis)
              :part.getDecodedPartFilePath();
   }

   private void recordValidationRef(AnalysisPart part, Map<String, String> urlParams) {
      if (!MapUtils.isEmpty(urlParams) && urlParams.containsKey(QueryParam.VALIDATION_REF)) {
         analysisPartManager.recordValidation(part, urlParams.get(QueryParam.VALIDATION_REF));
      }
   }

   private AnalysisPart getSelectedAnalysisPart(Analysis selectedAnalysis, Map<String, String> urlParams) {
      if (!MapUtils.isEmpty(urlParams) && urlParams.containsKey(QueryParam.PROCESSING_OID)) {
         return findAnalysisPart(selectedAnalysis.getRootAnalysisPart(),
               urlParams.get(QueryParam.PROCESSING_OID));
      } else {
         return null;
      }
   }

   public Analysis getAnalysis() {
      return selectedObject;
   }

   private AnalysisPart findAnalysisPart(AnalysisPart part, String oid) {
      if (oid.equals(part.getOid())) {
         return part;
      } else {
         for (final AnalysisPart analysisPart : part.getChildPart()) {
            AnalysisPart p = findAnalysisPart(analysisPart, oid);
            if (p != null) {
               return p;
            }
         }
      }
      return null;
   }

   public GazelleTreeNodeImpl<AnalysisPart> buildTree() {
      if (selectedObject==null) {
         return new GazelleTreeNodeImpl<>();
      }
      if (nodes.containsKey(selectedObject)) {
         return nodes.get(selectedObject);
      }
      final GazelleTreeNodeImpl<AnalysisPart> tree = new GazelleTreeNodeImpl<>();
      nodes.put(selectedObject,tree);
      tree.setData(selectedObject.getRootAnalysisPart());
      int index = 0;
      for (final AnalysisPart analysisPart : selectedObject.getRootAnalysisPart().getChildPart()) {
         final List<AnalysisPart> tmpList = analysisPart.getChildPart();
         if (tmpList != null && !tmpList.isEmpty()) {
            tree.addChild(index, this.processFileList(analysisPart));
         } else {
            final GazelleTreeNodeImpl<AnalysisPart> node = new GazelleTreeNodeImpl<>();
            node.setData(analysisPart);
            tree.addChild(index, node);
         }
         index++;
      }

      return tree;
   }

   private GazelleTreeNodeImpl<AnalysisPart> processFileList(final AnalysisPart file) {
      final GazelleTreeNodeImpl<AnalysisPart> tree = new GazelleTreeNodeImpl<>();
      tree.setData(file);
      int index = 0;
      for (final AnalysisPart childFile : file.getChildPart()) {
         final List<AnalysisPart> tmpList = childFile.getChildPart();
         if (tmpList != null && !tmpList.isEmpty()) {
            tree.addChild(index, this.processFileList(childFile));
         } else {
            final GazelleTreeNodeImpl<AnalysisPart> node = new GazelleTreeNodeImpl<>();
            node.setData(childFile);
            tree.addChild(index, node);
         }
         index++;
      }
      return tree;
   }

   public byte[] getMessagePart() {
      return this.messagePart;
   }

   public boolean getNamespacesActuallyAdded() {
      return namespacesActuallyAdded;
   }

   public void setMessagePart(String filePath, AnalysisPart analysisPart) {
      try {
         setAnalysisPartToDisplay(analysisPart);
         this.messagePart = mcaApi.getOriginalPartContent(filePath, analysisPart);
         setEnhancedDocumentContent(this.messagePart);
         setSelectedMessagePartEdited(false);
         setEditMessageInGui(false);
      } catch (UnexpectedAnalysisException e) {
         this.messagePart = new byte[]{};
         logUnexpectedException(e);
      }
   }

   public String getEditedMessagePart() {
      return document.toString();
   }

   public void setEditedMessagePart(String displayedMessagePart) {
      setDocumentText(displayedMessagePart);
   }

   public boolean isDisplayablePart(AnalysisPart part) {
      return part!=null && !isFolder(part);
   }

   private void setEnhancedDocumentContent(byte[] content) {
      setDocumentContent(content);
      if (isZipPart(analysisPartToDisplay)) {
         getDocument().setSyntax(SyntaxDetector.Syntax.ZIP);
      } else if (isFolder(analysisPartToDisplay)) {
         GuiMessage.logMessage(StatusMessage.Severity.INFO, "net.ihe.gazelle.mca.ThisIsADirectory");
         return;
      } else if (!EncodedType.B64_ENCODED.equals(analysisPartToDisplay.getEncodedType()) && isDisplayedPartXml() && !SyntaxDetector.Syntax.XML.equals(getDocument().getSyntax())) {
         getDocument().setSyntax(SyntaxDetector.Syntax.XML);
      }
      if (EncodedType.B64_ENCODED.equals(analysisPartToDisplay.getEncodedType())
              && SyntaxDetector.Syntax.TXT.equals(document.getSyntax())
              && base64Converter.base64Detection(content)) {
         getDocument().setSyntax(SyntaxDetector.Syntax.B64);
         getDocument().setMimeType(getMimeType(analysisPartToDisplay.getValidationType(),getDocument().getMimeType()));
      }
      if (EncodedType.B64_ENCODED.equals(analysisPartToDisplay.getEncodedType())
              && SyntaxDetector.Syntax.BIN.equals(document.getSyntax())) {
         getDocument().setMimeType(getMimeType(analysisPartToDisplay.getValidationType(), getDocument().getMimeType()));
         getDocument().setSyntax(
                 syntaxDetector.detectSyntax(getDocument().getContent())
         );
      }
      this.namespacesActuallyAdded = false;
      if (SyntaxDetector.Syntax.XML.equals(getDocument().getSyntax())) {
         Map<String, String> namespaces = getNameSpaces();
         if (!isNameSpacesEmpty(namespaces)) {
            String originalContent = new String(content, StandardCharsets.UTF_8);
            String enhancedContent = FileContentUtils.xmlAddNamespacesToFileContent(originalContent, namespaces);
            this.namespacesActuallyAdded = !enhancedContent.equals(originalContent);
            if (namespacesActuallyAdded) {
               setDocumentText(enhancedContent);
               getDocument().setSyntax(SyntaxDetector.Syntax.XML);
            }
         }
      }
   }

   private String getMimeType(ValidationType type, String mimeType) {
      if (type==null) {
         return "text/plain";
      }
      switch (type) { // TODO
         case PDF:
            return "application/pdf";
         case TLS:
            return "application/x-pem-file";
         case DICOM:
            return "application/dicom";
         default:
            return mimeType;
      }

   }

   private boolean isZipPart(AnalysisPart node) {
      return node!=null && StringUtils.isNotEmpty(node.getDocType())
              && (
                      (DocTypeUtils.isZip(node.getDocType())
                        || (DocTypeUtils.isDocument(node.getDocType())
                              && !node.getChildPart().isEmpty()
                              && DocTypeUtils.isZip(node.getChildPart().get(0).getDocType())))
                   || (mcaConfigManager.getZipStructureWithDocType(node.getDocType()) != null
                        || (DocTypeUtils.isDocument(node.getDocType())
                              && !node.getChildPart().isEmpty()
                            && mcaConfigManager.getZipStructureWithDocType(node.getChildPart().get(0).getDocType()) != null))
               );
   }

   public String performAnotherValidation() {
      return getValidatorUrl();
   }

   @Override
   public Analysis loadProcessingObjectByOid(final String objectOid, final String privacyKey)
         throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
      Analysis analysis = null;
      try {
         analysis = super.loadProcessingObjectByOid(objectOid, privacyKey);
      } catch (NotFoundException nfe) {
         AnalysisPart analysisPart = analysisPartManager.getByOid(objectOid);
         if (analysisPart != null) {
            //final Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
            //      .getRequestParameterMap();
            //String xValResultOidFromUrl = EVSClientResults.getXValResultOidFromUrl(params);
            //if (xValResultOidFromUrl != null && !xValResultOidFromUrl.isEmpty()) {
            //   analysisPart.getxValMatch().setxValidatorOid(xValResultOidFromUrl);
            //   analysisPartManager.save(analysisPart);
            //}
            analysis = processingManager.getObjectByOID(
                  analysisPart.getRootParent().getOid(), privacyKey, identity);
         } else {
            throw nfe;
         }
      }
      return analysis;
   }

   @Override
   public Boolean needsUpload() {
      return (super.needsUpload() && selectedObject == null);
   }

   @Override
   public void reset() {
      super.reset();
      setSelectedObject(null);
      messagePart = new byte[]{};
   }

   public void executeOnEditedPart() {
      try {
         String fragment = getEditedMessagePart();
         setUploadedFileName(AnalyzerManager.EXECUTE_FROM_STR_DESCRIPTION
                 +"_"+ (0x00000000FFFFFFFFl & (long)fragment.hashCode())
                 +"_"+System.currentTimeMillis()
                 +(isDisplayedPartXml()?".xml":"")
         );
         setSelectedObject(
                 analyzerManager.executeWithString(
                         fragment,
                         getUploadedFileName(),
                         super.getEvsCallerMetadata(),
                         super.getOwnerMetadata()));
         getSelectedObject()
                 .getRootAnalysisPart()
                 .getChildPart().iterator().next()
                 .setDocType(DocType.XML.getValue());
      } catch (UnexpectedProcessingException | DecodedPartNotSavedException | ProcessingNotSavedException | NotLoadedException e) {
         logUnexpectedException(e);
         return;
      }
      redirectWithCallingToolsInfoIfNeeded(getPermanentLink(selectedObject));
   }

   public void execute(HandledDocument document, String filename,ValidationRef validation) {
      try {
         EVSCallerMetadata caller = super.getEvsCallerMetadata();
         OwnerMetadata owner = super.getOwnerMetadata();
         super.setDocument(document);
         super.setUploadedFileName(filename);
         if (super.getUploadedFileContent() != null) {
            setSelectedObject(
                    analyzerManager.execute(
                       super.getUploadedFileContent(),
                       super.getUploadedFileName(),
                       caller,
                       owner,
                       validation)
         )
         ;
            defineMessagePart(selectedObject.getRootAnalysisPart(),selectedObject);
         } else {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "net.ihe.gazelle.mca.PleaseSelectAFile");
         }
      } catch (Exception e) {
         logUnexpectedException(e);
      }
   }

   public void executeWithString(byte[] messageContent, String fileName) {
      try {
         EVSCallerMetadata caller = super.getEvsCallerMetadata();
         OwnerMetadata owner = super.getOwnerMetadata();
         if (messageContent != null) {
            setSelectedObject(analyzerManager.execute(messageContent, fileName, caller, owner));
            redirectWithCallingToolsInfoIfNeeded(getPermanentLink(selectedObject));
         } else if (selectedObject != null) {
            setSelectedObject(analyzerManager.execute(selectedObject.getObjects().iterator().next().getContent(),
                    selectedObject.getObjects().iterator().next().getOriginalFileName(),
                    caller, owner));
            redirectWithCallingToolsInfoIfNeeded(getPermanentLink(selectedObject));
         } else {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "net.ihe.gazelle.mca.PleaseSelectAFile");
         }
      } catch (Exception e) {
         logUnexpectedException(e);
      }
   }
   public void execute() {
      executeWithString(
              selectedObject == null ? super.getUploadedFileContent() : selectedObject.getObject().getContent(),
              selectedObject == null ? super.getUploadedFileName() : selectedObject.getObject().getOriginalFileName()
      );
   }

   public void executeFromRemote(String fileContent, String externalId, String proxyType, String toolOid) {

      super.externalId = externalId;
      super.proxyType = proxyType;
      super.toolOid = toolOid;

      executeWithString(
              fileContent.getBytes(StandardCharsets.UTF_8),
              EDITED_MESSAGE_PART_FROM_REMOTE_VALIDATION +"_"+externalId+"_"+toolOid+"_"+proxyType+"_"+System.currentTimeMillis());
   }


   private void redirectWithCallingToolsInfoIfNeeded(String url) {
      if (url != null && !url.isEmpty()) {
         addCallingToolsInfo(url, toolOid, externalId);
         redirectOnAnalysisResult(url);
      }
   }

   private void redirectOnAnalysisResult(String url) {
      final ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
      try {
         ec.redirect(url);
      } catch (IOException e) {
         GuiMessage.logMessage(StatusMessage.Severity.ERROR, "net.ihe.gazelle.mca.UnableToRedirectToTheAnalysisResult");
      }
   }


   public String revalidate() {
      return this.getValidatorUrl() + "?" + QueryParam.PROCESSING_OID + "=" + this.selectedObject.getOid();
   }

   public void downloadFile(final AnalysisPart node, final boolean downloadAddedNamespaces) {
      downloadFile(processingManager.getAnalyzedObjectFilePath(getAnalysis()), node, downloadAddedNamespaces);
   }

   public void downloadFile(final String objectPath, final AnalysisPart node, final boolean downloadAddedNamespaces) {

      byte[] bytes;

      if (!isDownloadable(node)) {
         logFolderDownloadError();
         return;
      } else {
         try {
            bytes = mcaApi.getOriginalPartContent(objectPath, node);
         } catch (UnexpectedAnalysisException e) {
            logUnexpectedException(e);
            return;
         }
      }
      if (ValidationType.DICOM.equals(node.getValidationType())) {
         try {
            // Test base 64
            final boolean result = base64Converter.base64Detection(bytes);
            if (result) {
               bytes = base64Converter.base64Decoding(bytes);

               if (bytes != null) {
                  filesDownloaderManager.downloadDICOMFile(bytes, node, selectedObject,
                        FacesContext.getCurrentInstance());
               }
            } else {
               filesDownloaderManager.downloadDICOMFile(bytes,
                     node, selectedObject,
                     FacesContext.getCurrentInstance());
            }
         } catch (UnexpectedAnalysisException e) {
            logUnexpectedException(e);
         }
      } else {
         Map<String, String> namespaces = node.getNamespaces();
         if (namespaces != null && downloadAddedNamespaces) {
            String fileContent = FileContentUtils.xmlAddNamespacesToFileContent(
                  new String(bytes, StandardCharsets.UTF_8), namespaces);
            filesDownloaderManager.downloadFile(fileContent, node, selectedObject,
                  FacesContext.getCurrentInstance());
         } else if (analyzerManager.isZipNode(node) ||
               (DocTypeUtils.isDocument(node.getDocType()) && !node.getChildPart().isEmpty() && analyzerManager.isZipNode(
                     node.getChildPart().get(0)))) {
            filesDownloaderManager.downloadZipFile(objectPath, node, selectedObject, FacesContext.getCurrentInstance());
         } else if (ValidationType.PDF.equals(node.getValidationType())) {
            filesDownloaderManager.downloadPdfFile(bytes, node, selectedObject, FacesContext.getCurrentInstance());
         } else {
            filesDownloaderManager.downloadFile(new String(bytes, StandardCharsets.UTF_8), node, selectedObject,
                  FacesContext.getCurrentInstance());
         }
      }
   }

   public boolean isDownloadable(AnalysisPart node) {
      return node!=null && StringUtils.isNotEmpty(node.getDocType()) && !isFolder(node);
   }

   public boolean isFolder(AnalysisPart node) {
      return node.getDocType().endsWith("/") || DocTypeUtils.isFolder(node.getDocType());
   }

   public String getIconForNode(final AnalysisPart node) {
      if (node == null || StringUtils.isEmpty(node.getDocType()) || StringUtils.isEmpty(node.getOid())) {
         return "gzl-icon-globe";
      } else if (NOT_PERFORMED.equals(this.getValidationPermanentLinkFromDb(node.getOid()))) {
         return "gzl-icon-circle-orange";
      } else {
         return getStatusStyle(getValidationStatus(node));
      }
   }

   private boolean isFailed(String status) {
      return FAILED.equals(status)||ValidationStatus.DONE_FAILED.name().equals(status);
   }

   private boolean isPassed(String status) {
      return PASSED.equals(status)||ValidationStatus.DONE_PASSED.name().equals(status);
   }

   private String getStatusStyle(String status) {
      if (status == null) {
         return null;
      } else if (isFailed(status)) {
         return "gzl-icon-circle-red";
      } else if (isPassed(status)) {
         return "gzl-icon-circle-green";
      } else {
         return "gzl-icon-circle-blue";
      }
   }

   @Nullable
   private String getValidationStatus(AnalysisPart node) {
      String status = null;
      ValidationRef validationRef = node.getValidation();
      if (validationRef != null) {
         status = validationRef.getValidationStatus();
         if (StringUtils.isEmpty(status)) {
            // status in validationRef is transient : populate with current status
            status = EVSClientResults.getValidationStatus(validationRef.getOid(), getApplicationUrl());
            validationRef.setValidationStatus(status);
         }
      }
      return status;
   }

   private String getApplicationUrl() {
      return applicationPreferenceManager.getStringValue("application_url");
   }

   public String getValidationPermanentLinkFromDb(final String oid) {
      if (oid.length() == 0) {
         return NOT_PERFORMED;
      }
      final boolean createContexts = !Contexts.isEventContextActive() && !Contexts.isApplicationContextActive();
      if (createContexts) {
         Lifecycle.beginCall();
      }
      AnalysisPart analysisPart = analysisPartManager.getByOid(oid);
      Analysis analysis = analysisPart.getRootParent().getAnalysis();
      if (analysis == null) {
         return NOT_PERFORMED;
      }
      ValidationRef validationRef = analysisPart.getValidation();
      if (validationRef == null || StringUtils.isEmpty(validationRef.getOid())) {
         return NOT_PERFORMED;
      }
      return EVSClientResults.getValidationPermanentLink(validationRef.getOid(), getApplicationUrl());
   }

   public String getCrossValidationPermanentLink(String oid) {
      return EVSClientResults.getLastXValPermanentLinkWithExternalId(oid, getMcaToolOid(),
            getApplicationUrl());
   }

   private String getMcaToolOid() {
      return applicationPreferenceManager.getStringValue("mca_tool_oid");
   }

   public String getIconForXNode(String oid) {
      if (oid != null && !oid.isEmpty()) {
         return getStatusStyle(
                 EVSClientResults.getLastXValStatusWithExternalId(
                         oid,
                         getMcaToolOid(),
                         getApplicationUrl()));
      }
      return null;
   }



   public boolean crossValidatorAvailable(String xValidatorOid) {
      if (xValidatorOid != null && !xValidatorOid.isEmpty()) {
         String validatorStatus = EVSClientResults.getXValidatorStatus(xValidatorOid, getApplicationUrl());
         return "Available".equals(validatorStatus);
      }
      return false;
   }

   public String validate(final AnalysisPart node) {
      return validate(processingManager.getAnalyzedObjectFilePath(getAnalysis()), node);
   }

   String validate(String objectPath, AnalysisPart node) {
      try {
         if (!isValidable(node)) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "net.ihe.gazelle.mca.CannotValidateAZipFile");
            return McaVueFileNaming.DETAILED_RESULT.getMenuLink() + "?" + QueryParam.PROCESSING_OID + "=" + findAnalysisOid(node);
         }
         if (DocTypeUtils.isFolder(node.getDocType())) { // should never happens
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "net.ihe.gazelle.mca.CannotValidateAFolder");
            return McaVueFileNaming.DETAILED_RESULT.getMenuLink() + "?" + QueryParam.PROCESSING_OID + "=" + findAnalysisOid(
                    node);
         }
         if (DocTypeUtils.isEmbeddedB64(node.getDocType())) {
            node = node.getChildPart().get(0);
         }
         if (node.getDecodedPartFilePath() != null) {
            objectPath = node.getDecodedPartFilePath();
         } 
         if (EncodedType.NOT_ENCODED.equals(node.getEncodedType()) && (node.getNamespaces() == null || node.getNamespaces().isEmpty())) {
            // no namespaces to add
            return validate(
                    objectPath,
                    node.getValidationType(),
                    node.getStartOffset(),
                    node.getEndOffset(),
                    node.getOid()
            );
         }
         if (node.getDecodedPartFilePath() == null) {
            if (!EncodedType.NOT_ENCODED.equals(node.getEncodedType())) {
               // BASE64 encoded or ZIP Encoded
               objectPath = createDecodedPart(
                       mcaApi.getOriginalPartContent(objectPath, node),
                       objectPath,
                       node
               );
            } else {
               // add namespaces
               String stringFileContent = FileContentUtils.xmlAddNamespacesToFileContent(
                       new String(mcaApi.getOriginalPartContent(objectPath, node), StandardCharsets.UTF_8),
                       node.getNamespaces()
               );
               objectPath = createAddedNamespacePart(
                       stringFileContent.getBytes(StandardCharsets.UTF_8),
                       objectPath,
                       node
               );
            }
         }
         return validate(
                 objectPath,
                 node.getValidationType(),
                 0,
                 0,
                 node.getOid()
         );
      } catch (UnexpectedAnalysisException | DecodedPartNotSavedException e) {
         logUnexpectedException(e);
         return McaVueFileNaming.DETAILED_RESULT.getMenuLink() + "?" + QueryParam.PROCESSING_OID + "=" + findAnalysisOid(
               node);
      }
   }

   public boolean isValidable(AnalysisPart node) {
      return node!=null && StringUtils.isNotEmpty(node.getDocType()) && !(
              DocTypeUtils.isZip(node.getDocType())
               || (DocTypeUtils.isDocument(node.getDocType())
                    && !node.getChildPart().isEmpty()
                    && DocTypeUtils.isZip(node.getChildPart().get(0).getDocType()))
               || mcaConfigManager.getZipStructureWithDocType(node.getDocType()) != null
               || (DocTypeUtils.isDocument(node.getDocType())
                     && !node.getChildPart().isEmpty()
                     && mcaConfigManager.getZipStructureWithDocType(node.getChildPart().get(0).getDocType()) != null)
              || (node.getDecodedPartFilePath() == null && EncodedType.ZIP_ENCODED.equals(node.getEncodedType()) && DocTypeUtils.isFolder(node.getDocType()))
              );
   }

   private String readFileContent(String path) throws UnexpectedAnalysisException {
      try {
         return new ContentConverter().toString(FileUtils.readFileToByteArray(new File(path)),
                 StandardCharsets.UTF_8);
      } catch (IOException e) {
         LOGGER.error("Unable to read file : ", e);
         throw new UnexpectedAnalysisException("Unable to read File.", e);
      }
   }

   String validate(final String objectPath, final ValidationType validationType, final int startOffset,
                   final int endOffset, final String oid) {

      String key = Upload.putFile(objectPath);

      String url = "/validate.seam"
            + "?" + QueryParamMca.START_OFFSET + "=" + startOffset
            + "&" + QueryParamMca.END_OFFSET + "=" + endOffset
            + "&" + QueryParam.KEY + "=" + key;

      if (validationType != null) {
         url += "&" + QueryParamMca.VALIDATION_TYPE + "=" + validationType.getValue();
      }
      if(this.selectedObject != null && this.selectedObject.getSharing() != null && Boolean.TRUE.equals(this.selectedObject.getSharing().getIsPrivate())
              && this.selectedObject.getSharing().getPrivacyKey() != null){
         url += "&" + QueryParam.CALLER_PRIVACY_KEY + "=" + this.selectedObject.getSharing().getPrivacyKey();
      }
      return addCallingToolsInfo(url, getMcaToolOid(), oid);
   }

   private String createDecodedPart(byte[] decodedPartBytesContent, String objectPath, AnalysisPart node)
         throws DecodedPartNotSavedException {

      String decodedPartPath = FileContentUtils.computeEditedPartPath(objectPath, node.getOid(),
            findAnalysis(node).getOid());

      processingManager.createDecodedPartFile(decodedPartBytesContent, decodedPartPath);

      node.setDecodedPartFilePath(decodedPartPath);

      return decodedPartPath;
   }

   private String createAddedNamespacePart(byte[] decodedPartBytesContent, String objectPath, AnalysisPart node)
         throws DecodedPartNotSavedException {

      String decodedPartPathWithAddedNamespaces = FileContentUtils.computeEditedPartPath(objectPath, node.getOid(),
            findAnalysis(node).getOid());

      processingManager.createDecodedPartFile(decodedPartBytesContent, decodedPartPathWithAddedNamespaces);

      node.setDecodedPartFilePath(decodedPartPathWithAddedNamespaces);

      return decodedPartPathWithAddedNamespaces;
   }

   private Analysis findAnalysisOrNull(AnalysisPart node) {
      Analysis analysis = node.getAnalysis();
      if (analysis == null && node.getRootParent() != null) {
         analysis = node.getRootParent().getAnalysis();
      }
      return analysis;
   }

   private Analysis findAnalysis(AnalysisPart node) throws DecodedPartNotSavedException {
      Analysis analysis = findAnalysisOrNull(node);
      if (analysis == null) {
         throw new DecodedPartNotSavedException(MessageFormat.format("parent-analysis-not-found {0}", node.getOid()));
      }
      return analysis;
   }

   private String findAnalysisOid(AnalysisPart node) {
      Analysis analysis = findAnalysisOrNull(node);
      return analysis == null ? "" : analysis.getOid();
   }

   public void sendToXValidation(final AnalysisPart analysisPart) {

      List<AnalysisPart> xValInputsList = analysisPart.getXValInputList();
      List<EVSClientCrossValidatedObject> crossValidationInputList = new ArrayList<>();
      for (AnalysisPart l_o_input : xValInputsList) {
         try {
            Analysis analysis = findAnalysis(analysisPart);
            HandledObjectFile file =  ((HandledObjectFile) analysis.getObject());
            crossValidationInputList.add(
                    new CrossValidationInput(
                            l_o_input.getxValInputType(),
                            mcaApi.getOriginalPartContent(
                                  file.getFilePath(),
                                  l_o_input),
                           "xml"));
         } catch (Exception e) {
            logUnexpectedException(e);
         }
      }
      try {
         EVSClientServletConnector.sendToXValidation(crossValidationInputList,
               FacesContext.getCurrentInstance().getExternalContext(),
               getApplicationUrl(), getMcaToolOid(), analysisPart.getxValidatorOid(),
               analysisPart.getOid());
      } catch (IOException e) {
         GuiMessage.logMessage(StatusMessage.Severity.ERROR, "net.ihe.gazelle.mca.CannotRedirectToCrossValidation");
      }
   }

   private String addCallingToolsInfo(String url, String toolOid, String externalOid) {
      if (StringUtils.isNotEmpty(toolOid)) {
         url += "&" + QueryParam.TOOL_OID + "=" + toolOid;
      }
      if (StringUtils.isNotEmpty(externalOid)) {
         url += "&" + QueryParam.EXTERNAL_ID + "=" + externalOid;
      }
      return url;
   }

   public FilterDataModel<Analysis> getDataModel() {
      if (this.dataModel == null) {
         boolean isLoggedIn = super.identity.isLoggedIn();
         boolean isMonitor = super.identity.hasRole("monitor_role") || super.identity.hasRole("admin_role");
         String login = super.identity.getUsername();
         String organization = super.identity.getOrganisationKeyword();
         this.dataModel = new FilterDataModel<Analysis>(
               new Filter<>(processingManager.getCriterionForFilter(login, organization, isMonitor, isLoggedIn))) {
            private static final long serialVersionUID = -6565448932848176287L;

            @Override
            protected Object getId(final Analysis t) {
               return t.getId();
            }
         };
         Filter<Analysis> filter = dataModel.getFilter();
         filter.getFormatters().put("username", new UserValueFormatter(filter, "username"));
         dataModel.setFilter(filter);
      }
      return this.dataModel;
   }

   public void resetFilter() {
      final Filter<?> filter = this.dataModel.getFilter();
      if (filter != null) {
         filter.clear();
      }
   }

   public Map<String, String> getNameSpaces() {
      if (DocTypeUtils.isDocument(analysisPartToDisplay.getDocType()) && !analysisPartToDisplay.getChildPart().isEmpty()
            && analysisPartToDisplay.getChildPart().get(0).getNamespaces() != null) {
         return analysisPartToDisplay.getChildPart().get(0).getNamespaces();
      } else {
         return analysisPartToDisplay.getNamespaces();
      }
   }

   public void setShowAddedNameSpacesInGui(boolean showAddedNameSpacesInGui) {
      this.showAddedNameSpacesInGui = showAddedNameSpacesInGui;
   }

   public boolean getShowAddedNameSpacesInGui() {
      return showAddedNameSpacesInGui;
   }

   private boolean isNameSpacesEmpty(Map<String, String> namespaces) {
      return namespaces.isEmpty();
   }

   public boolean isDisplayedPartXml() {
      return analysisPartManager.isXmlChild(analysisPartToDisplay);
   }

   public String getMessagePartLog() {
      return analysisPartToDisplay.getPartLog();
   }

   public AnalysisPart getAnalysisPartToDisplay() {
      return analysisPartToDisplay;
   }

   public void setAnalysisPartToDisplay(AnalysisPart analysisPartToDisplay) {
      this.analysisPartToDisplay = analysisPartToDisplay;
   }

   public boolean getAdviseNodeOpened() {
      return adviseNodeOpened;
   }

   public void setAdviseNodeOpened(boolean adviseNodeOpened) {
      this.adviseNodeOpened = adviseNodeOpened;
   }

   public Boolean adviseNodeOpened(final UITree tree) {
      return this.adviseNodeOpened;
   }

   public boolean isSelectedMessagePartEdited() {
      return this.selectedMessagePartEdited;
   }

   public void setSelectedMessagePartEdited(final boolean selectedMessagePartEdited) {
      this.selectedMessagePartEdited = selectedMessagePartEdited;
   }

   public String getValidationTypeListAsString(Analysis analysis) {
      return AnalysisUtils.getValidationTypeListAsString(analysis.getValidationTypes());
   }

   public void logUnexpectedException(Exception e) {
      GuiMessage.logMessage(StatusMessage.Severity.ERROR, MessageFormat.format(I18n.get("net.ihe.gazelle.mca.PleaseContactAnAdministrator"),e.getMessage()));
   }

   public void logFolderDownloadError() {
      GuiMessage.logMessage(StatusMessage.Severity.INFO, "net.ihe.gazelle.mca.CannotDownloadFolders");
   }

   public boolean getEditMessageInGui() {
      return editMessageInGui;
   }

   public void setEditMessageInGui(boolean editMessageInGui) {
      this.editMessageInGui = editMessageInGui;
   }

   protected String getValidatorUrl() {
      return McaVueFileNaming.ANALYSIS.getMenuLink();
   }

   public boolean getEditModeEnabled() {
      return editModeEnabled;
   }

   public void setEditModeEnabled(boolean editModeEnabled) {
      this.editModeEnabled = editModeEnabled;
   }

   public void loadAnalysisForValidation(ValidationRef validationRef) {
      try {
         this.setSelectedObject(
                 findAnalysis(
                         analysisPartManager.getAnalysisPart(validationRef)));
         defineMessagePart(
                 selectedObject.getRootAnalysisPart(),
                 selectedObject);
      } catch (Exception e) {
         this.setSelectedObject(null);
      }
   }

   public void shareResult() {
      processingManager.shareResult(selectedObject);
   }

   public boolean getShowLogArea() {
      return !isZipPart(analysisPartToDisplay) || applicationPreferenceManager.getBooleanValue("mca_show_zip_mimtype");
   }

}
