package net.ihe.gazelle.mca.contentanalyzer.application.analyzers.xml;

import gnu.trove.map.hash.THashMap;
import gnu.trove.set.hash.THashSet;
import gnu.trove.stack.TIntStack;
import gnu.trove.stack.array.TIntArrayStack;
import net.ihe.gazelle.evsclient.interlay.gui.document.ContentConverter;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.MessageContentAnalyzer;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.McaConfigDao;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.DocTypeUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.EncodedType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.PlainMatcher;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.RegexpMatcher;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.XpathMatcher;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.XmlNodeMatcher;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.XmlTagConfigInterface;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.annotation.Nullable;
import javax.persistence.Transient;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class TagDetector implements Serializable {
    private static final long serialVersionUID = -5808145503851149781L;
    private static final Logger logger =     Logger.getLogger(TagDetector.class);
    private static final String PART_LOG_TYPE = "XML Tag Detection";

    class OffsetBuilder {
        final int[] lineStart;
        final String message;
        final int documentStart;

        public OffsetBuilder(String message,int documentStart) {
            this.message = message;
            this.documentStart = documentStart;
            lineStart = new int[StringUtils.countMatches(message,"\n")+1];
            lineStart[0]=0;
            int end = lineStart.length-1;
            for (int i=0 ; i<end ; i++) {
                lineStart[i+1] = message.indexOf('\n',lineStart[i])+1;
            }
        }

        private int pos(Locator locator) {
            if (lineStart.length <= 1 && message.split("<").length > 1) {
                return locator.getColumnNumber()-2;
            } else {
                return lineStart[locator.getLineNumber()-1]+locator.getColumnNumber()-1;
            }
        }

        private int posEndOffset(Locator locator) {
            return lineStart[locator.getLineNumber()-1]+locator.getColumnNumber()-1;
        }

        public int startPosition(Locator locator) {
            return documentStart +strOffset(message,message.lastIndexOf('<', pos(locator)-1));
        }
        public int endPosition(Locator locator) {
            return offset(locator);
        }

        private int offset(Locator locator) {
            return documentStart +strOffset(message,posEndOffset(locator));
        }

        public int getDocumentStart() {
            return documentStart;
        }

        private int strOffset(String str, int strIndex) {
            return str.substring(0,strIndex).getBytes(StandardCharsets.UTF_8).length;
        }
    }

    private final McaConfigDao mcaConfigDao;

    @Transient
    private transient Map<String,Map<String,XmlTagConfigInterface>> plainTagConfig;
    @Transient
    private transient Map<String,List<XmlTagConfigInterface>> regexpConfig;
    @Transient
    private transient Map<String,List<XmlTagConfigInterface>> xpathConfig;


    @Transient
    private transient List<AnalysisPart> orphans;
    @Transient
    private transient TIntStack starts = new TIntArrayStack();
    @Transient
    private transient final MessageContentAnalyzer mca;

    public TagDetector(McaConfigDao mcaConfigDao, MessageContentAnalyzer mca) {
        this.mcaConfigDao = mcaConfigDao;
        this.mca = mca;
        this.orphans = new ArrayList<>();
    }

    public void initTagConfigByNamespace() {
        if (plainTagConfig==null) {
            plainTagConfig = new THashMap<>();
            regexpConfig = new THashMap<>();
            xpathConfig = new THashMap<>();
            for (XmlTagConfigInterface c:mcaConfigDao.getAllTags()) {
                XmlNodeMatcher matcher = c.getMatcher();
                String n = c.getNamespace() == null ? "" : c.getNamespace();
                if (PlainMatcher.class.isInstance(matcher)) {
                    Map<String,XmlTagConfigInterface> m = plainTagConfig.get(n);
                    if (m == null) {
                        m=new THashMap<>();
                        plainTagConfig.put(n,m);
                    }
                    m.put(c.getTag(),c);
                } else if (RegexpMatcher.class.isInstance(matcher)) {
                    List<XmlTagConfigInterface> l = regexpConfig.get(n);
                    if (l == null) {
                        l=new ArrayList<>();
                        regexpConfig.put(n,l);
                    }
                    l.add(c);
                } else if (XpathMatcher.class.isInstance(matcher)) {
                    List<XmlTagConfigInterface> l = xpathConfig.get(n);
                    if (l == null) {
                        l=new ArrayList<>();
                        xpathConfig.put(n,l);
                    }
                    l.add(c);
                }
            }
        }
    }

    public Map<String, Map<String,XmlTagConfigInterface>> getPlainTagConfigByNamespace() {
        if (plainTagConfig==null) {
            initTagConfigByNamespace();
        }
        return plainTagConfig;
    }

    public Map<String, List<XmlTagConfigInterface>> getRegexpTagConfigByNamespace() {
        if (regexpConfig==null) {
            initTagConfigByNamespace();
        }
        return regexpConfig;
    }

    public Map<String, List<XmlTagConfigInterface>> getXpathTagConfigByNamespace() {
        if (xpathConfig==null) {
            initTagConfigByNamespace();
        }
        return xpathConfig;
    }

    public void detectTags(final String message, final AnalysisPart parent)
            throws UnexpectedAnalysisException {
        try {
            XmlDocumentReader documentReader = new XmlDocumentReader();
            AnalysisHandler handler = new AnalysisHandler(message,parent.getEncodedType(),parent.getStartOffset());
            documentReader.readXMLFile(new InputSource(
                    new ContentConverter().toInputStream(message)), handler);
            processChildren(message,parent);
        } catch (SAXException | ParserConfigurationException | IOException e) {
            throw new UnexpectedAnalysisException("Error while trying to parse the XML content of the file.", e);
        }
    }

    private void processChildren(String message, final AnalysisPart part) {
        if (orphans !=null&&!orphans.isEmpty()) {
            for (AnalysisPart child : orphans) {
                if (child.getEndOffset() < part.getStartOffset()) {
                    // It's sibling
                } else {
                    // It's children
                    child.setParentPart(part);
                    part.addLog(PART_LOG_TYPE, "Detected " + child.getDocType() + " sub-part");
                    part.getChildPart().add(child);
                }
            }
            // remove attached children from orphans
            for (AnalysisPart child:part.getChildPart()) {
                orphans.remove(child);
            }
        }
        orphans.add(part);
        if (DocTypeUtils.isEmbeddedB64(part.getDocType())) {
            int start = part.getStartOffset();
            int end = part.getEndOffset();
            try {
                String str = new ContentConverter().strByOffsets(message, part.getStartOffset(), part.getEndOffset());
                int x = str.getBytes(StandardCharsets.UTF_8).length;
                str = str.replaceFirst("(?:\\s*<.*?>\\s*)(.*?)","$1");
                int so = x-str.getBytes(StandardCharsets.UTF_8).length;
                str = str.replaceFirst("(</.*)$","");
                int eo = x-so-str.getBytes(StandardCharsets.UTF_8).length;
                part.setStartOffset(part.getStartOffset()+so);
                part.setEndOffset(part.getEndOffset()-eo);
                mca.analyzeMessageContent(str.replaceAll("\\s*","")
                                .getBytes(StandardCharsets.UTF_8)
                        ,part);
            } catch (UnexpectedAnalysisException e) {
                part.addLog("Embedded BASE64 Tag Detection", "Error while parsing BASE64 : "+e.getMessage());
            } finally {
                part.setStartOffset(start);
                part.setEndOffset(end);
            }
        }
    }

    private class AnalysisHandler extends DefaultHandler {
        private final OffsetBuilder offsetBuilder;
        private final String message;
        private final EncodedType encodedType;
        private Locator locator;
        @Transient
        private transient Deque<Map<String,String>> namespaces;

        public AnalysisHandler(String message,EncodedType encodedType,int documentStart) {
            this.offsetBuilder = new OffsetBuilder(message,documentStart);
            this.message = message;
            this.encodedType = encodedType;
            this.namespaces = new ArrayDeque<>();
        }

        @Override
        public void setDocumentLocator(Locator locator){
            this.locator = locator;
        }

        @Override
        public void startElement(final String uri, final String localName, final String qName,
                                 final Attributes attributes)
                throws SAXException {
            starts.push(offsetBuilder.startPosition(locator));
//            if (logger.isDebugEnabled()) {
//                logger.debug("I am now on line " + locator.getLineNumber() + " column " + locator.getColumnNumber() + " (position=[" + (starts.peek() - offsetBuilder.getDocumentStart())+","+offsetBuilder.pos(locator) + "]) at start element " + qName);
//                String starttag = new ContentConverter().strByOffsets(message, starts.peek() - offsetBuilder.getDocumentStart(), offsetBuilder.pos(locator)+1);
//                logger.debug(starttag);
//            }
            pushNamespace(attributes);

        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            int partStartOffset = starts.pop();
            int partEndOffset = offsetBuilder.endPosition(locator);
            if (logger.isDebugEnabled()) {
                logger.debug("I am now on line " + locator.getLineNumber() + " column " + locator.getColumnNumber() + " (position=" + partEndOffset + ") at end element " + qName);
                String content = new ContentConverter().strByOffsets(message,partStartOffset - offsetBuilder.getDocumentStart(), partEndOffset - offsetBuilder.getDocumentStart());
                logger.debug(content);
            }
            Map<String, String> n = popNamespace();
            String namespace = detectTagNamespace(qName,n);
            String tagname = detectTagName(localName,qName);
            processTag(partStartOffset, partEndOffset, n, namespace, tagname);
        }
        private void processTag(final int partStartOffset, final int partEndOffset, final Map<String, String> namespaces, final String namespace, final String tagname) {
            XmlTagConfigInterface found = findByNamespace(partStartOffset-offsetBuilder.getDocumentStart(), partEndOffset-offsetBuilder.getDocumentStart(), namespace, tagname, namespaces);
            if (found==null) {
                List<XmlTagConfigInterface> loose = findLoosely(partStartOffset-offsetBuilder.getDocumentStart(), partEndOffset-offsetBuilder.getDocumentStart(), tagname);
                if (!loose.isEmpty()) {
                    String concatenatedDocType = concatenateDocTypes(loose);
                    AnalysisPart part = createPart(
                            partStartOffset,
                            partEndOffset,
                            namespaces,
                            "The tag " + tagname + " has no defined namespace in the document. " +
                                    "It may be of type " + concatenatedDocType + " listed in configuration",
                            getEncodedType(loose),
                            concatenatedDocType
                    );
                    part.setNoDefinedNamespace(true);
                }
            } else {
                AnalysisPart part = createPart(
                        partStartOffset,
                        partEndOffset,
                        namespaces,
                        tagname +" tag found in configuration with namespace : "+found.getNamespace(),
                        getEncodedType(found),
                        found.getDocType()
                );
                part.setValidationType(found.getValidationType());
            }
        }

        private EncodedType getEncodedType(List<XmlTagConfigInterface> loose) {
            if (loose.size()==1) {
                return getEncodedType(loose.get(0));
            }
            return encodedType;
        }

        private EncodedType getEncodedType(XmlTagConfigInterface found) {
            if (DocTypeUtils.isEmbeddedB64(found.getDocType())) {
                // it's an embedded b64 pdf
                return EncodedType.B64_ENCODED;
            }
            return encodedType;
        }

        private AnalysisPart createPart(final int start, final int end, final Map<String, String> n, String partlog, EncodedType encodedType, String docType) {
            AnalysisPart part = new AnalysisPart();
            part.addLog(PART_LOG_TYPE, partlog);
            part.setStartOffset(start);
            part.setEndOffset(end);
            if (logger.isDebugEnabled()) {
                String decoded = new ContentConverter().strByOffsets(message,start - offsetBuilder.getDocumentStart(), end - offsetBuilder.getDocumentStart());
                logger.info("decoded="+decoded);
            }
            part.setNamespaces(n);
            part.setEncodedType(encodedType);
            part.setDocType(docType);
            processChildren(message, part);
            return part;
        }

        private String concatenateDocTypes(List<XmlTagConfigInterface> tagConfigList) {
            Set<String> types = new THashSet<>();
            for (int i = 0; i < tagConfigList.size(); i++) {
                types.add(tagConfigList.get(i).getDocType());
            }
            StringBuilder concatenatedDocType = new StringBuilder();
            for (String t:types) {
                if (concatenatedDocType.length()>0) {
                    concatenatedDocType.append(" or ");
                }
                concatenatedDocType.append(t);
            }
            return concatenatedDocType.toString();
        }

        private List<XmlTagConfigInterface> findLoosely(final int start, final int end, final String tagname) {
            List<XmlTagConfigInterface> found = new ArrayList<>();
            for (Map.Entry<String,Map<String,XmlTagConfigInterface>> e:getPlainTagConfigByNamespace().entrySet()) {
                Map<String, XmlTagConfigInterface> pc = e.getValue();
                if (pc.containsKey(tagname)) {
                    found.add(pc.get(tagname));
                }
            }
            for (Map.Entry<String,List<XmlTagConfigInterface>> e:getRegexpTagConfigByNamespace().entrySet()) {
                for (XmlTagConfigInterface c : e.getValue()) {
                    if (((RegexpMatcher) c.getMatcher()).match(tagname)) {
                        found.add(c);
                    }
                }
            }
            for (Map.Entry<String,List<XmlTagConfigInterface>> e:getXpathTagConfigByNamespace().entrySet()) {
                for (XmlTagConfigInterface c : e.getValue()) {
                    String str = new ContentConverter().strByOffsets(message,start, end);
                    if (((XpathMatcher)c.getMatcher()).match(str,tagname,null)) {
                        found.add(c);
                    }
                }
            }
            return found;
        }
        @Nullable
        private XmlTagConfigInterface findByNamespace(final int start, final int end, final String namespace, final String tagname, Map<String,String> namespaces) {
            if (namespace==null) {
                return null;
            }
            XmlTagConfigInterface found = null;
            found = findByPlainTagConfig(namespace, tagname);
            if (found == null) {
                found = findByRegexpTagConfig(namespace, tagname);
            }
            if (found == null) {
                found = findByXpathTagConfig(start, end, namespace, tagname, namespaces);
            }
            return found;
        }

        private XmlTagConfigInterface findByXpathTagConfig(int start, int end, String namespace, String tagname, Map<String, String> namespaces) {
            XmlTagConfigInterface found = null;
            if (getXpathTagConfigByNamespace().containsKey(namespace)) {
                for (XmlTagConfigInterface c : getXpathTagConfigByNamespace().get(namespace)) {
                    if (((XpathMatcher)c.getMatcher()).match(new ContentConverter().strByOffsets(message,start, end), tagname, namespaces)) {
                        found = c;
                        break;
                    }
                }
            }
            return found;
        }

        private XmlTagConfigInterface findByRegexpTagConfig(String namespace, String tagname) {
            XmlTagConfigInterface found = null;
            if (getRegexpTagConfigByNamespace().containsKey(namespace)) {
                for (XmlTagConfigInterface c : getRegexpTagConfigByNamespace().get(namespace)) {
                    if (((RegexpMatcher) c.getMatcher()).match(tagname, namespace)) {
                        found = c;
                        break;
                    }
                }
            }
            return found;
        }

        private XmlTagConfigInterface findByPlainTagConfig(String namespace, String tagname) {
            XmlTagConfigInterface found = null;
            if (getPlainTagConfigByNamespace().containsKey(namespace)) {
                Map<String, XmlTagConfigInterface> pc = getPlainTagConfigByNamespace().get(namespace);
                found = pc.get(tagname);
            }
            return found;
        }

        private void pushNamespace(Attributes attributes) {
            Map<String,String> n = namespaces.isEmpty() ? new THashMap<String,String>() : namespaces.peek();
            String namespace = "";
            for (int x = 0; x < attributes.getLength(); x++) {
                if (attributes.getQName(x).startsWith("xmlns")) {
                    namespace = attributes.getValue(x);
                    n = new THashMap<String,String>(n);
                    n.put(attributes.getQName(x),namespace);
                }
            }
            namespaces.push(n);
        }
        private Map<String, String> peekNamespace() {
            return namespaces.isEmpty() ? new THashMap<String,String>() : namespaces.peek();
        }
        private Map<String, String> popNamespace() {
            return namespaces.isEmpty() ? new THashMap<String,String>() : namespaces.pop();
        }
        private String detectTagName(final String localName, final String qName) {
            if (StringUtils.isNotEmpty(localName)) {
                return localName;
            }
            if (qName.contains(":")) {
                return qName.substring(qName.indexOf(':')+1);
            } else {
                return qName;
            }
        }
        private String detectTagNamespace(final String qName, Map<String, String> n) {
            if (qName.contains(":")) {
                return n.get("xmlns:" + qName.substring(0, qName.indexOf(':')));
            } else {
                return n.get("xmlns");
            }
        }
    }
}