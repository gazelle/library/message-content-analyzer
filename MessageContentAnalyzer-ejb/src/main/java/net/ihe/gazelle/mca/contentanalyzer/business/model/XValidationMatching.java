package net.ihe.gazelle.mca.contentanalyzer.business.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "mca_x_validation_matching", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "x_validation_matching_sequence", sequenceName = "mca_x_validation_matching_id_seq", allocationSize = 1)
public class XValidationMatching {

    @Id
    @NotNull
    @GeneratedValue(generator = "x_validation_matching_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "x_val_inpt_type")
    private String xValInputType;

    @Column(name = "x_validator_oid")
    private String xValidatorOid;

    public XValidationMatching() {

    }

    public XValidationMatching(String xValInputType, String xValidatorOid) {
        this.xValInputType = xValInputType;
        this.xValidatorOid = xValidatorOid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getxValInputType() {
        return xValInputType;
    }

    public void setxValInputType(String xValInputType) {
        this.xValInputType = xValInputType;
    }

    public String getxValidatorOid() {
        return xValidatorOid;
    }

    public void setxValidatorOid(String xValidatorOid) {
        this.xValidatorOid = xValidatorOid;
    }

    @Override
    public String toString() {
        return "XValidationMatching{" +
                "id=" + id +
                ", xValInputType='" + xValInputType + '\'' +
                ", xValidatorOid='" + xValidatorOid + '\'' +
                '}';
    }
}
