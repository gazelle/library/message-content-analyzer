package net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;

import java.util.List;

public interface ZipStructureInterface{

    String getArchiveName();

    List<FileInterface> getFileChildren();

    List<FolderInterface> getFolderChildren();

    String getXValidatorOid();

    String getDocType();

    ValidationType getValidationType();
}

