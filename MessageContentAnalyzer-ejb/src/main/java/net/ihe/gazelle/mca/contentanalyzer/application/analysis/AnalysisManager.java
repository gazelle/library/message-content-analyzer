package net.ihe.gazelle.mca.contentanalyzer.application.analysis;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.AbstractProcessingManagerImpl;
import net.ihe.gazelle.evsclient.application.interfaces.OidGeneratorManager;
import net.ihe.gazelle.evsclient.application.interfaces.ProcessingCacheManager;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.domain.processing.*;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.interlay.dao.HandledObjectFile;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.AnalysisDao;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.DecodedPartNotSavedException;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.AnalysisUtils;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.DocTypeUtils;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.FileContentUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AnalysisManager extends AbstractProcessingManagerImpl<Analysis> implements Serializable {
    private static final long serialVersionUID = 8399965488523617862L;
    private static final Logger LOGGER = LoggerFactory.getLogger(AnalysisManager.class);



    private final transient MetadataServiceProvider metadataServiceProvider;
    private final AnalysisDao analysisDao;

    private final ProcessingCacheManager analysisCacheManager;

    public AnalysisManager(AnalysisDao analysisDao, ApplicationPreferenceManager applicationPreferenceManager,
                           OidGeneratorManager oidGenerator,
                           MetadataServiceProvider metadataServiceProvider, ProcessingCacheManager analysisCacheManager) {
        super(analysisDao, applicationPreferenceManager, oidGenerator);
        this.analysisCacheManager = analysisCacheManager;
        this.metadataServiceProvider = metadataServiceProvider;
        this.analysisDao = analysisDao;
    }

    public Analysis create(HandledObject object, EVSCallerMetadata evsCallerMetadata, OwnerMetadata ownerMetadata) {
        Analysis analysis = new Analysis(object, evsCallerMetadata, ownerMetadata);
        super.init(analysis);
        // Get the version of the analyzer (EVSClient Version)
        analysis.setContentAnalyzerVersion(metadataServiceProvider.getMetadata().getVersion());
        AnalysisUtils.addRootAnalysisPart(object, analysis);
        return analysis;
    }

    public void addAllValidationTypesFromResult(Analysis analysis) {
        final List<ValidationType> listAmpValType = new ArrayList<>();
        getAllAmpValidationTypes(analysis.getRootAnalysisPart(), listAmpValType);
        analysis.setValidationTypes(listAmpValType);
    }

    private void getAllAmpValidationTypes(AnalysisPart analysisPart, List<ValidationType> listAmpValType) {

        if (analysisPart.getValidationType() != null) {
            listAmpValType.add(analysisPart.getValidationType());
        }
        for (final AnalysisPart childMessagePart : analysisPart.getChildPart()) {
            getAllAmpValidationTypes(childMessagePart, listAmpValType);
        }
    }

    public Analysis persistAnalysisData(Analysis analysis) throws UnexpectedProcessingException, NotLoadedException, ProcessingNotSavedException, DecodedPartNotSavedException {
        final Boolean isDecodedStorageEnabled = applicationPreferenceManager
                .getBooleanValue("mca_enable_decoded_storage");

        Map<AnalysisPart, byte[]> decodedParts =  new HashMap<>();
        putAllDecodedPartLocally(decodedParts, analysis.getRootAnalysisPart());

        // Locally stored because transient field to avoid reload from getObjectContent(analysis);
        // byte[] messageContent = analysis.getObjects().iterator().next().getContent();
        analysis = analysisDao.create(analysis);
        //createFile(messageContent, analysis.getObject().getFilePath());


        //FIXME next lines should be done by a DAO
        if(isDecodedStorageEnabled != null && isDecodedStorageEnabled) {
            createFileForDecodedParts(decodedParts, ((HandledObjectFile)analysis.getObject()).getFilePath(),
                  analysis.getOid());
        }

        // Update the cache with the newest values
        EVSCallerMetadata caller = analysis.getCaller();
        if (caller instanceof OtherCallerMetadata) {
            analysisCacheManager.refreshGazelleCacheWebService(((OtherCallerMetadata) caller).getToolObjectId(), ((OtherCallerMetadata) caller).getToolOid(),
                    analysis.getOid(), "off");
        }

        return analysis;
    }

    //FIXME To move in a DAO
    private void createFileForDecodedParts(Map<AnalysisPart, byte[]> decodedParts, String originalContentFilePath,
                                                String analysisOid) throws DecodedPartNotSavedException {

        for (Map.Entry<AnalysisPart, byte[]> entry : decodedParts.entrySet()) {

            String decodedFilePath = FileContentUtils.computeEditedPartPath(originalContentFilePath, entry.getKey().getOid(),
                    analysisOid);

            setDecodedPartFilePathToAnalysisPartAndChildrenPart(entry.getKey(), decodedFilePath);
            createDecodedPartFile(entry.getValue(), decodedFilePath);
        }
    }

    public HQLCriterionsForFilter<Analysis> getCriterionForFilter(String login, String organization, boolean isMonitor, boolean isLoggedIn) {
        return analysisDao.getCriterionForFilter(login, organization, isMonitor, isLoggedIn);
    }

    private void putAllDecodedPartLocally(Map<AnalysisPart, byte[]> decodedParts, AnalysisPart parent){

        for(AnalysisPart child : parent.getChildPart()){
            if(child.getDecodedPart()!=null){
                decodedParts.put(child, child.getDecodedPart());
            }
            putAllDecodedPartLocally(decodedParts, child);
        }
    }

    public void createDecodedPartFile(byte[] messageContent, String filePath) throws DecodedPartNotSavedException {
        analysisDao.createDecodedPartFile(messageContent, filePath);
    }

    public void setDecodedPartFilePathToAnalysisPartAndChildrenPart(AnalysisPart analysisPart, String newDecodedPartFilePath) {

        if (analysisPart.getDecodedPartFilePath() == null && (analysisPart.getNamespaces() == null
                || analysisPart.getNamespaces().isEmpty())) {
            analysisPart.setDecodedPartFilePath(newDecodedPartFilePath);
        }

        if (analysisPart.getChildPart() != null) {
            for (AnalysisPart child: analysisPart.getChildPart()) {
                if (child.getEncodedType().equals(analysisPart.getEncodedType()) && !DocTypeUtils.isBase64(analysisPart.getDocType())
                        && !DocTypeUtils.isZip(analysisPart.getDocType())) {
                    setDecodedPartFilePathToAnalysisPartAndChildrenPart(child, newDecodedPartFilePath);
                }
            }
        }
    }

    @Deprecated
    public String getAnalyzedObjectFilePath(Analysis analysis) {
        return analysisDao.getObjectDAOImpl(analysis).getFilePath();
    }

}
