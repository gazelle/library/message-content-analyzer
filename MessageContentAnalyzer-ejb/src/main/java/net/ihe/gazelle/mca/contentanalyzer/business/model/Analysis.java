/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.mca.contentanalyzer.business.model;

import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "mca_analysis", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "analysis_sequence", sequenceName = "mca_analysis_id_seq", allocationSize = 1)
public class Analysis extends AbstractProcessing {

    private static final long serialVersionUID = -1232813067358180925L;

    public static final String AO_FILE_PREFIX = "analysis_";
    public static final String AO_FILE_SUFFIX = ".mca";

    @Column(name = "content_analyzer_version")
    private String contentAnalyzerVersion;

    @ElementCollection
    @CollectionTable(name="mca_validation_types", joinColumns=@JoinColumn(name="analysis_id"))
    @Column(name="validation_type")
    @Enumerated(EnumType.STRING)
    private List<ValidationType> validationTypes;

    @ManyToOne(targetEntity = AnalysisPart.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "root_analysis_part_id")
    private AnalysisPart rootAnalysisPart;

    @Enumerated(EnumType.STRING)
    @Column(name ="analysis_status")
    private AnalysisStatus analysisStatus = AnalysisStatus.PENDING;

    public Analysis() {
        super();
    }
    public Analysis(List<HandledObject> objects, EVSCallerMetadata caller) {
        super(objects, caller);
    }

    public Analysis(List<HandledObject> objects, EVSCallerMetadata caller, OwnerMetadata ownerMetadata) {
        super(objects, caller, ownerMetadata);
    }

    public Analysis(HandledObject object, EVSCallerMetadata caller) {
        this(new ArrayList<>(Arrays.asList(new HandledObject[]{object})), caller);
    }

    public Analysis(HandledObject object, EVSCallerMetadata caller, OwnerMetadata ownerMetadata) {
        this(new ArrayList<>(Arrays.asList(new HandledObject[]{object})), caller, ownerMetadata);
    }

    public String getContentAnalyzerVersion() {
        return contentAnalyzerVersion;
    }

    public void setContentAnalyzerVersion(String contentAnalyzerVersion) {
        this.contentAnalyzerVersion = contentAnalyzerVersion;
    }

    public List<ValidationType> getValidationTypes() {
        return validationTypes;
    }

    public void setValidationTypes(List<ValidationType> validationTypes) {
        this.validationTypes = validationTypes;
    }

    public AnalysisPart getRootAnalysisPart() {
        return rootAnalysisPart;
    }

    public void setRootAnalysisPart(AnalysisPart rootAnalysisPart) {
        this.rootAnalysisPart = rootAnalysisPart;
        rootAnalysisPart.setAnalysis(this);
    }

    public AnalysisStatus getAnalysisStatus() {
        return analysisStatus;
    }

    public void setAnalysisStatus(AnalysisStatus analysisStatus) {
        this.analysisStatus = analysisStatus;
    }

    @Override
    public void setOid(String oid) {
        super.setOid(oid);
        if (rootAnalysisPart != null) {
            rootAnalysisPart.setOid(oid);
        }
    }

    public boolean equals(final Object obj) {
        if (! super.equals(obj)) {
            return false;
        }
        final Analysis fobj = (Analysis) obj;
        return this.getId().equals(fobj.getId());  // added fields are tested
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.getOid() == null ? 0 : this.getOid().hashCode());
        return result;
    }

}