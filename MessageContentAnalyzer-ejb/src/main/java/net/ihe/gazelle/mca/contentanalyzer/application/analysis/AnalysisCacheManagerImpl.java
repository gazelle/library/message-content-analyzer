package net.ihe.gazelle.mca.contentanalyzer.application.analysis;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.AbstractGuiPermanentLink;
import net.ihe.gazelle.evsclient.application.AbstractProcessingCacheManager;
import net.ihe.gazelle.mca.contentanalyzer.adapters.common.gui.McaVueFileNaming;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.AnalysisDao;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;

public class AnalysisCacheManagerImpl extends AbstractProcessingCacheManager<Analysis> {

    public AnalysisCacheManagerImpl(ApplicationPreferenceManager applicationPreferenceManager, AnalysisDao analysisDao) {
        super(applicationPreferenceManager, analysisDao);
    }

    @Override
    public String getStatus(Analysis analysis) {
        return analysis.getAnalysisStatus().name();
    }

    @Override
    public String getPermanentLink(Analysis analysis) {
        return new AbstractGuiPermanentLink(applicationPreferenceManager) {
            @Override
            public String getResultPageUrl() {
                return McaVueFileNaming.DETAILED_RESULT.getMenuLink();
            }
        }.getPermanentLink(analysis);
    }
}
