/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.mca.contentanalyzer.application.converters;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.mca.contentanalyzer.application.files.TmpFilesManager;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;

public class DicomToTxtConverter implements Serializable {
    private static final long serialVersionUID = 5654656904805366674L;

    private static final Logger LOGGER = LoggerFactory.getLogger(DicomToTxtConverter.class);
    private static final String OS = System.getProperty("os.name").toLowerCase(Locale.getDefault());


    private final ApplicationPreferenceManager applicationPreferenceManager;
    private final TmpFilesManager tmpFilesManager;

    public DicomToTxtConverter(ApplicationPreferenceManager applicationPreferenceManager, TmpFilesManager tmpFilesManager) {
        this.applicationPreferenceManager = applicationPreferenceManager;
        this.tmpFilesManager = tmpFilesManager;
    }

    public byte[] dicom2txt(final byte[] messageBytesContent) throws UnexpectedAnalysisException{

        final File dcmFile = tmpFilesManager.createTmpFileFromByte("temp-file-viewFile-dicom",
                messageBytesContent);
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        LOGGER.info(DicomToTxtConverter.OS);
        String pathToDcmDump = applicationPreferenceManager.getStringValue("path_to_dcmDump");
        if (pathToDcmDump == null) {
            if (isMac()) {
                pathToDcmDump = "/opt/local/bin/dcmdump";
            } else {
                pathToDcmDump = "/usr/bin/dcmdump";
            }
        }
        final File dcmdump = new File(pathToDcmDump);

        if (!dcmdump.exists()) {
            DicomToTxtConverter.LOGGER.error("Please install dcmdump (from DCMTK) in {}", pathToDcmDump);
            tmpFilesManager.deleteTmpFile(dcmFile);
            throw new UnexpectedAnalysisException("dcmdump is not installed on the server.");
        } else {
            try {
                final CommandLine commandLine = new CommandLine(pathToDcmDump);
                commandLine.addArgument(dcmFile.getCanonicalPath());
                final PumpStreamHandler streamHandler = new PumpStreamHandler(outputStream);
                final DefaultExecutor executor = new DefaultExecutor();
                executor.setStreamHandler(streamHandler);
                executor.execute(commandLine);
            } catch (final IOException e) {
                LOGGER.error("Error in the transformation of DICOM to text ", e);
                tmpFilesManager.deleteTmpFile(dcmFile);
                throw new UnexpectedAnalysisException("Error in the transformation of DICOM to text !", e);
            }
        }
        tmpFilesManager.deleteTmpFile(dcmFile);
        return outputStream.toByteArray();
    }

    private boolean isMac() {
        return OS.contains("mac");
    }

    private boolean isSolaris() {
        return OS.contains("sunos");
    }

    private boolean isUnix() {
        return OS.contains("nix") || OS.contains("nux") || OS.contains("aix");
    }

    private boolean isWindows() {
        return OS.contains("win");
    }
}
