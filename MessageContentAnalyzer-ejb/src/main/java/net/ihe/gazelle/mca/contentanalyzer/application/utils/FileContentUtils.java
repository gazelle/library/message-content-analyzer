package net.ihe.gazelle.mca.contentanalyzer.application.utils;

import net.ihe.gazelle.evsclient.interlay.factory.DocumentBuilderFactoryException;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipInputStream;

public class FileContentUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileContentUtils.class);

    public static final String DECODED_FILE_PREFIX = "decodedPart_";

    public static String computeEditedPartPath(String originalContentPath, String subpartOID, String analysisOID) {

        String subpartKey = subpartOID.replace(analysisOID + ".", "");
        return originalContentPath.replace(Analysis.AO_FILE_PREFIX,
                DECODED_FILE_PREFIX + subpartKey.replace(".", "_") + "_");
    }

    public static byte[] zipExtractFile(ZipInputStream zipIn) throws IOException {
        ByteArrayOutputStream bos ;
        bos = new ByteArrayOutputStream();
        byte[] bytesIn = new byte[4096];
        int read;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
        return bos.toByteArray();
    }

    public static String xmlAddNamespacesToFileContent(final String fileContent, final Map<String, String> namespaces) {

        LOGGER.info("Look for Missing Namespaces");

        try {
            searchAllNamespacesToFindMissing(fileContent);
            return fileContent;
        } catch (final SAXException e) {

            String contentWithAddedNamespaces = fileContent;
            final String errMess = e.getMessage();
            final String firstQuoteReg = "(\"([^\"]+)\")";
            final Pattern regex = Pattern.compile(firstQuoteReg, Pattern.DOTALL);
            final Matcher regexMatcher = regex.matcher(errMess);
            String missingNameSpace = null;
            if (regexMatcher.find()) {
                missingNameSpace = regexMatcher.group(2);
                missingNameSpace = "xmlns:" + missingNameSpace;
                LOGGER.info("missingNameSpace : {}", missingNameSpace);
            }
            String missingTag = null;
            if (regexMatcher.find()) {
                missingTag = regexMatcher.group(2);
                LOGGER.info("missingTag : {}", missingTag);
            }
            if (namespaces != null && !namespaces.isEmpty()
                    && missingNameSpace != null && missingTag != null) {

                LOGGER.info("Adding Missing Namespaces");

                final String nsURI = namespaces.get(missingNameSpace);
                LOGGER.info("{}={}", missingNameSpace, nsURI);

                String fileContentAddedNamespaces ;

                if (contentWithAddedNamespaces.contains('<' + missingTag)) {
                    missingTag = '<' + missingTag;
                    final String newTag = missingTag + ' ' + missingNameSpace + "=\"" + nsURI + '"';
                    LOGGER.info("Replacing missing tag {} with new tag {}", missingTag, newTag);
                    fileContentAddedNamespaces = contentWithAddedNamespaces.replace(missingTag, newTag);
                    fileContentAddedNamespaces = xmlAddNamespacesToFileContent(fileContentAddedNamespaces, namespaces);
                    return fileContentAddedNamespaces;
                } else if (contentWithAddedNamespaces.contains(missingTag + '=')) {
                    missingTag = missingTag + '=';
                    final String newTag = missingNameSpace + "=\"" + nsURI + "\" " + missingTag;
                    LOGGER.info("Replacing missing attribute {} with new attribute {}", missingTag, newTag);
                    fileContentAddedNamespaces = contentWithAddedNamespaces.replace(missingTag, newTag);
                    fileContentAddedNamespaces = xmlAddNamespacesToFileContent(fileContentAddedNamespaces, namespaces);
                    return fileContentAddedNamespaces;
                }
                return contentWithAddedNamespaces;

            } else {
                LOGGER.info("No missing namespaces");
                return fileContent;
            }
        }
    }

    private static void searchAllNamespacesToFindMissing(final String fileContent) throws SAXException {
        Document document;
        try {

            final DocumentBuilder builder = new XmlDocumentBuilderFactory()
                    .setNamespaceAware(true)
                    .setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true)
                    .getBuilder();
            document = builder.parse(new ByteArrayInputStream(fileContent.getBytes(StandardCharsets.UTF_8)));

            final XPathFactory xPathFactory = XPathFactory.newInstance();
            final XPath xpath = xPathFactory.newXPath();
            try {
                xpath.evaluate("//namespace::*", document, XPathConstants.NODESET);
            } catch (final XPathExpressionException e) {
                LOGGER.error("Error when evaluate xpath", e);
            }
        } catch (final DocumentBuilderFactoryException e) {
            LOGGER.error("Error when creating document builder", e);
        } catch (final IOException e) {
            LOGGER.error("Error when reading file", e);
        }
    }
}
