package net.ihe.gazelle.mca.contentanalyzer.application.analyzers.xml;

import javax.xml.stream.XMLInputFactory;

public interface XmlInputFactoryFactory {

    XMLInputFactory configureAndGetXmlInputFactory();
}
