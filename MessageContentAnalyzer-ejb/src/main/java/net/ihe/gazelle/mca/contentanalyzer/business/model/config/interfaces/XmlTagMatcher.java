package net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces;

public interface XmlTagMatcher extends XmlNodeMatcher {
    boolean match(String tag, String namespace);
    boolean match(String tag);
}
