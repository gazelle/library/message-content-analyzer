package net.ihe.gazelle.mca.contentanalyzer.application.interfaces;

import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingDao;
import net.ihe.gazelle.evsclient.interlay.dao.HandledObjectFile;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;

public interface AnalysisDao extends ProcessingDao<Analysis> {

    void createDecodedPartFile(byte[] messageContent, String filePath) throws DecodedPartNotSavedException;

    Analysis getByParentAnalysisPart(AnalysisPart parentAnalysisPart);

    HQLCriterionsForFilter<Analysis> getCriterionForFilter(String login, String organization, boolean isMonitor, boolean isLoggedIn);

    /**
     * Get sub-type implementation of {@link HandledObject} used as proxy for the file persistence.
     *
     * @param analysis processing owning the HandledObject.
     *
     * @return an instance of {@link HandledObjectFile}
     *
     * @deprecated This method is exposing internal information of the DAO mechanism and is violating responsabilies.
     * However some legacy code are still coupled with the file mechanism (such as MCA) and must be refactored first
     * before deleting this method.
     */
    @Deprecated
    HandledObjectFile getObjectDAOImpl(Analysis analysis);

}
