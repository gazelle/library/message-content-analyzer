package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ContentAnalysisConfigInterface;

import java.util.Arrays;
import java.util.List;

public enum ContentAnalysisConfigEnum implements ContentAnalysisConfigInterface {

    HTTP("HTTP", null, Arrays.asList("GET", "HEAD", "POST", "PUT", "DELETE", "TRACE", "CONNECT"), null,
            null, null),

    MTOM("MTOM", null, null, null, "^( )*--[^>]((?!-*BEGIN CERTIFICATE)).+", null),

    SYSLOG("SYSLOG", null, Arrays.asList("<"), null, "(<[0-9]{2,3}>1 [^<]*)<(\\?xml|AuditMessage)", null);


    private final String docType;
    private final ValidationType validationType;
    private final List<String> startsWith;
    private final byte[] bytePattern;
    private final String stringPattern;
    private final String unwantedContent;
    private static final ConfigType configType = ConfigType.CONTENT_ANALYSIS_CONFIGURATION;

    ContentAnalysisConfigEnum(final String docType, final ValidationType validationType,
                              final List<String> startsWith, final byte[] bytePattern, final String stringPattern,
                              final String unwantedContent) {
        this.docType = docType;
        this.validationType = validationType;
        this.startsWith = startsWith;
        this.bytePattern = bytePattern;
        this.stringPattern = stringPattern;
        this.unwantedContent = unwantedContent;
    }

    public String getDocType() {
        return docType;
    }

    public ValidationType getValidationType() {
        return validationType;
    }

    public List<String> getStartsWith() {
        return startsWith;
    }

    public byte[] getBytePattern() {
        if (bytePattern != null) {
            return bytePattern.clone();
        } else {
            return null;
        }
    }

    @Override
    public String getSample() {
        return null;
    }

    @Override
    public void setBytePattern(byte[] bytePattern) {
    }

    @Override
    public void setStartsWith(List<String> startsWith) {
    }

    public String getStringPattern() {
        return stringPattern;
    }

    public String getUnwantedContent() {
        return unwantedContent;
    }

    @Override
    public boolean isHardCoded() {
        return true;
    }

    @Override
    public ConfigType getConfigType() {
        return configType;
    }

    @Override
    public String getStartsWithAsString() {
        if (this.startsWith != null && !this.startsWith.isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(startsWith.get(0));
            stringBuilder.append(" ");
            for (int i = 1; i < startsWith.size(); i++) {
                stringBuilder.append(startsWith.get(i));
                stringBuilder.append(" ");
            }
            return stringBuilder.toString();
        }
        return null;
    }
}