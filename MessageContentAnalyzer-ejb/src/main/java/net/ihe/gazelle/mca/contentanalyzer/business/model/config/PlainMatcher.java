package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.XmlTagMatcher;

public class PlainMatcher implements XmlTagMatcher {
    private String tag;
    private String namespace;

    public PlainMatcher(String tag, String namespace) {
        this.tag = tag;
        this.namespace = namespace;
    }

    @Override
    public boolean match(String tag, String namespace) {
        if ((namespace == null && this.namespace == null)) {
            return match(tag);
        } else if (this.namespace != null && namespace != null) {
            return (match(tag) && namespace.equals(this.namespace));
        }
        return false;
    }

    @Override
    public boolean match(String tag) {
        return this.tag.equals(tag);
    }

}
