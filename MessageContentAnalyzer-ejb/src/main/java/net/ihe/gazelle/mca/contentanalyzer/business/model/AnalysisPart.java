package net.ihe.gazelle.mca.contentanalyzer.business.model;

import net.ihe.gazelle.evsclient.domain.validation.ValidationRef;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.interlay.gui.document.ContentConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Entity
@Table(name = "mca_analysis_part", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "analysis_part_sequence", sequenceName = "mca_analysis_part_id_seq", allocationSize = 1)
public class AnalysisPart implements Serializable {

    private static final long serialVersionUID = 6827624080912716763L;
    private static final Logger log = LoggerFactory.getLogger(AnalysisPart.class);

    @Id
    @NotNull
    @GeneratedValue(generator = "analysis_part_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "oid")
    protected String oid;

    @Enumerated(EnumType.STRING)
    @Column(name = "encoded_type")
    private EncodedType encodedType;

    @Transient
    private byte[] decodedPart;

    @Column(name = "start_offset")
    private int startOffset;

    @Column(name = "end_offset")
    private int endOffset;

    @ElementCollection(fetch = FetchType.EAGER)
    @MapKeyColumn(name="namespace_prefix")
    @Column(name="namespace_uri")
    @CollectionTable(name="mca_analysis_part_namespaces", joinColumns=@JoinColumn(name="analysis_part_id"))
    private Map<String, String> namespaces = new ConcurrentHashMap<>();

    @Column( name = "decoded_part_file_path")
    private String decodedPartFilePath;

    @Column(name = "part_log")
    private byte[] partLog;

    @Column(name = "no_defined_namespace")
    private Boolean noDefinedNamespace;

    @Column(name = "validation_type")
    @Enumerated(EnumType.STRING)
    private ValidationType validationType;

    @OneToMany(mappedBy = "parentPart", targetEntity = AnalysisPart.class, cascade = CascadeType.ALL)
    private List<AnalysisPart> childPart = new ArrayList<>();

    @ManyToOne(targetEntity = AnalysisPart.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_part_id")
    private AnalysisPart parentPart;

    @OneToOne(cascade = CascadeType.ALL)
    private XValidationMatching xValMatch;

    @Embedded
    private ValidationRef validation;

    @Column(name = "doc_type")
    private String docType;

    @OneToOne(mappedBy = "rootAnalysisPart", cascade = CascadeType.ALL)
    private Analysis analysis;

    public AnalysisPart() {
        this.xValMatch = new XValidationMatching();
        this.validation = new ValidationRef();
    }

    public AnalysisPart(final AnalysisPart parent) {
        this();
        if(parent!=null){
            this.parentPart = parent;
            this.analysis = parent.getAnalysis();
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public EncodedType getEncodedType() {
        return encodedType;
    }

    public void setEncodedType(EncodedType encodedType) {
        this.encodedType = encodedType;
    }

    public byte[] getDecodedPart() {
        /*
        if (decodedPart != null) {
            return decodedPart.clone();
        } else {
            return null;
        }
        */
        return this.decodedPart;
    }

    public void setDecodedPart(byte[] decodedPart) {
        /*
        if (decodedPart != null) {
            this.decodedPart = decodedPart.clone();
        } else {
            this.decodedPart = null;
        }
        */
        this.decodedPart = decodedPart;
    }

    public ValidationType getValidationType() {
        return this.validationType;
    }

    public void setValidationTypeFromString(final String validationType) {
        if (validationType != null && !validationType.isEmpty()) {
            try {
                this.validationType = ValidationType.valueOf(validationType);
            } catch (IllegalArgumentException e) {
                this.validationType = null;
            }
        } else {
            this.validationType = null;
        }
    }

    public void setValidationType(final ValidationType validationType) {
        this.validationType = validationType;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public ValidationRef getValidation() {
        return validation;
    }

    public void setValidation(ValidationRef validation) {
        this.validation = validation;
    }

    public Analysis getAnalysis() {
        return analysis;
    }

    public void setAnalysis(Analysis analysis) {
        this.analysis = analysis;
        for(AnalysisPart child : childPart) {
            child.setAnalysis(analysis);
        }
    }

    public Map<String, String> getNamespaces() {
        return namespaces;
    }

    public void setNamespaces(Map<String, String> namespaces) {
        this.namespaces = namespaces;
    }

    public void setPartLog(byte[] partLog) {
        this.partLog = partLog;
    }

    public XValidationMatching getxValMatch() {
        return xValMatch;
    }

    public void setxValMatch(XValidationMatching xValMatch) {
        this.xValMatch = xValMatch;
    }

    public int getStartOffset() {
        return this.startOffset;
    }

    public void setStartOffset(final int startOffset) {
        this.startOffset = startOffset;
    }
    
    public int getEndOffset() {
        return this.endOffset;
    }

    public void setEndOffset(final int endOffset) {
        this.endOffset = endOffset;
    }
    public List<AnalysisPart> getChildPart() {
        if (this.childPart == null) {
            this.childPart = new ArrayList<>();
        }
        return this.childPart;
    }

    public void setChildPart(final List<AnalysisPart> childPart) {
        if (childPart != null) {
            this.childPart = new ArrayList<>(childPart);
        }
        else {
            this.childPart = null;
        }
    }

    public AnalysisPart getParentPart() {
        return this.parentPart;
    }

    public void setParentPart(final AnalysisPart parentPart) {
        this.parentPart = parentPart;
    }

    public String getxValInputType() {
        return this.xValMatch.getxValInputType();
    }

    public void setxValInputType(String xValInputType) {
        this.xValMatch.setxValInputType(xValInputType);
    }

    public String getxValidatorOid() {
        return this.xValMatch.getxValidatorOid();
    }

    public void setxValidatorOid(String xValidatorOid) {
        this.xValMatch.setxValidatorOid(xValidatorOid);
    }

    public String getDecodedPartFilePath() {
        return decodedPartFilePath;
    }

    public void setDecodedPartFilePath(String decodedPartFilePath) {
            this.decodedPartFilePath = decodedPartFilePath;
    }

    public String getPartLog() {
        if (this.partLog != null) {
            return new String(this.partLog, StandardCharsets.UTF_8);
        }
        return "";
    }

    public void setPartLog(String partLog) {
        this.partLog = partLog.getBytes(StandardCharsets.UTF_8);
    }

    public Boolean getNoDefinedNamespace() {
        return noDefinedNamespace;
    }

    public void setNoDefinedNamespace(Boolean noDefinedNamespace) {
        this.noDefinedNamespace = noDefinedNamespace;
    }

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////      Public Methods     //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public void addLog(String logType, String log) {
        setPartLog(getPartLog()+"\n"+ "["+logType+"] : " + log);
    }

    public boolean isEmpty() {
        return this.getChildPart().isEmpty();
    }

    public List<AnalysisPart> getXValInputList() {
        List<AnalysisPart> xValInputList = new ArrayList<>();
        if (this.getxValInputType()!=null && !this.getxValInputType().isEmpty()) {
            xValInputList.add(this);
        }
        if (this.getChildPart() != null && !this.getChildPart().isEmpty()) {
            for (AnalysisPart child : this.getChildPart()) {
                xValInputList.addAll(child.getXValInputList());
            }
        }
        return xValInputList;
    }

    public AnalysisPart getRootParent() {
        if (DocType.FILE_FOLDER_ARCHIVE.getValue().equals(this.getDocType())) {
            return this;
        } else if (getParentPart() != null && DocType.FILE_FOLDER_ARCHIVE.getValue().equals(getParentPart().getDocType())) {
            return getParentPart();
        } else if (getParentPart() != null) {
            return getParentPart().getRootParent();
        }
        return null;
    }

    public String dump(byte[] message) {
        return new ContentConverter().strByOffsets(message, startOffset,endOffset);
    }
    public String toString() {
        return MessageFormat.format("{0}[{1},{2}]{3}:{4}",validationType,startOffset,endOffset,getOid(),new String(decodedPart,StandardCharsets.UTF_8));
    }
}
