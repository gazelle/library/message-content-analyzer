package net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces;

import java.util.List;

public interface FolderInterface {

    String getFolderName();

    List<FileInterface> getFileChildren();

    List<FolderInterface> getFolderChildren();
}
