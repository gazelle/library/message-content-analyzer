package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="mcaConfigurations")
@XmlAccessorType(XmlAccessType.FIELD)
public class ConfigsFromDB {

    @XmlElementWrapper(name = "xmlTagConfigList")
    @XmlElementRefs({@XmlElementRef(name = "xmlTagConfig")})
    private List<XmlTagConfigEntity> xmlTagConfigEntities;

    @XmlElementWrapper(name = "contentAnalysisConfigList")
    @XmlElementRefs({@XmlElementRef(name = "contentAnalysisConfig")})
    private List<ContentAnalysisConfigEntity> contentAnalysisConfigEntities;

    @XmlElementWrapper(name = "mimeTypeConfigList")
    @XmlElementRefs({@XmlElementRef(name = "mimeTypeConfig")})
    private List<MimeTypeConfigEntity> mimeTypeConfigEntities;

    public ConfigsFromDB() {
        contentAnalysisConfigEntities = new ArrayList<ContentAnalysisConfigEntity>();
        mimeTypeConfigEntities = new ArrayList<MimeTypeConfigEntity>();
        xmlTagConfigEntities = new ArrayList<XmlTagConfigEntity>();
    }

    public List<XmlTagConfigEntity> getXmlTagConfigEntities() {
        return xmlTagConfigEntities;
    }

    public void setXmlTagConfigEntities(List<XmlTagConfigEntity> xmlTagConfigEntities) {
        this.xmlTagConfigEntities = xmlTagConfigEntities;
    }

    public List<ContentAnalysisConfigEntity> getContentAnalysisConfigEntities() {
        return contentAnalysisConfigEntities;
    }

    public void setContentAnalysisConfigEntities(List<ContentAnalysisConfigEntity> contentAnalysisConfigEntities) {
        this.contentAnalysisConfigEntities = contentAnalysisConfigEntities;
    }

    public List<MimeTypeConfigEntity> getMimeTypeConfigEntities() {
        return mimeTypeConfigEntities;
    }

    public void setMimeTypeConfigEntities(List<MimeTypeConfigEntity> mimeTypeConfigEntities) {
        this.mimeTypeConfigEntities = mimeTypeConfigEntities;
    }
}
