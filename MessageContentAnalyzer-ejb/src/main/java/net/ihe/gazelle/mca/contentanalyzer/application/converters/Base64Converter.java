/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.mca.contentanalyzer.application.converters;

import org.apache.commons.codec.binary.Base64;

import javax.xml.bind.DatatypeConverter;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;

public class Base64Converter implements Serializable {
    private static final long serialVersionUID = -4035965471117257469L;

    public boolean base64Detection(final String fileContent) {

        boolean isBase64 = false;
        final String pattern = "^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{4})$";
        if (fileContent.matches(pattern) || Base64.isArrayByteBase64(fileContent.getBytes(StandardCharsets.UTF_8)) && !fileContent
                .contains("CERTIFICATE")) {
            final byte[] fileByte = fileContent.getBytes(StandardCharsets.UTF_8);
            // Currently the method treats whitespace as valid.
            isBase64 = Base64.isArrayByteBase64(fileByte);
        }
        return isBase64;
    }

    public boolean base64Detection(final byte[] messageBytesContent) {

        String fileContent = new String(messageBytesContent, StandardCharsets.UTF_8).
                replaceAll("(g?)(m?)\\s*","");
        return base64Detection(fileContent);
    }

    public byte[] base64Decoding(final byte[] messageBytesContent) {
        return Base64.decodeBase64(messageBytesContent);
    }

    public byte[] base64Decoding(final String fileContent) {
        return DatatypeConverter.parseBase64Binary(fileContent);
    }
}
