package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FileInterface;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "mca_file_config", schema = "public")
@SequenceGenerator(name = "mca_file_config_sequence", sequenceName = "mca_file_config_id_seq", allocationSize = 1)
public class File implements FileInterface {

    @Id
    @NotNull
    @GeneratedValue(generator = "mca_file_config_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "regex")
    private boolean regex;

    @Column(name = "x_val_input_type")
    private String xValInputType;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public boolean isRegex() {
        return regex;
    }

    public void setRegex(boolean regex) {
        this.regex = regex;
    }

    @Override
    public String getXValInputType() {
        return xValInputType;
    }

    public void setXValInputType(String xValInputType) {
        this.xValInputType = xValInputType;
    }

}
