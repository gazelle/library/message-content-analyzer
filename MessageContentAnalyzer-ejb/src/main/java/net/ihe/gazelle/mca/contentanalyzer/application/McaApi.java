package net.ihe.gazelle.mca.contentanalyzer.application;

import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.interlay.gui.document.ContentConverter;
import net.ihe.gazelle.evsclient.interlay.gui.document.ZipReader;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.MessageContentAnalyzer;
import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.MimeTypeDetector;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.Base64Converter;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.DicomToTxtConverter;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.AnalysisUtils;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.DocTypeUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.*;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.zip.ZipInputStream;

public class McaApi implements Serializable {
    private static final long serialVersionUID = 6827846040769061764L;
    private static final Logger LOGGER = LoggerFactory.getLogger(McaApi.class);
    private static final int TO0_BIG_TO_KEEP_IN_MEMORY = 1000000;

    private final AnalysisManager analysisManager;

    private final MessageContentAnalyzer messageContentAnalyzer;

    private final DicomToTxtConverter dicomToTxtConverter;

    private final Base64Converter base64Converter;

    public McaApi(AnalysisManager analysisManager, MessageContentAnalyzer messageContentAnalyzer,
                  DicomToTxtConverter dicomToTxtConverter, Base64Converter base64Converter) {
        this.analysisManager = analysisManager;
        this.messageContentAnalyzer = messageContentAnalyzer;
        this.dicomToTxtConverter = dicomToTxtConverter;
        this.base64Converter = base64Converter;
    }

    public Analysis analyze(Analysis analysis) throws UnexpectedAnalysisException {

        analysis.setAnalysisStatus(AnalysisStatus.ANALYSIS_IN_PROGRESS);

        AnalysisPart parent = analysis.getRootAnalysisPart();
        HandledObject handledObject = analysis.getObjects().iterator().next();
        messageContentAnalyzer.setMessageName(handledObject.getOriginalFileName().getBytes(StandardCharsets.UTF_8));
        messageContentAnalyzer.analyzeMessageContent(handledObject.getContent(), parent);

        AnalysisPart topAMP = new AnalysisPart(null);
        topAMP.setDocType(DocType.FILE_FOLDER_ARCHIVE.getValue());

        topAMP.getChildPart().add(parent);
        parent.setParentPart(topAMP);

        analysis.setRootAnalysisPart(topAMP);

        AnalysisUtils.setOidForAllAnalysisPartChildren(analysis.getOid(), topAMP);

        analysisManager.addAllValidationTypesFromResult(analysis);

        return analysis;
    }

    public byte[] getDecodedPartSource(final String objectPath, final AnalysisPart node) throws UnexpectedAnalysisException {

        if (node.getDecodedPart()==null && node.getDecodedPartFilePath()!=null) {
            // decoded part already persisted but not loaded
            try {
                node.setDecodedPart(
                        FileUtils.readFileToByteArray(new File(node.getDecodedPartFilePath()))
                );
            } catch (IOException e) {
                LOGGER.error("Unable to read file : ",e);
            }
        }

        if (node.getDecodedPart()!=null&&DocTypeUtils.isSourcePart(node.getDocType())) {
            // decoded part already loaded
            return node.getDecodedPart();
        }

        // decoded part must be computed
        File file = new File(objectPath);
        if (DocTypeUtils.isDocument(node.getDocType())) {
            try {
                // load root document file
                return  new ContentConverter().protect(FileUtils.readFileToByteArray(file));
            } catch (IOException e) {
                LOGGER.error("Unable to read file : ",e);
                throw new UnexpectedAnalysisException("Unable to read File.",e);
            }
        }

        AnalysisPart parent = node.getParentPart();
        if (!EncodedType.NOT_ENCODED.equals(node.getEncodedType())) {
            if (EncodedType.B64_ENCODED.equals(node.getEncodedType())) {
                if (DocTypeUtils.isEmbeddedB64(node.getDocType())) {
                    // Embedded B64 in XML : find decoded content in parent node.
                    return getDecodedPartSource(objectPath, parent);
                } else {
                    // B64 Document
                    byte[] decodedByteContent = getDecodedPartSource(objectPath, parent);
                    if (DocTypeUtils.isBase64(parent.getDocType())) {
                        // BASE64 fragment must be extracted and decoded
                        if (parent.getDecodedPart()==null) {
                            decodedByteContent = getPartContent(decodedByteContent, parent);
                        }
                        decodedByteContent = base64Converter.base64Decoding(
                                decodedByteContent
                        );
                    }
                    if (node.getDecodedPart()==null&&DocTypeUtils.isBase64(parent.getDocType())) {
                        node.setDecodedPart(decodedByteContent);
                    }
                    return decodedByteContent;
                }
            } else if (EncodedType.ZIP_ENCODED.equals(node.getEncodedType())) {
                if (DocTypeUtils.isFile(node.getDocType()) || DocTypeUtils.isFolder(node.getDocType())) {
                    // ZipEntry must be loaded.
                    String expectedEntryName = "";
                    if (DocTypeUtils.isFile(node.getDocType())) {
                        expectedEntryName = node.getDocType().replace(MimeTypeDetector.FILE_PREFIX, "");
                    } else if (DocTypeUtils.isFolder(node.getDocType())) {
                        expectedEntryName = node.getDocType().replace(MimeTypeDetector.FOLDER_PREFIX, "");
                    }

                    try {
                        // return unzipped ZipEntry
                        return new ZipReader().readZipEntry(file,expectedEntryName);
                    } catch (IOException e) {
                        LOGGER.error("Unable to read zip file : ",e);
                        throw new UnexpectedAnalysisException("Unable to read zip File.",e);
                    }

                } else {
                    // Zip Encoded part : find corresponding ZipEntry
                    return getDecodedPartSource(objectPath, parent);
                }
            }

        } else if (isZipPart(node)) {

            try {
                return FileUtils.readFileToByteArray(file);
            } catch (IOException e) {
                LOGGER.error("Unable to read file : ",e);
                throw new UnexpectedAnalysisException("Unable to read File",e);
            }
        } else {
            if(parent.getStartOffset() == parent.getEndOffset()){
                try {
                    return FileUtils.readFileToByteArray(new File(objectPath));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return getDecodedPartSource(objectPath, parent);
        }
        return "Error retrieving string content".getBytes(StandardCharsets.UTF_8);
    }


    public byte[] getOriginalPartContent(final String objectPath, final AnalysisPart node) throws UnexpectedAnalysisException {
        try {
            byte[] source = getDecodedPartSource(objectPath, node);
            if (node.getDecodedPart() == null) {
                node.setDecodedPart(getPartContent(source, node));
            }
            return node.getDecodedPart();
        } finally {
            if (node.getDecodedPart()!=null && node.getDecodedPart().length> TO0_BIG_TO_KEEP_IN_MEMORY) {
                // Too big to keep in memory
                node.setDecodedPart(null);
            }
        }
    }
    private byte[] getPartContent(final byte[] decodedPartContent, final AnalysisPart node) {
        return new ContentConverter().toByteArray(decodedPartContent,StandardCharsets.UTF_8,node.getStartOffset(),node.getEndOffset());
    }


    private boolean isZipPart(AnalysisPart node) {
        return DocTypeUtils.isZip(node.getDocType())
                || messageContentAnalyzer.isZipDocType(node.getDocType())
                || (DocTypeUtils.isDocument(node.getDocType())
                    && !node.getChildPart().isEmpty()
                    && messageContentAnalyzer.isZipDocType(node.getChildPart().get(0).getDocType()));
    }
}
