package net.ihe.gazelle.mca.contentanalyzer.business.model;

public enum EncodedType {

    NOT_ENCODED,
    ZIP_ENCODED,
    B64_ENCODED;
}
