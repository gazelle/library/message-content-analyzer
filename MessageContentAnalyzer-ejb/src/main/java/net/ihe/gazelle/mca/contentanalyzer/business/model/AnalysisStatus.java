package net.ihe.gazelle.mca.contentanalyzer.business.model;

public enum AnalysisStatus {

    PENDING,
    ANALYSIS_IN_PROGRESS,
    VALIDATION_IN_PROGRESS,
    DONE;

}
