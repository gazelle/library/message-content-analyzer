package net.ihe.gazelle.mca.contentanalyzer.application.analyzers;

import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.McaConfigDao;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ContentAnalysisConfigInterface;
import org.apache.tika.Tika;
import org.apache.tika.detect.MagicDetector;
import org.apache.tika.mime.MediaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContentAnalysisTypeDetector implements Analyzer {
    private static final long serialVersionUID = 3793195042266726846L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ContentAnalysisTypeDetector.class);
    private static final String PART_LOG_TYPE = "Content Analysis";

    private final Analyzer messageSplitter;

    private final McaConfigDao mcaConfigDAO;

    public ContentAnalysisTypeDetector(Analyzer messageSplitter, McaConfigDao mcaConfigDAO) {
        this.messageSplitter = messageSplitter;
        this.mcaConfigDAO = mcaConfigDAO;

    }

    @Override
    public AnalysisPart analyze(final MessageContentAnalyzer mca, final AnalysisPart parent)
            throws UnexpectedAnalysisException {
        return detectType(mca, parent, mcaConfigDAO);
    }

    AnalysisPart detectType(final MessageContentAnalyzer mca,
                              final AnalysisPart parent, McaConfigDao mcaConfigDAO) throws UnexpectedAnalysisException {

        for (ContentAnalysisConfigInterface catdc : mcaConfigDAO.getAllContentAnalysisConfig()) {
            parent.addLog(PART_LOG_TYPE,"Test if document is of type : " + catdc.getDocType());
            LOGGER.info("Is the Document of {} type ?", catdc.getDocType());
            if (catdc.getUnwantedContent()!= null) {
                parent.addLog(PART_LOG_TYPE,"Look for unwanted content : " + catdc.getUnwantedContent());
                if (!containsUnwantedContent(catdc.getUnwantedContent(), mca.getMessageByteContent())) {
                    LOGGER.info("Unwanted content wasn't found in file.");
                    parent.addLog(PART_LOG_TYPE,"Unwanted content wasn't found in file");
                    AnalysisPart found = analyzeContent(mca, catdc, parent, mcaConfigDAO);
                    if (found!=null) {
                        return found;
                    }
                } else {
                    parent.addLog(PART_LOG_TYPE,"Unwanted content was found in file, try to detect next known type\n");
                }
            } else {
                AnalysisPart found = analyzeContent(mca, catdc, parent, mcaConfigDAO);
                if (found!=null) {
                    return found;
                }
            }
        }
        parent.addLog(PART_LOG_TYPE,"No known type detected");
        return null;
    }

    private AnalysisPart analyzeContent(final MessageContentAnalyzer mca, ContentAnalysisConfigInterface catdc,
                                   final AnalysisPart parent, McaConfigDao mcaConfigDAO)
            throws UnexpectedAnalysisException {

        boolean detected = false;
        if (catdc.getBytePattern() != null) {
            parent.addLog(PART_LOG_TYPE,"Try to detect byte pattern : " + Arrays.toString(catdc.getBytePattern()));
            detected = detectBytePattern(mca.getMessageByteContent(), catdc.getDocType(), catdc.getBytePattern(), parent);
            if (!detected) {
                return null;
            }
        }

        if (catdc.getStartsWith() != null && !catdc.getStartsWith().isEmpty()) {
            parent.addLog(PART_LOG_TYPE,"Try to detect start element : " + catdc.getStartsWith());
            detected = detectStartElement(catdc.getStartsWith(), mca.getMessageByteContent(), parent);
            if (!detected) {
                return null;
            }
        }

        if (catdc.getStringPattern() != null) {
            parent.addLog(PART_LOG_TYPE,"Try to detect string pattern : " + catdc.getStringPattern());
            detected = detectStringPattern(catdc.getStringPattern(), mca.getMessageByteContent(), parent);
        }

        if (!detected) {
            return null;
        }

        LOGGER.info("Detected {} type ", catdc.getDocType());
        parent.addLog(PART_LOG_TYPE,"All conditions verified, part detected as type : " + catdc.getDocType());
        final AnalysisPart child = new AnalysisPart(parent);
        mca.setChildOffsetsAndDecodedPart(child, parent);
        child.setDocType(catdc.getDocType());
        child.setValidationType(catdc.getValidationType());
        parent.getChildPart().add(child);
        messageSplitter.analyze(mca, child);
        return child;
    }

    private boolean detectBytePattern(byte[] messageBytesContent, final String docType, final byte[] bytePattern,
                                      final AnalysisPart parent) throws UnexpectedAnalysisException {

        LOGGER.info("{} byte pattern Detection", docType);

        final MediaType type = new MediaType("application", docType);
        final MagicDetector detector = new MagicDetector(type, bytePattern);
        final Tika t = new Tika(detector);
        final String result;
        try {
            result = t.detect(new ByteArrayInputStream(messageBytesContent));
        } catch (IOException e) {
            LOGGER.error("Error detecting byte pattern ", e);
            throw new UnexpectedAnalysisException("Error detecting byte pattern.", e);
        }
        if (("application/"+docType.toLowerCase(Locale.getDefault())).equals(result)) {
            parent.addLog(PART_LOG_TYPE,"Byte pattern detected, continue type detection");
            return true;
        }
        parent.addLog(PART_LOG_TYPE,"Byte pattern not detected, try to detect next known type \n");
        return false;
    }


    private boolean detectStartElement(final List<String> startElements, byte[] messageBytesContent,
                                       final AnalysisPart parent) {

        LOGGER.info("Start Element detection");
        final String fileContent = new String(messageBytesContent, StandardCharsets.UTF_8);

        for (String startElement : startElements) {
            if (fileContent.startsWith(startElement)) {
                parent.addLog(PART_LOG_TYPE,"Start element \""+ startElement +"\" detected, continue type detection");
                return true;
            }
        }
        parent.addLog(PART_LOG_TYPE,"Start element not detected, try to detect next known type \n");
        return false;
    }

    private boolean detectStringPattern(final String stringPattern, byte[] messageBytesContent,
                                        final AnalysisPart parent) {

        LOGGER.info("String pattern detection with pattern : {}", stringPattern);

        final String fileContent = new String(messageBytesContent, StandardCharsets.UTF_8);
        final Pattern regex = Pattern.compile(stringPattern, Pattern.DOTALL);
        final Matcher regexMatcher = regex.matcher(fileContent);

        if (regexMatcher.find()) {
            LOGGER.debug("String pattern found in : {}", regexMatcher.group());
            parent.addLog(PART_LOG_TYPE,"String pattern found");
            return true;
        }
        parent.addLog(PART_LOG_TYPE,"String pattern not detected, try to detect next known type \n");
        return false;
    }

    private boolean containsUnwantedContent(final String unwantedContent, byte[] messageBytesContent) {

        LOGGER.info("Unwanted Content detection");
        final String fileContent = new String(messageBytesContent, StandardCharsets.UTF_8);
        return fileContent.contains(unwantedContent);
    }
}
