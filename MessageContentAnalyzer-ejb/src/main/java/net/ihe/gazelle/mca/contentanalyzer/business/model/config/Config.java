package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "docType",
        "validationType",
})
@XmlRootElement(name = "xmlTagConfig")
@MappedSuperclass
public class Config {

    @XmlElement(required = true)
    @Column(name = "doc_type")
    private String docType;

    @XmlElement
    @Column(name = "validation_type")
    @Enumerated(EnumType.STRING)
    private ValidationType validationType;

    public String getDocType() {
        return this.docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public ValidationType getValidationType() {
        return this.validationType;
    }

    public void setValidationType(ValidationType validationType) {
        this.validationType = validationType;
    }

}
