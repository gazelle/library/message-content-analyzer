package net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.dao;

import net.ihe.gazelle.evsclient.domain.validation.ValidationRef;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.AnalysisPartDao;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPartQuery;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class AnalysisPartDaoImpl implements AnalysisPartDao {
    private static final long serialVersionUID = -7926697072559860210L;

    private transient EntityManagerFactory entityManagerFactory;

    public AnalysisPartDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public AnalysisPart getByOid(String oid) {
        AnalysisPartQuery analysisPartQuery = new AnalysisPartQuery(entityManagerFactory.createEntityManager());
        analysisPartQuery.oid().eq(oid);
        return analysisPartQuery.getUniqueResult();
    }

    public AnalysisPart save(AnalysisPart analysisPart) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        AnalysisPart merged = entityManager.merge(analysisPart);
        entityManager.flush();
        return merged;
    }

    public AnalysisPart getByValidation(ValidationRef validation) {
        AnalysisPartQuery analysisPartQuery = new AnalysisPartQuery(entityManagerFactory.createEntityManager());
        analysisPartQuery.validation().oid().eq(validation.getOid());
        return analysisPartQuery.getUniqueResult();
    }
}
