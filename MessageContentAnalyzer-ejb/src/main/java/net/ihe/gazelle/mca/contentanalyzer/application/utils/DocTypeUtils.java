package net.ihe.gazelle.mca.contentanalyzer.application.utils;

import net.ihe.gazelle.mca.contentanalyzer.application.analyzers.MimeTypeDetector;
import net.ihe.gazelle.mca.contentanalyzer.business.model.DocType;
import org.apache.commons.lang.StringUtils;

public class DocTypeUtils {

    public static boolean isEmbeddedB64(String docType) {
        return StringUtils.isNotEmpty(docType) && docType.startsWith("B64"); // TODO add property to detect embedded B64
    }

    public static boolean isFolder(String docType) {
        return StringUtils.isNotEmpty(docType) && docType.contains(MimeTypeDetector.FOLDER_PREFIX);
    }

    public static boolean isFile(String docType) {
        return StringUtils.isNotEmpty(docType) && docType.contains(MimeTypeDetector.FILE_PREFIX);
    }

    public static boolean isZip(String docType) {
        return DocType.ZIP.getValue().equals(docType);
    }

    public static boolean isDocument(String docType) {
        return DocType.DOCUMENT.getValue().equals(docType);
    }

    public static boolean isBase64(String docType) {
        return DocType.BASE64.getValue().equals(docType);
    }

    public static boolean isDicom(String docType) {
        return DocType.DICOM.getValue().equals(docType);
    }

    public static boolean isXml(String docType) {
        return DocType.XML.getValue().equals(docType);
    }

    public static boolean isMtom(String docType) {
        return StringUtils.isNotEmpty(docType) && docType.startsWith(DocType.MTOM.getValue());    }

    public static boolean isSourcePart(String docType) {
        return isFile(docType)||isFolder(docType)||isDocument(docType)||isBase64(docType);
    }
}
