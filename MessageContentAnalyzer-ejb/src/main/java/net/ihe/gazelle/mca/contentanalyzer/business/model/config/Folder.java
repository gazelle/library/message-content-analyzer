package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FileInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FolderInterface;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "mca_folder_config", schema = "public")
@SequenceGenerator(name = "mca_folder_config_sequence", sequenceName = "mca_folder_config_id_seq", allocationSize = 1)
public class Folder implements FolderInterface {

    @Id
    @NotNull
    @GeneratedValue(generator = "mca_folder_config_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "folder_name")
    private String folderName;

    @ManyToMany
    @JoinTable(name = "mca_folder_file_children", joinColumns = @JoinColumn(name = "folder_id"), inverseJoinColumns = @JoinColumn(name = "file_child_id"))
    private List<File> fileChildren;

    @ManyToMany
    @JoinTable(name = "mca_folder_folder_children", joinColumns = @JoinColumn(name = "folder_id"), inverseJoinColumns = @JoinColumn(name = "folder_child_id"))
    private List<Folder> folderChildren;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    @Override
    public List<FileInterface> getFileChildren() {
        return new ArrayList<FileInterface>(fileChildren);
    }

    public void setFileChildren(List<File> fileChildren) {
        if (fileChildren != null) {
            this.fileChildren = new ArrayList<>(fileChildren);
        } else {
            this.fileChildren = new ArrayList<>();
        }
    }

    @Override
    public List<FolderInterface> getFolderChildren() {
        return new ArrayList<FolderInterface>(folderChildren);
    }

    public void setFolderChildren(List<Folder> folderChildren) {
        if (folderChildren != null) {
            this.folderChildren = new ArrayList<>(folderChildren);
        } else {
            this.folderChildren = new ArrayList<>();
        }
    }
}
