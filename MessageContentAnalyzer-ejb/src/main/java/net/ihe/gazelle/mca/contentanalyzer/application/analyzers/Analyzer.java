package net.ihe.gazelle.mca.contentanalyzer.application.analyzers;

import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;

import java.io.Serializable;

public interface Analyzer extends Serializable {

    AnalysisPart analyze(final MessageContentAnalyzer mca, final AnalysisPart parent)
            throws UnexpectedAnalysisException;
}
