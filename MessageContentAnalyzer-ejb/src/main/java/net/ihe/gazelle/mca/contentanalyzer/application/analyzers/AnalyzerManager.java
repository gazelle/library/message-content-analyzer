package net.ihe.gazelle.mca.contentanalyzer.application.analyzers;

import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.domain.validation.ValidationRef;
import net.ihe.gazelle.evsclient.interlay.gui.document.ContentConverter;
import net.ihe.gazelle.mca.contentanalyzer.application.McaApi;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisManager;
import net.ihe.gazelle.mca.contentanalyzer.application.analysis.AnalysisPartManager;
import net.ihe.gazelle.mca.contentanalyzer.application.config.McaConfigManager;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.DecodedPartNotSavedException;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.DocTypeUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisStatus;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.MimeTypeConfigEnum;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ZipStructureInterface;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;

public class AnalyzerManager implements Serializable {
    private static final long serialVersionUID = -7175516819679714402L;

    private static final Logger LOGGER = LoggerFactory.getLogger(AnalyzerManager.class);
    public static final String EXECUTE_FROM_STR_DESCRIPTION = "EditedMessagePart";


    private final McaApi mcaApi;

    private final AnalysisManager analysisManager;

    private final AnalysisPartManager analysisPartManager;

    private final McaConfigManager mcaConfigManager;

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////       Constructor       //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public AnalyzerManager(McaApi mcaApi, AnalysisManager analysisManager,
                           AnalysisPartManager analysisPartManager, McaConfigManager mcaConfigManager) {
        this.mcaApi = mcaApi;
        this.analysisManager = analysisManager;
        this.analysisPartManager = analysisPartManager;
        this.mcaConfigManager = mcaConfigManager;
    }

    ///////////////////////////////////////////////////////////////////////////////
    ////////////////////      Public Methods     //////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////

    public Analysis execute(byte[] messageContent, String messageName, EVSCallerMetadata callerMetadata, OwnerMetadata ownerMetadata) throws UnexpectedProcessingException, NotLoadedException, ProcessingNotSavedException, DecodedPartNotSavedException {
        return execute(messageContent,messageName,callerMetadata,ownerMetadata,null);
    }
    public Analysis execute(byte[] messageContent, String messageName, EVSCallerMetadata callerMetadata, OwnerMetadata ownerMetadata, ValidationRef validation) throws UnexpectedProcessingException, NotLoadedException, ProcessingNotSavedException, DecodedPartNotSavedException {
        if (messageContent == null || messageContent.length == 0) {
            LOGGER.error("Bytes content received by the API is null or empty and this should not be the case.");
            throw new UnexpectedAnalysisException("Bytes content is null or empty and this should not be the case.");
        }
        // create analysis object
        HandledObject object = new HandledObject(new ContentConverter().protect(messageContent), messageName);
        Analysis analysis = analysisManager.create(object, callerMetadata, ownerMetadata);
        // Start analyze
        analysis = mcaApi.analyze(analysis);
        if (validation!=null) {
            analysis.getRootAnalysisPart().setValidation(validation);
        }
        analysis.setAnalysisStatus(AnalysisStatus.DONE);

        analysis = analysisManager.persistAnalysisData(analysis);
        return analysis;
    }

    @Deprecated
    public Analysis executeFromPath(String filePath, String fileName, EVSCallerMetadata callerMetadata, OwnerMetadata ownerMetadata)
            throws UnexpectedProcessingException, DecodedPartNotSavedException, ProcessingNotSavedException, NotLoadedException {

        final File file = new File(filePath);
        byte[] messageByteContent;

        try {
            messageByteContent = FileUtils.readFileToByteArray(file);
        } catch(IOException e) {
            LOGGER.error("Error reading byte content of message : ", e);
            throw new UnexpectedAnalysisException("Error reading content of the file to analyze.", e);
        }
        return execute(messageByteContent, fileName, callerMetadata, ownerMetadata);
    }

    public Analysis executeWithString(String fileContent, String filename, EVSCallerMetadata callerMetadata, OwnerMetadata ownerMetadata) throws UnexpectedProcessingException, DecodedPartNotSavedException, ProcessingNotSavedException, NotLoadedException {

        byte[] messageContent = fileContent.getBytes(StandardCharsets.UTF_8);
        return execute(messageContent, filename, callerMetadata, ownerMetadata);
    }

    public String getEncodedFilePath(final String objectPath) throws UnexpectedAnalysisException{

        String encodedObjectPath;
        try {
            encodedObjectPath = DatatypeConverter.printBase64Binary(objectPath.getBytes(StandardCharsets.UTF_8));
            LOGGER.info("Encoded object path : {}", encodedObjectPath);
        } catch (RuntimeException e) {
            LOGGER.error(e.getMessage());
            throw new UnexpectedAnalysisException("Error computing encoded object path to send to the validator.", e);
        }
        return encodedObjectPath;
    }

    public boolean isZipNode(AnalysisPart node) {
        if (node != null) {
            ZipStructureInterface zipStructure = mcaConfigManager.getZipStructureWithDocType(node.getDocType());
            return (zipStructure != null || (DocTypeUtils.isZip(node.getDocType())));
        }
        return false;
    }
}
