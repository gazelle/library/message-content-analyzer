package net.ihe.gazelle.mca.contentanalyzer.application.analyzers;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.interlay.gui.document.ZipLister;
import net.ihe.gazelle.evsclient.interlay.gui.document.ZipReader;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.McaConfigDao;
import net.ihe.gazelle.mca.contentanalyzer.application.utils.DocTypeUtils;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.EncodedType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FileInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FolderInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.MimeTypeConfigInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ZipStructureInterface;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class MimeTypeDetector implements Analyzer {
    private static final long serialVersionUID = -7612465720778819516L;

    public static final Logger LOGGER = LoggerFactory.getLogger(MimeTypeDetector.class);
    private static final String MIME_TYPE_DETECTION = "Mime Type Detection";
    private static final String ZIP_STRUCTURE_DETECTION = "ZIP Structure Detection";
    public static final String FILE_PREFIX = "File : ";
    public static final String FOLDER_PREFIX = "Folder : ";

    private final McaConfigDao mcaConfigDao;
    private final ApplicationPreferenceManager applicationPreferenceManager;
    ///////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////Mime type detection functions///////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////


    public MimeTypeDetector(ApplicationPreferenceManager applicationPreferenceManager, McaConfigDao mcaConfigDao) {
        this.applicationPreferenceManager = applicationPreferenceManager;
        this.mcaConfigDao = mcaConfigDao;
    }

    @Override
    public AnalysisPart analyze(final MessageContentAnalyzer mca, final AnalysisPart parent)
            throws UnexpectedAnalysisException {
        AnalysisPart child = new AnalysisPart(parent);
        boolean mimeTypeIsDetected = this.detectMimeType(mca.getMessageByteContent(), child, parent);

        if (mimeTypeIsDetected) {
            mimeTypeAnalyze(mca, child, parent);
            return child;
        }
        return null;
    }

    boolean detectMimeType(byte[] messageBytesContent, AnalysisPart child, AnalysisPart parent)
            throws UnexpectedAnalysisException{

        String detectedMimeType = getMimeTypeBetweenOffsets(messageBytesContent);
        List<MimeTypeConfigInterface> mimeTypeConfigList = mcaConfigDao.getAllMimeTypeConfig();

        for (MimeTypeConfigInterface mimeTypeConfig : mimeTypeConfigList) {
            if (detectedMimeType.equals(mimeTypeConfig.getMimeType())) {
                child.setDocType(mimeTypeConfig.getDocType());
                child.setValidationType(mimeTypeConfig.getValidationType());
                parent.addLog(MIME_TYPE_DETECTION,"Mime type detected for this part : " + detectedMimeType);
                return true;
            }
        }
        parent.addLog(MIME_TYPE_DETECTION,"No mime type detected for this part");
        return false;
    }

    private String getMimeTypeBetweenOffsets(byte[] messageBytesContent) throws UnexpectedAnalysisException{

        final Tika t = new Tika();
        final String value;

        try {
            value = t.detect(new ByteArrayInputStream(messageBytesContent));
        } catch (IOException e) {
            throw new UnexpectedAnalysisException("Error while trying to detect mime type on uploaded file.", e);
        }
        return value;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////Mime type special treatment function//////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////

    void mimeTypeAnalyze(final MessageContentAnalyzer mca, final AnalysisPart child,
                                final AnalysisPart parent) throws UnexpectedAnalysisException{
        if (DocTypeUtils.isZip(child.getDocType())) {
            parent.addLog(MIME_TYPE_DETECTION,"Analyze sub-part as ZIP");
            zipMimeTypeAnalyze(mca, child, parent);
        } else {
            parent.addLog(MIME_TYPE_DETECTION,"Analyze sub-part as " + child.getDocType());
            mca.setChildOffsetsAndDecodedPart(child, parent);
            child.addLog(MIME_TYPE_DETECTION,"End of analysis : No further analysis for " + child.getDocType() + " sub-part");
            parent.getChildPart().add(child);
        }
    }

    private void zipMimeTypeAnalyze(final MessageContentAnalyzer mca, final AnalysisPart child,
                                    final AnalysisPart parent)
            throws UnexpectedAnalysisException{

        LOGGER.info("Child is a ZIP file");
        LOGGER.info("Extract files from zip");

        child.addLog(MIME_TYPE_DETECTION,"Extraction of files in ZIP archive");
        mca.setChildOffsetsAndDecodedPart(child, parent);
        parent.getChildPart().add(child);

        try {
            analyzeByteZipContent(mca, child);
        } catch (IOException | NullPointerException e) {
            LOGGER.error("Error analyzing zip file");
            throw new UnexpectedAnalysisException("Error analyzing ZIP File", e);
        }
        try {
            analyzeZipArchitecture(mca, child, mcaConfigDao.getZipStructures());
        } catch (Exception e) {
            LOGGER.error("Error analyzing zip file structure " + e.getMessage());
            throw new UnexpectedAnalysisException("Error analyzing ZIP File structure", e);
        }
    }

    // Analyze all files and folders found in the archive. No parent is yet attributed (see recreateTreeLinks function)
    private void analyzeByteZipContent(final MessageContentAnalyzer mca, AnalysisPart parent)
            throws IOException, NullPointerException, UnexpectedAnalysisException {

        List<AnalysisPart> listOfFolderAop = new ArrayList<>();
        List<AnalysisPart> listOfFileAop = new ArrayList<>();
        ZipReader reader = new ZipReader();
        byte[] zipContent = mca.getMessageByteContent();
        try (ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(zipContent))) {
            SortedSet<ZipEntry> entries = ZipLister.list(zis);
            // iterates over entries in the zip file
            for (ZipEntry entry : entries) {
                String entryName = entry.getName();
                byte[] unzippedEntryByte = reader.readZipEntry(zipContent,entryName);
                AnalysisPart aop = new AnalysisPart();
                aop.setEncodedType(EncodedType.ZIP_ENCODED);
                aop.setStartOffset(0);
                aop.setEndOffset(unzippedEntryByte.length);
                if (!entry.isDirectory()) {
                    // If the entry is a file launch analysis on it.
                    aop.setDocType(FILE_PREFIX + entryName);
                    aop.setDecodedPart(unzippedEntryByte);
                    mca.analyzeMessageContent(unzippedEntryByte, aop);
                    listOfFileAop.add(aop);
                } else {
                    // if the entry is a directory, make a node
                    aop.setDocType(FOLDER_PREFIX + entryName);
                    listOfFolderAop.add(aop);
                }
                zis.closeEntry();
            }

        } catch (IOException e) {
            LOGGER.error("Error access list of files in folder !");
            parent.addLog(MIME_TYPE_DETECTION, "End of analysis : Error reading zip file !");
        }
        recreateTreeLinks(parent, listOfFolderAop, listOfFileAop);
    }

    // Recreate parent/children links to allow results to keep the same tree structure as the original zip file.
    private void recreateTreeLinks(AnalysisPart parent, List<AnalysisPart> listOfFolderAop, List<AnalysisPart> listOfFileAop) {

        // Browse list of folders and give them appropriate parent
        for (AnalysisPart childFolder : listOfFolderAop) {
            setParentFolder(parent, listOfFolderAop, childFolder);
        }
        // Browse list of files and give them appropriate parent
        for (AnalysisPart childFile : listOfFileAop) {
            setParentFolder(parent, listOfFolderAop, childFile);
        }
    }

    //Browse list of folders and give child the appropriate parent
    private void setParentFolder(AnalysisPart parent, List<AnalysisPart> listOfFolderAop, AnalysisPart child) {

        String childName;
        String parentName;
        childName = child.getDocType().replace(MimeTypeDetector.FOLDER_PREFIX, "");
        childName = childName.replace(MimeTypeDetector.FILE_PREFIX, "");

        for (AnalysisPart parentFolder: listOfFolderAop) {
            parentName = parentFolder.getDocType().replace(MimeTypeDetector.FOLDER_PREFIX, "");
            if (!childName.equals(parentName) && childName.contains(parentName)
                    && (child.getParentPart() == null || !child.getParentPart().getDocType().contains(parentName))) {
                child.setParentPart(parentFolder);
                parentFolder.addLog(MIME_TYPE_DETECTION, "Add a sub-part for " + childName);
                parentFolder.getChildPart().add(child);
            }
        }
        if (child.getParentPart() == null) {
            parent.addLog(MIME_TYPE_DETECTION,"Add sub-part for " + childName);
            child.setParentPart(parent);
            parent.getChildPart().add(child);
        }

    }

    private void analyzeZipArchitecture(final MessageContentAnalyzer mca, AnalysisPart child, List<ZipStructureInterface> zipStructures) {

        for (ZipStructureInterface zipStructure : zipStructures) {
            if (zipPartMatchZipStructure(new String(mca.getMessageName(), StandardCharsets.UTF_8), child, zipStructure)) {
                child.addLog(ZIP_STRUCTURE_DETECTION,"ZIP file was detected as : " + zipStructure.getDocType());
                child.setxValidatorOid(zipStructure.getXValidatorOid());
                child.setDocType(zipStructure.getDocType());
                child.setValidationType(zipStructure.getValidationType());
            } else {
                child.addLog(ZIP_STRUCTURE_DETECTION,"ZIP file is not of type : " + zipStructure.getDocType());
            }
        }
    }

    private boolean zipPartMatchZipStructure(String zipName, AnalysisPart zipPart, ZipStructureInterface zipStructure) {
        zipPart.addLog(ZIP_STRUCTURE_DETECTION,"Analyze if archive is of type : " + zipStructure.getDocType());
        if (!zipName.equals(zipStructure.getArchiveName())) {
            zipPart.addLog(ZIP_STRUCTURE_DETECTION,"Uploaded ZIP name (" + zipName +") does not match expected zip name : " + zipStructure.getArchiveName());
            return false;
        } else {
            zipPart.addLog(ZIP_STRUCTURE_DETECTION,"Uploaded ZIP name (" + zipName +") matches expected zip name : " + zipStructure.getArchiveName());
            return analyzeFolderContent(zipPart, zipPart, zipStructure.getFolderChildren(), zipStructure.getFileChildren());
        }
    }

    private boolean analyzeFolderContent(AnalysisPart zip, AnalysisPart zipPart, List<FolderInterface> folders, List<FileInterface> files) {

        List<AnalysisPart> foldersInPart = getSubPartWithPrefix(zipPart, FOLDER_PREFIX);
        List<AnalysisPart> filesInPart = getSubPartWithPrefix(zipPart, FILE_PREFIX);

        List<FileInterface> fileWithRegex = new ArrayList<>();
        List<FileInterface> fileWithoutRegex= new ArrayList<>();
        for (FileInterface file : files) {
            if (file.isRegex()) {
                fileWithRegex.add(file);
            } else {
                fileWithoutRegex.add(file);
            }
        }

        if (!matchFiles(zip, fileWithoutRegex, fileWithRegex, filesInPart)) {
            return false;
        }
        return matchFolders(zip, folders, foldersInPart);
    }

    private boolean matchFiles(AnalysisPart zip, List<FileInterface> filesToMatchWithoutRegex, List<FileInterface> filesToMatchWithRegex,
                               List<AnalysisPart> filesInPart) {
        int matchedFile = 0;
        List<AnalysisPart> matchedFiles = new ArrayList<>();

        for (FileInterface file : filesToMatchWithoutRegex) {
            zip.addLog(ZIP_STRUCTURE_DETECTION,"Try to find file with name : " + file.getFileName());
            boolean match = false;
            for (AnalysisPart fileInPart : filesInPart) {
                if (!matchedFiles.contains(fileInPart)) {
                    String fileName = fileInPart.getDocType().replace(FILE_PREFIX, "");
                    if (fileName.contains("/")) {
                        fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
                    }
                    if (fileName.equals(file.getFileName())) {
                        zip.addLog(ZIP_STRUCTURE_DETECTION, "File found in subpart : " + fileInPart.getDocType());
                        if (file.getXValInputType() != null) {
                            fileInPart.setxValInputType(file.getXValInputType());
                        }
                        matchedFiles.add(fileInPart);
                        match = true;
                        matchedFile++;
                        break;
                    }
                }
            }
            if (!match) {
                zip.addLog(ZIP_STRUCTURE_DETECTION,"Did not find file with name : " + file.getFileName());
                return false;
            }
        }


        for (FileInterface file : filesToMatchWithRegex) {
            zip.addLog(ZIP_STRUCTURE_DETECTION,"Try to find file with a name matching the regular expression : " + file.getFileName());
            boolean match = false;
            for (AnalysisPart fileInPart : filesInPart) {
                if (!matchedFiles.contains(fileInPart)) {
                    String fileName = fileInPart.getDocType().replace(FILE_PREFIX, "");
                    if (fileName.contains("/")) {
                        fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
                    }
                    if (fileName.matches(file.getFileName())) {
                        zip.addLog(ZIP_STRUCTURE_DETECTION, "File found in subpart : " + fileInPart.getDocType());
                        if (file.getXValInputType() != null) {
                            fileInPart.setxValInputType(file.getXValInputType());
                        }
                        matchedFiles.add(fileInPart);
                        match = true;
                        matchedFile++;
                        break;
                    }
                }
            }
            if (!match) {
                zip.addLog(ZIP_STRUCTURE_DETECTION,"Did not find file with name : " + file.getFileName());
                return false;
            }
        }

        if (matchedFile < filesInPart.size()) {
            zip.addLog(ZIP_STRUCTURE_DETECTION,"The archive contains files that are not registered in the configuration and will therefore not match this type.");
            return false;
        }
        return true;
    }

    private boolean matchFolders(AnalysisPart zip, List<FolderInterface> foldersToMatch, List<AnalysisPart> foldersInPart) {
        int matchedFolder = 0;
        List<AnalysisPart> matchedFolders = new ArrayList<>();

        for (FolderInterface folder : foldersToMatch) {
            zip.addLog(ZIP_STRUCTURE_DETECTION,"Try to find folder with name : " + folder.getFolderName());
            boolean match = false;
            for (AnalysisPart folderInPart : foldersInPart) {
                if (!matchedFolders.contains(folderInPart)) {
                    String folderName = folderInPart.getDocType().replace(FOLDER_PREFIX, "");
                    if (folderName.endsWith("/")) {
                        folderName = folderName.substring(0, folderName.length() - 1);
                    }
                    if (folderName.contains("/")) {
                        folderName = folderName.substring(folderName.lastIndexOf("/") + 1);
                    }
                    if (folderName.equals(folder.getFolderName())) {
                        zip.addLog(ZIP_STRUCTURE_DETECTION, "Folder found in subpart : " + folderInPart.getDocType() +
                                " . Analyze this folder content to see if it matches expected ZIP Structure");
                        match = analyzeFolderContent(zip, folderInPart, folder.getFolderChildren(), folder.getFileChildren());
                        if (match) {
                            matchedFolders.add(folderInPart);
                            matchedFolder++;
                            break;
                        }
                    }
                }
            }
            if (!match) {
                zip.addLog(ZIP_STRUCTURE_DETECTION,"Did not find folder with name : " + folder.getFolderName() + " or the folder had invalid content.");
                return false;
            }
        }
        if (matchedFolder < foldersInPart.size()) {
            zip.addLog(ZIP_STRUCTURE_DETECTION,"The archive contains folders that are not registered in the configuration and will therefore not match this type.");
            return false;
        }
        return true;
    }

    private List<AnalysisPart> getSubPartWithPrefix(AnalysisPart part, String prefix) {
        ArrayList<AnalysisPart> prefixInPart = new ArrayList<>();

        for (AnalysisPart analysisPart : part.getChildPart()) {
            if (analysisPart.getDocType().contains(prefix)) {
                String relativeName = analysisPart.getDocType().replace(prefix, "");
                relativeName = relativeName.replaceFirst(part.getDocType().replace(FOLDER_PREFIX, ""), "");
                if (relativeName.endsWith("/")) {
                    relativeName = relativeName.substring(0, relativeName.length()-1);
                }
                if (!relativeName.contains("/")) {
                    prefixInPart.add(analysisPart);
                }
            }
        }
        return prefixInPart;
    }

    public ZipStructureInterface getZipStructureWithDocType(String docType) {
        return mcaConfigDao.getZipStructureWithDocType(docType);
    }
}
