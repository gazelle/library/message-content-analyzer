package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FileInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.FolderInterface;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.ZipStructureInterface;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "mca_zip_config", schema = "public")
@SequenceGenerator(name = "mca_zip_config_sequence", sequenceName = "mca_zip_config_id_seq", allocationSize = 1)
public class ZipStructure extends Config implements ZipStructureInterface {

    @Id
    @NotNull
    @GeneratedValue(generator = "mca_zip_config_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "archive_name")
    private String archiveName;

    @Column(name = "x_validator_oid")
    private String xValidatorOid;

    @ManyToMany
    @JoinTable(name = "mca_zip_file_children", joinColumns = @JoinColumn(name = "zip_structure_id"), inverseJoinColumns = @JoinColumn(name = "folder_id"))
    private List<File> fileChildren;

    @ManyToMany
    @JoinTable(name = "mca_zip_folder_children", joinColumns = @JoinColumn(name = "zip_structure_id"), inverseJoinColumns = @JoinColumn(name = "folder_id"))
    private List<Folder> folderChildren;

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getArchiveName() {
        return archiveName;
    }

    public void setArchiveName(String archiveName) {
        this.archiveName = archiveName;
    }

    @Override
    public List<FileInterface> getFileChildren() {
        return new ArrayList<FileInterface>(fileChildren);
    }

    public void setFileChildren(List<File> fileChildren) {
        if (fileChildren != null) {
            this.fileChildren = new ArrayList<>(fileChildren);
        } else {
            this.fileChildren = new ArrayList<>();
        }
    }

    @Override
    public List<FolderInterface> getFolderChildren() {
        return new ArrayList<FolderInterface>(folderChildren);
    }

    public void setFolderChildren(List<Folder> folderChildren) {
        if (folderChildren != null) {
            this.folderChildren = new ArrayList<>(folderChildren);
        } else {
            this.folderChildren = new ArrayList<>();
        }
    }

    @Override
    public String getXValidatorOid() {
        return xValidatorOid;
    }

    public void setXValidatorOid(String xValidatorOid) {
        this.xValidatorOid = xValidatorOid;
    }
}
