package net.ihe.gazelle.mca.contentanalyzer.application.config;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.McaConfigDao;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.ConfigsFromDB;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.*;

import java.io.Serializable;
import java.util.List;

public class McaConfigManager implements Serializable {
    private static final long serialVersionUID = -3299219531246051968L;

    private final McaConfigDao mcaConfigDao;

    public McaConfigManager(McaConfigDao mcaConfigDao) {
        this.mcaConfigDao = mcaConfigDao;
    }

    public List<MimeTypeConfigInterface> getAllMimeTypeConfig() {
        return mcaConfigDao.getAllMimeTypeConfig();
    }

    public List<ContentAnalysisConfigInterface> getAllContentAnalysisConfig() {
        return mcaConfigDao.getAllContentAnalysisConfig();
    }

    public List<XmlTagConfigInterface> getAllTags() {
        return mcaConfigDao.getAllTags();
    }

    public ConfigsFromDB getConfigsFromDB() {
        return mcaConfigDao.getConfigsFromDB();
    }

    public List<MimeTypeConfigInterface> getMimeTypeConfigWithValidationType(ValidationType validationType) {
        return mcaConfigDao.getMimeTypeConfigWithValidationType(validationType);
    }

    public List<ContentAnalysisConfigInterface> getContentAnalysisTypeDetectionConfigWithValidationType(ValidationType validationType) {
        return mcaConfigDao.getContentAnalysisTypeDetectionConfigWithValidationType(validationType);
    }

    public List<XmlTagConfigInterface> getXmlTagsWithValidationType(ValidationType validationType) {
        return mcaConfigDao.getXmlTagsWithValidationType(validationType);
    }

    public List<ZipStructureInterface> getZipStructures() {
        return mcaConfigDao.getZipStructures();
    }

    public ZipStructureInterface getZipStructureWithDocType(String docType) {
        return mcaConfigDao.getZipStructureWithDocType(docType);
    }

    public int addNewImportedConfiguration(ConfigsFromDB configsToImport, ConfigsFromDB overwrittenConfigs) {
        return mcaConfigDao.addNewImportedConfiguration(configsToImport, overwrittenConfigs);
    }

    public int removeAllConfigsFromDB() {
        return mcaConfigDao.removeAllConfigsFromDB();
    }

    public int addAllImportedConfigurationsToDB(ConfigsFromDB configsToImport) {
        return mcaConfigDao.addAllImportedConfigurationsToDB(configsToImport);
    }

    public void remove(ConfigInterface selectedConfig) {
        mcaConfigDao.remove(selectedConfig);
    }

    public void merge(ConfigInterface selectedConfig) {
        mcaConfigDao.merge(selectedConfig);
    }


}
