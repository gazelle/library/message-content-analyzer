package net.ihe.gazelle.mca.contentanalyzer.application.analyzers.xml;

import java.util.ArrayList;
import java.util.List;

public class FileContentAndListOfModifications {

    protected String content;

    protected List<OffsetAndSizeOfText> offsetsAndSizesOfText;

    public FileContentAndListOfModifications() {
        offsetsAndSizesOfText = new ArrayList<>();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<OffsetAndSizeOfText> getOffsetsAndSizesOfText() {
        return offsetsAndSizesOfText;
    }

    public void setOffsetsAndSizesOfText(List<OffsetAndSizeOfText> offsetsAndSizesOfText) {
        this.offsetsAndSizesOfText = offsetsAndSizesOfText;
    }

    static class OffsetAndSizeOfText {
        Integer offset;
        Integer size;

        OffsetAndSizeOfText(int offset, int size) {
            this.offset = offset;
            this.size = size;
        }

        public Integer getOffset() {
            return offset;
        }

        public Integer getSize() {
            return size;
        }
    }
}
