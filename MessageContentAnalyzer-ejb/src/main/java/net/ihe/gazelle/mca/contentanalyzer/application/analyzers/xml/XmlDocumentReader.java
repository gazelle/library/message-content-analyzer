package net.ihe.gazelle.mca.contentanalyzer.application.analyzers.xml;

import net.ihe.gazelle.evsclient.interlay.factory.DocumentBuilderFactoryException;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import java.io.IOException;



public class XmlDocumentReader {

    public Document readXMLFile(InputSource is, DefaultHandler eleHandler) throws IOException, SAXException, ParserConfigurationException, DocumentBuilderFactoryException {

        Document doc = null;
        SAXParser parser = null;

        SAXParserFactory saxFactory = SAXParserFactory.newInstance();

        parser = saxFactory.newSAXParser();
        DocumentBuilder docBuilder = new XmlDocumentBuilderFactory()
                .setNamespaceAware(true)
                .setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true)
                .getBuilder();
        doc = docBuilder.newDocument();

        parser.parse(is, eleHandler);

        return doc;
    }


}