package net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.ConfigType;

public interface ConfigInterface {

    String getDocType();

    ValidationType getValidationType();

    ConfigType getConfigType();

    boolean isHardCoded();

    String getSample();
}
