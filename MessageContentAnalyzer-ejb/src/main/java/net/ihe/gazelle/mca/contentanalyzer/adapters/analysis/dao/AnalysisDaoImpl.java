package net.ihe.gazelle.mca.contentanalyzer.adapters.analysis.dao;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.interlay.dao.AbstractProcessingDaoImpl;
import net.ihe.gazelle.evsclient.interlay.dao.HandledObjectFile;
import net.ihe.gazelle.evsclient.interlay.dao.Repository;
import net.ihe.gazelle.evsclient.interlay.util.FileUtil;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.paths.HQLSafePathBasicDate;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.AnalysisDao;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.DecodedPartNotSavedException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.Analysis;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.util.Date;
import java.util.TimeZone;

public class AnalysisDaoImpl extends AbstractProcessingDaoImpl<Analysis> implements AnalysisDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnalysisDaoImpl.class);

    public AnalysisDaoImpl(EntityManagerFactory entityManagerFactory, ApplicationPreferenceManager applicationPreferenceManager) {
        super(entityManagerFactory, applicationPreferenceManager);
    }

    public void createDecodedPartFile(byte[] messageContent, String filePath) throws DecodedPartNotSavedException {
        try {
            FileUtil.createFileAndParentDirectories(messageContent, filePath);
        } catch (IOException e) {
            throw new DecodedPartNotSavedException(e);
        }

    }

    @Override
    public Repository getRepositoryType(Analysis processedObject) {
        return McaRepositoryTypes.MCA;
    }

    @Override
    protected void deleteSpecificItems(Analysis processing) throws UnexpectedProcessingException {
        //TODO delete all decodedPart Files
    }

    public Analysis getByParentAnalysisPart(AnalysisPart parentAnalysisPart) {
        AnalysisQuery analysisQuery = new AnalysisQuery(getEntityManager());
        analysisQuery.rootAnalysisPart().id().eq(parentAnalysisPart.getId());
        return analysisQuery.getUniqueResult();
    }

    public HQLCriterionsForFilter<Analysis> getCriterionForFilter(String login, String organization, boolean isMonitor, boolean isLoggedIn) {

        final AnalysisQuery analysisQuery = new AnalysisQuery();
        modifyQuery(analysisQuery, login, organization, isMonitor, isLoggedIn);
        final HQLCriterionsForFilter<Analysis> result = analysisQuery.getHQLCriterionsForFilter();
        final HQLSafePathBasicDate<Date> timestamp = analysisQuery.date();
        timestamp.setCriterionWithoutTime(TimeZone.getDefault());
        result.addPath("validation_date", timestamp);
        result.addPath("username", analysisQuery.owner().username());
        result.addPath("organization", analysisQuery.owner().organization());
        result.addPath("validation_types", analysisQuery.validationTypes());
        return result;
    }

    private void modifyQuery(AnalysisQuery analysisQuery, String login, String organization, boolean isMonitor, boolean isLoggedIn) {
        if (!isMonitor) {
            if (!isLoggedIn) {
                analysisQuery.addRestriction(HQLRestrictions
                        .or(HQLRestrictions.eq("sharing.isPrivate", null),
                                HQLRestrictions.eq("sharing.isPrivate", Boolean.FALSE)));
            } else {
                analysisQuery.addRestriction(HQLRestrictions
                        .or(HQLRestrictions.eq("sharing.isPrivate", null),
                                HQLRestrictions.eq("sharing.isPrivate", Boolean.FALSE),
                                HQLRestrictions.eq("owner.username", login),
                                HQLRestrictions.eq("owner.organization", organization)
                        ));
            }
        }
    }


    /**
     *
     * {@inheritDoc}
     *
     * @deprecated This method is exposing internal information of the DAO mechanism and is violating responsabilies.
     * However some legacy code are still coupled with the file mechanism (such as MCA) and must be refactored first
     * before deleting this method.
     */
    @Override
    @Deprecated
    public HandledObjectFile getObjectDAOImpl(Analysis analysis) {
        return getEntityManager().find(HandledObjectFile.class, analysis.getObject().getId());
    }
}
