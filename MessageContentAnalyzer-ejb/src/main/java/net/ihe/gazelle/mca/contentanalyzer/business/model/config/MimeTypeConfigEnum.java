package net.ihe.gazelle.mca.contentanalyzer.business.model.config;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.DocType;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.MimeTypeConfigInterface;

//This class corespond to hard coded mimetype detection
public enum MimeTypeConfigEnum implements MimeTypeConfigInterface {

    ZIP("application/zip", DocType.ZIP.getValue(), null);

    private final String mimeType;
    private final String docType;
    private final ValidationType validationType;
    private static final ConfigType configType = ConfigType.MIME_TYPE_CONFIG;

    MimeTypeConfigEnum(final String mimeType, final String docType, final ValidationType validationType) {
        this.mimeType = mimeType;
        this.docType = docType;
        this.validationType = validationType;
    }

    public String getDocType() {
        return docType;
    }

    public String getMimeType() {
        return mimeType;
    }

    public ValidationType getValidationType() {
        return validationType;
    }

    @Override
    public String getSample() {
        return null;
    }

    @Override
    public boolean isHardCoded() {
        return true;
    }

    @Override
    public ConfigType getConfigType() {
        return configType;
    }
}
