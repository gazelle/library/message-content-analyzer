package net.ihe.gazelle.mca.contentanalyzer.application.analyzers;

import net.ihe.gazelle.evsclient.interlay.gui.document.ContentConverter;
import net.ihe.gazelle.mca.contentanalyzer.business.exceptions.UnexpectedAnalysisException;
import net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart;
import net.ihe.gazelle.mca.contentanalyzer.business.model.DocType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MessageSplitter implements Analyzer {
    private static final long serialVersionUID = 4987046636834432201L;

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageSplitter.class);

    private static final String HTTP_FIRST_LINE_MATCH = "^[GET|HEAD|POST|PUT|DELETE|TRACE|CONNECT]{1}.*(HTTP/1.)[0-9]";
    private static final String MTOM_FIRST_LINE_MATCH = "^([ ])*--([^>].+)";
    private static final String PART_LOG_TYPE = "Message Splitting";

    private final Analyzer xmlAnalyzer;

    private Analyzer contentAnalysisTypeDetector;

    private final Analyzer mimeTypeDetector;

    private Analyzer b64Analyzer;
    private ContentConverter converter;

    public MessageSplitter(Analyzer xmlAnalyzer, Analyzer contentAnalysisTypeDetector,
                           Analyzer mimeTypeDetector, Analyzer b64Analyzer) {
        this.xmlAnalyzer = xmlAnalyzer;
        this.contentAnalysisTypeDetector = contentAnalysisTypeDetector;
        this.mimeTypeDetector = mimeTypeDetector;
        this.b64Analyzer = b64Analyzer;
        this.converter = new ContentConverter();
    }

    public void setContentAnalysisTypeDetector(Analyzer contentAnalysisTypeDetector) {
        this.contentAnalysisTypeDetector = contentAnalysisTypeDetector;
    }

    public void setB64Analyzer(Analyzer b64Analyzer) {
        this.b64Analyzer = b64Analyzer;
    }

    @Override
    public AnalysisPart analyze(final MessageContentAnalyzer mca, final AnalysisPart parent)
            throws UnexpectedAnalysisException {
        if (isSplittable(parent)) {
            split(mca, parent);
            return parent;
        }
        return null;
    }

    private boolean isSplittable(final AnalysisPart child) {

        LOGGER.info("Is {} document splittabe ?", child.getDocType());
        if (DocType.HTTP.getValue().equals(child.getDocType())
                || DocType.MTOM.getValue().equals(child.getDocType())
                || DocType.SYSLOG.getValue().equals(child.getDocType())) {
            child.addLog(PART_LOG_TYPE,child.getDocType() + " document is splittable");
            return true;
        }
        child.addLog(PART_LOG_TYPE,child.getDocType() + " document is not splittable");
        return false;
    }

    void split(final MessageContentAnalyzer mca, final AnalysisPart parent)
            throws UnexpectedAnalysisException{

        if (DocType.HTTP.getValue().equals(parent.getDocType())) {
            parent.addLog(PART_LOG_TYPE,"Split HTTP document");
            splitHttp(mca, parent);
        }
        else if (DocType.MTOM.getValue().equals(parent.getDocType())) {
            parent.addLog(PART_LOG_TYPE,"Split MTOM document");
            splitMTOM(mca, parent);
        } else if (DocType.SYSLOG.getValue().equals(parent.getDocType())) {
            parent.addLog(PART_LOG_TYPE,"Split SYSLOG document");
            splitSYSLOG(mca, parent);
        }
    }

    private int regexpOffset(String str, int strIndex) {
        return str.substring(0,strIndex).getBytes(StandardCharsets.UTF_8).length;
    }

    private void splitHttp(final MessageContentAnalyzer mca, final AnalysisPart parent)
            throws UnexpectedAnalysisException{

        LOGGER.info("Extract parts from http");
        String fileContent = new String(mca.getMessageByteContent(), StandardCharsets.UTF_8);

        final AnalysisPart headerFileToValidate = new AnalysisPart(parent);
        final AnalysisPart bodyFileToValidate = new AnalysisPart(parent);

        Pattern regex = Pattern.compile(HTTP_FIRST_LINE_MATCH);
        final Matcher regexMatcher = regex.matcher(fileContent);


        if (regexMatcher.find()) {

            final String httpReg = "(\\n\\n\\n)";

            regex = Pattern.compile(httpReg, Pattern.MULTILINE & Pattern.DOTALL);
            final Matcher regexMatcher2 = regex.matcher(fileContent);

            if (regexMatcher2.find()) {

                headerFileToValidate.setStartOffset(regexpOffset(fileContent,regexMatcher.start()) + parent.getStartOffset());
                headerFileToValidate.setEndOffset(regexpOffset(fileContent,regexMatcher2.end()) + parent.getStartOffset());
                headerFileToValidate.setDocType("Http Header");
                headerFileToValidate.setEncodedType(parent.getEncodedType());
                parent.addLog(PART_LOG_TYPE,"HTTP header detected in this part");
                parent.getChildPart().add(headerFileToValidate);

                LOGGER.info("Http header :\n{}", converter.strByOffsets(fileContent, headerFileToValidate.getStartOffset(),headerFileToValidate.getEndOffset()));


                bodyFileToValidate.setStartOffset(headerFileToValidate.getEndOffset());
                bodyFileToValidate.setEndOffset(parent.getEndOffset());

                if (bodyFileToValidate.getStartOffset() != bodyFileToValidate.getEndOffset()) {
                    bodyFileToValidate.setDocType("Http Body");
                    bodyFileToValidate.setEncodedType(parent.getEncodedType());
                    parent.addLog(PART_LOG_TYPE,"HTTP body detected in this part");
                    parent.getChildPart().add(bodyFileToValidate);

                    LOGGER.info("Http body :\n{}", converter.strByOffsets(fileContent, bodyFileToValidate.getStartOffset(),bodyFileToValidate.getEndOffset()));
                    parent.addLog(PART_LOG_TYPE,"Launch analysis of HTTP body sub-part");
                    bodyFileToValidate.addLog(PART_LOG_TYPE,"Start analysis as HTTP body sub-part");
                    final MessageContentAnalyzer messageContentAnalyzer =
                            new MessageContentAnalyzer(xmlAnalyzer, contentAnalysisTypeDetector, mimeTypeDetector, b64Analyzer);
                    messageContentAnalyzer.analyzeMessageContent(converter.toByteArray(fileContent,
                            bodyFileToValidate.getStartOffset(), bodyFileToValidate.getEndOffset()), bodyFileToValidate);
                }
            }
        }
    }

    private void splitMTOM(final MessageContentAnalyzer mca, final AnalysisPart parent)
            throws UnexpectedAnalysisException{

        int actualBegin, end, begin;
        String fileContent = new String(mca.getMessageByteContent(), StandardCharsets.UTF_8);
        String firstLineMatch;
        final ArrayList<Integer> offsetStartTab = new ArrayList<>();
        final ArrayList<Integer> offsetEndTab = new ArrayList<>();

        Pattern regex = Pattern.compile(MTOM_FIRST_LINE_MATCH);
        Matcher regexMatcher = regex.matcher(fileContent);
        if (regexMatcher.find()) {

            LOGGER.info("We got a first line match !");

            firstLineMatch = regexMatcher.group();

            final String mtomHeader = "(" + firstLineMatch + ")((\\n|\\r|\\r\\n| )*(((Content-.*?:(\\n|\\r|\\r\\n| )*.*(\\n|\\r|\\r\\n| )*))*)|--)";
            regex = Pattern.compile(mtomHeader, Pattern.MULTILINE);
            regexMatcher = regex.matcher(fileContent);
            while (regexMatcher.find()) {
                LOGGER.info("group : {} start :{} end :{}",
                        regexMatcher.group(), regexMatcher.start(), regexMatcher.end());

                offsetStartTab.add(regexpOffset(fileContent,regexMatcher.start()));
                if (regexMatcher.group().contains("Content-")) {
                    offsetEndTab.add(regexpOffset(fileContent,regexMatcher.end()));
                }
            }
            if (offsetEndTab.size() == offsetStartTab.size()) {
                parent.addLog(PART_LOG_TYPE,"Found " + (offsetStartTab.size()) + " sub-parts in MTOM part");
            } else {
                parent.addLog(PART_LOG_TYPE,"Found " + (offsetStartTab.size()-1) + " sub-parts in MTOM part");
            }
            for (int j = 0; j < offsetEndTab.size(); j++) {

                actualBegin = offsetStartTab.get(j);
                begin = offsetEndTab.get(j);
                if (offsetStartTab.size() <= j+1) {
                    end = fileContent.getBytes(StandardCharsets.UTF_8).length;
                } else {
                    end = offsetStartTab.get(j+1);
                }
                if (end - actualBegin > 0) {

                    final AnalysisPart mtomAnalysisPart = new AnalysisPart(parent);

                    mtomAnalysisPart.setDocType("MTOM part " + (j + 1));

                    mtomAnalysisPart.setStartOffset(begin + parent.getStartOffset());
                    mtomAnalysisPart.setEndOffset(end + parent.getStartOffset());
                    mtomAnalysisPart.setEncodedType(parent.getEncodedType());
                    parent.addLog(PART_LOG_TYPE,"Launch analysis of MTOM part " + (j+1));
                    final MessageContentAnalyzer messageContentAnalyzer =
                            new MessageContentAnalyzer(xmlAnalyzer, contentAnalysisTypeDetector, mimeTypeDetector, b64Analyzer);
                    messageContentAnalyzer.analyzeMessageContent(converter.toByteArray(fileContent,begin , end), mtomAnalysisPart);

                    parent.getChildPart().add(mtomAnalysisPart);
                    mtomAnalysisPart.setStartOffset(actualBegin + parent.getStartOffset());
                }
            }
        }
    }

    private void splitSYSLOG(final MessageContentAnalyzer mca, final AnalysisPart parent)
            throws UnexpectedAnalysisException{

        String fileContent = new String(mca.getMessageByteContent(), StandardCharsets.UTF_8);
        AnalysisPart syslogHeaderPart;
        AnalysisPart syslogStructuredDataPart;
        AnalysisPart syslogMessagePart;

        Pattern regex = Pattern.compile("<[0-9]{2,3}>1(\\s[^\\s]+){5}", Pattern.DOTALL);
        Matcher regexMatcher = regex.matcher(fileContent);
        Integer headerEndOffset = null;
        Integer structuredDateEndOffset = null;

        if (regexMatcher.find()) {
            syslogHeaderPart = new AnalysisPart(parent);
            syslogHeaderPart.setDocType("Syslog Header");
            headerEndOffset = regexMatcher.end();
            parent.addLog(PART_LOG_TYPE, "Syslog Header found.");

            syslogHeaderPart.setStartOffset(regexpOffset(fileContent,regexMatcher.start()) + parent.getStartOffset());
            syslogHeaderPart.setEndOffset(regexpOffset(fileContent,regexMatcher.end()) + parent.getStartOffset());
            syslogHeaderPart.setEncodedType(parent.getEncodedType());
            parent.getChildPart().add(syslogHeaderPart);

            regex = Pattern.compile("(\\[[^\\s]*(\\s[^\\s]+=[^\\s]+)*\\s*)|- ", Pattern.DOTALL);
            if (headerEndOffset == null) {
                headerEndOffset = 0;
            }
            String str =converter.strByOffsets(fileContent,headerEndOffset,fileContent.getBytes(StandardCharsets.UTF_8).length);
            regexMatcher = regex.matcher(str);

            if (regexMatcher.find()) {
                structuredDateEndOffset = regexpOffset(str,regexMatcher.end());

                if (!regexMatcher.group().equals("- ")) {
                    syslogStructuredDataPart = new AnalysisPart(parent);
                    syslogStructuredDataPart.setDocType("Syslog Structured Data");
                    parent.addLog(PART_LOG_TYPE, "Syslog Structured Data found.");

                    syslogStructuredDataPart.setStartOffset(headerEndOffset + regexpOffset(str,regexMatcher.start()) + parent.getStartOffset());
                    syslogStructuredDataPart.setEndOffset(headerEndOffset + regexpOffset(str,regexMatcher.end()) + parent.getStartOffset());
                    syslogStructuredDataPart.setEncodedType(parent.getEncodedType());
                    parent.getChildPart().add(syslogStructuredDataPart);
                }
            } else {
                structuredDateEndOffset = 0;
                parent.addLog(PART_LOG_TYPE, "No Syslog Structured Data found.");
            }

            syslogMessagePart = new AnalysisPart(parent);
            syslogMessagePart.setDocType("Syslog Message");

            syslogMessagePart.setStartOffset(headerEndOffset + structuredDateEndOffset + parent.getStartOffset());
            syslogMessagePart.setEndOffset(parent.getEndOffset());
            syslogMessagePart.setEncodedType(parent.getEncodedType());
            parent.getChildPart().add(syslogMessagePart);
            final MessageContentAnalyzer messageContentAnalyzer =
                    new MessageContentAnalyzer(xmlAnalyzer, contentAnalysisTypeDetector, mimeTypeDetector, b64Analyzer);
            messageContentAnalyzer.analyzeMessageContent(converter.toByteArray(fileContent,syslogMessagePart.getStartOffset(),
                    syslogMessagePart.getEndOffset()), syslogMessagePart);
        } else {
            parent.addLog(PART_LOG_TYPE, "No Syslog Header found.");
        }
    }
}
