package net.ihe.gazelle.mca.contentanalyzer.adapters.config.dao;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;
import net.ihe.gazelle.mca.contentanalyzer.application.interfaces.McaConfigDao;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.*;
import net.ihe.gazelle.mca.contentanalyzer.business.model.config.interfaces.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class McaConfigDaoImpl implements McaConfigDao {
    private static final long serialVersionUID = -3382974977138964087L;

    private transient EntityManagerFactory entityManagerFactory;

    public McaConfigDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public List<MimeTypeConfigInterface> getAllMimeTypeConfig() {
        MimeTypeConfigEntityQuery mimeTypeConfigEntityQuery = new MimeTypeConfigEntityQuery(entityManagerFactory.createEntityManager());
        ArrayList<MimeTypeConfigInterface> configList  = new ArrayList<MimeTypeConfigInterface>(mimeTypeConfigEntityQuery.getList());
        configList.addAll(Arrays.asList(MimeTypeConfigEnum.values()));
        return configList;
    }

    @Override
    public List<ContentAnalysisConfigInterface> getAllContentAnalysisConfig() {
        ContentAnalysisConfigEntityQuery contentAnalysisConfigEntityQuery = new ContentAnalysisConfigEntityQuery(entityManagerFactory.createEntityManager());
        ArrayList<ContentAnalysisConfigInterface> configList  = new ArrayList<ContentAnalysisConfigInterface>(contentAnalysisConfigEntityQuery.getList());
        configList.addAll(Arrays.asList(ContentAnalysisConfigEnum.values()));
        return configList;
    }

    @Override
    public List<XmlTagConfigInterface> getAllTags() {
        XmlTagConfigEntityQuery xmlTagConfigEntityQuery = new XmlTagConfigEntityQuery(entityManagerFactory.createEntityManager());
        return new ArrayList<XmlTagConfigInterface>(xmlTagConfigEntityQuery.getList());
    }

    @Override
    public List<MimeTypeConfigInterface> getMimeTypeConfigWithValidationType(ValidationType validationType) {
        MimeTypeConfigEntityQuery mimeTypeConfigEntityQuery = new MimeTypeConfigEntityQuery(entityManagerFactory.createEntityManager());
        mimeTypeConfigEntityQuery.addRestriction(
                HQLRestrictions.eq("validationType", validationType));
        ArrayList<MimeTypeConfigInterface> configList  = new ArrayList<MimeTypeConfigInterface>(mimeTypeConfigEntityQuery.getList());
        for (MimeTypeConfigEnum mimeTypeConfigEnum :
                MimeTypeConfigEnum.values()) {
            if (mimeTypeConfigEnum.getValidationType() == validationType) {
                configList.add(mimeTypeConfigEnum);
            }
        }
        return configList;
    }

    @Override
    public List<ContentAnalysisConfigInterface> getContentAnalysisTypeDetectionConfigWithValidationType
            (ValidationType validationType) {
        ContentAnalysisConfigEntityQuery contentAnalysisConfigEntityQuery = new ContentAnalysisConfigEntityQuery(entityManagerFactory.createEntityManager());
        contentAnalysisConfigEntityQuery.addRestriction(
                HQLRestrictions.eq("validationType", validationType));
        ArrayList<ContentAnalysisConfigInterface> configList  = new ArrayList<ContentAnalysisConfigInterface>(contentAnalysisConfigEntityQuery.getList());
        for (ContentAnalysisConfigEnum contentAnalysisConfigEnum :
                ContentAnalysisConfigEnum.values()) {
            if (contentAnalysisConfigEnum.getValidationType() == validationType) {
                configList.add(contentAnalysisConfigEnum);
            }
        }
        return configList;
    }

    @Override
    public List<XmlTagConfigInterface> getXmlTagsWithValidationType(ValidationType validationType) {
        XmlTagConfigEntityQuery xmlTagConfigEntityQuery = new XmlTagConfigEntityQuery(entityManagerFactory.createEntityManager());
        xmlTagConfigEntityQuery.addRestriction(
                HQLRestrictions.eq("validationType", validationType));
        return new ArrayList<XmlTagConfigInterface>(xmlTagConfigEntityQuery.getList());
    }

    @Override
    public List<ZipStructureInterface> getZipStructures() {
        ZipStructureQuery zipStructureQuery = new ZipStructureQuery(entityManagerFactory.createEntityManager());
        return new ArrayList<ZipStructureInterface>(zipStructureQuery.getList());
    }

    @Override
    public ZipStructureInterface getZipStructureWithDocType(String docType) {
        ZipStructureQuery zipStructureQuery = new ZipStructureQuery(entityManagerFactory.createEntityManager());
        zipStructureQuery.docType().eq(docType);
        return zipStructureQuery.getUniqueResult();
    }

    @Override
    public ConfigsFromDB getConfigsFromDB() {

        ConfigsFromDB configsFromDB = new ConfigsFromDB();

        XmlTagConfigEntityQuery xmlTagConfigEntityQuery = new XmlTagConfigEntityQuery(entityManagerFactory.createEntityManager());
        configsFromDB.setXmlTagConfigEntities(xmlTagConfigEntityQuery.getList());

        ContentAnalysisConfigEntityQuery contentAnalysisConfigEntityQuery = new ContentAnalysisConfigEntityQuery(entityManagerFactory.createEntityManager());
        configsFromDB.setContentAnalysisConfigEntities(contentAnalysisConfigEntityQuery.getList());

        MimeTypeConfigEntityQuery mimeTypeConfigEntityQuery = new MimeTypeConfigEntityQuery(entityManagerFactory.createEntityManager());
        configsFromDB.setMimeTypeConfigEntities(mimeTypeConfigEntityQuery.getList());
        return configsFromDB;
    }

    @Override
    public int addNewImportedConfiguration(ConfigsFromDB configsToImport,
                                           ConfigsFromDB overwrittenConfigs) {

        ConfigsFromDB configsFromDB = getConfigsFromDB();
        boolean doMerge;
        int nbrAddedConfigs = 0;

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        if (configsToImport.getXmlTagConfigEntities() != null) {
            for (XmlTagConfigEntity importedXmlTagConfigEntity : configsToImport.getXmlTagConfigEntities()) {
                doMerge = true;
                int i = 0;
                while (i < configsFromDB.getXmlTagConfigEntities().size() && doMerge) {
                    if (configsFromDB.getXmlTagConfigEntities().get(i).hasSameDetectionElements(importedXmlTagConfigEntity)) {
                        if (importedXmlTagConfigEntity.getDocType()!= null
                            && (!importedXmlTagConfigEntity.getDocType().equals(configsFromDB.getXmlTagConfigEntities().get(i).getDocType()) ||
                                    (importedXmlTagConfigEntity.getValidationType() == null) !=
                                            (configsFromDB.getXmlTagConfigEntities().get(i).getValidationType() == null) ||
                                    (importedXmlTagConfigEntity.getValidationType() != null &&
                                            configsFromDB.getXmlTagConfigEntities().get(i).getValidationType() != null &&
                                            !importedXmlTagConfigEntity.getValidationType().equals(configsFromDB.getXmlTagConfigEntities().get(i).getValidationType())))) {
                                configsFromDB.getXmlTagConfigEntities().get(i).setValidationType(importedXmlTagConfigEntity.getValidationType());
                                configsFromDB.getXmlTagConfigEntities().get(i).setDocType(importedXmlTagConfigEntity.getDocType());
                                entityManager.merge(configsFromDB.getXmlTagConfigEntities().get(i));
                                overwrittenConfigs.getXmlTagConfigEntities().add(configsFromDB.getXmlTagConfigEntities().get(i));
                        }
                        doMerge = false;
                    }
                    i++;
                }
                if (doMerge) {
                    entityManager.merge(importedXmlTagConfigEntity);
                    nbrAddedConfigs ++;
                }
            }
            entityManager.flush();
        }

        if (configsToImport.getContentAnalysisConfigEntities() != null) {
            for (ContentAnalysisConfigEntity importedContentAnalysisConfigEntity : configsToImport.getContentAnalysisConfigEntities()) {
                doMerge = true;
                int i = 0;
                while (i < configsFromDB.getContentAnalysisConfigEntities().size() && doMerge) {
                    if (configsFromDB.getContentAnalysisConfigEntities().get(i).match(importedContentAnalysisConfigEntity.getStartsWith(),
                            importedContentAnalysisConfigEntity.getBytePattern(), importedContentAnalysisConfigEntity.getStringPattern(),
                            importedContentAnalysisConfigEntity.getUnwantedContent())) {
                        if (!importedContentAnalysisConfigEntity.getDocType().equals(configsFromDB.getContentAnalysisConfigEntities().get(i).getDocType()) ||
                                (importedContentAnalysisConfigEntity.getValidationType() == null) !=
                                        (configsFromDB.getContentAnalysisConfigEntities().get(i).getValidationType() == null) ||
                                (importedContentAnalysisConfigEntity.getValidationType() != null &&
                                        configsFromDB.getContentAnalysisConfigEntities().get(i).getValidationType() != null &&
                                        !importedContentAnalysisConfigEntity.getValidationType().equals(configsFromDB.getContentAnalysisConfigEntities().get(i).getValidationType()))) {
                            configsFromDB.getContentAnalysisConfigEntities().get(i)
                                    .setValidationType(importedContentAnalysisConfigEntity.getValidationType());
                            configsFromDB.getContentAnalysisConfigEntities().get(i)
                                    .setDocType(importedContentAnalysisConfigEntity.getDocType());
                            entityManager.merge(configsFromDB.getContentAnalysisConfigEntities().get(i));
                            overwrittenConfigs.getContentAnalysisConfigEntities().add(configsFromDB.getContentAnalysisConfigEntities().get(i));
                        }
                        doMerge = false;
                    }
                    i++;
                }
                if (doMerge) {
                    entityManager.merge(importedContentAnalysisConfigEntity);
                    nbrAddedConfigs ++;
                }
            }
            entityManager.flush();
        }

        if (configsToImport.getMimeTypeConfigEntities() != null) {
            for (MimeTypeConfigEntity importedMimeTypeConfigEntity : configsToImport.getMimeTypeConfigEntities()) {
                doMerge = true;
                int i = 0;
                while (i < configsFromDB.getMimeTypeConfigEntities().size() && doMerge) {
                    if (importedMimeTypeConfigEntity.getMimeType() != null &&
                            configsFromDB.getMimeTypeConfigEntities().get(i).getMimeType() != null &&
                            importedMimeTypeConfigEntity.getMimeType().equals(configsFromDB.getMimeTypeConfigEntities().get(i).getMimeType())) {
                        if (!importedMimeTypeConfigEntity.getDocType().equals(configsFromDB.getMimeTypeConfigEntities().get(i).getDocType()) ||
                                (importedMimeTypeConfigEntity.getValidationType() == null) !=
                                        (configsFromDB.getMimeTypeConfigEntities().get(i).getValidationType() == null) ||
                                (importedMimeTypeConfigEntity.getValidationType() != null &&
                                        configsFromDB.getMimeTypeConfigEntities().get(i).getValidationType() != null &&
                                        !importedMimeTypeConfigEntity.getValidationType().equals(configsFromDB.getMimeTypeConfigEntities().get(i).getValidationType()))) {
                            configsFromDB.getMimeTypeConfigEntities().get(i)
                                    .setValidationType(importedMimeTypeConfigEntity.getValidationType());
                            configsFromDB.getMimeTypeConfigEntities().get(i)
                                    .setDocType(importedMimeTypeConfigEntity.getDocType());
                            entityManager.merge(configsFromDB.getMimeTypeConfigEntities().get(i));
                            overwrittenConfigs.getMimeTypeConfigEntities().add(configsFromDB.getMimeTypeConfigEntities().get(i));
                        }
                        doMerge = false;
                    }
                    i++;
                }
                if (doMerge) {
                    entityManager.merge(importedMimeTypeConfigEntity);
                    nbrAddedConfigs ++;
                }
            }
            entityManager.flush();
        }
        return nbrAddedConfigs;
    }

    @Override
    public int removeAllConfigsFromDB() {

        int nbrRemovedConfigs = 0;

        ConfigsFromDB configsFromDB = getConfigsFromDB();
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        for (XmlTagConfigEntity xmlTagConfigEntity : configsFromDB.getXmlTagConfigEntities()) {
            entityManager.remove(entityManager.merge(xmlTagConfigEntity));
            nbrRemovedConfigs ++;
        }
        entityManager.flush();

        for (ContentAnalysisConfigEntity contentAnalysisConfigEntity : configsFromDB.getContentAnalysisConfigEntities()) {
            entityManager.remove(entityManager.merge(contentAnalysisConfigEntity));
            nbrRemovedConfigs ++;
        }
        entityManager.flush();

        for (MimeTypeConfigEntity mimeTypeConfigEntity : configsFromDB.getMimeTypeConfigEntities()) {
            entityManager.remove(entityManager.merge(mimeTypeConfigEntity));
            nbrRemovedConfigs ++;
        }
        entityManager.flush();

        return nbrRemovedConfigs;
    }

    @Override
    public int addAllImportedConfigurationsToDB(ConfigsFromDB configsToImport) {

        int nbrPersistedConfig = 0;

        EntityManager entityManager = entityManagerFactory.createEntityManager();

        if (configsToImport.getXmlTagConfigEntities() != null) {
            for (XmlTagConfigEntity importedXmlTagConfigEntity : configsToImport.getXmlTagConfigEntities()) {
                entityManager.merge(importedXmlTagConfigEntity);
                nbrPersistedConfig++;
            }
            entityManager.flush();
        }

        if (configsToImport.getContentAnalysisConfigEntities() != null) {
            for (ContentAnalysisConfigEntity importedContentAnalysisConfigEntity : configsToImport.getContentAnalysisConfigEntities()) {
                entityManager.merge(importedContentAnalysisConfigEntity);
                nbrPersistedConfig++;
            }
            entityManager.flush();
        }

        if (configsToImport.getMimeTypeConfigEntities() != null) {
            for (MimeTypeConfigEntity importedMimeTypeConfigEntity : configsToImport.getMimeTypeConfigEntities()) {
                entityManager.merge(importedMimeTypeConfigEntity);
                nbrPersistedConfig++;
            }
            entityManager.flush();
        }
        return nbrPersistedConfig;
    }

    @Override
    public void remove(ConfigInterface selectedConfig) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.remove(entityManager.merge(selectedConfig));
        entityManager.flush();
    }

    @Override
    public void merge(ConfigInterface selectedConfig) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.merge(selectedConfig);
        entityManager.flush();
    }
}
