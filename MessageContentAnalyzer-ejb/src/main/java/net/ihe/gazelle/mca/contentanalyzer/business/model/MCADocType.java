package net.ihe.gazelle.mca.contentanalyzer.business.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "mca_doc_type", schema = "public")
@SequenceGenerator(name = "mca_doc_type_sequence", sequenceName = "mca_doc_type_seq", allocationSize = 1)
public class MCADocType {

    @Id
    @NotNull
    @GeneratedValue(generator = "mca_doc_type_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "name")
    private String name;

    public MCADocType() {
    }

    public MCADocType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
