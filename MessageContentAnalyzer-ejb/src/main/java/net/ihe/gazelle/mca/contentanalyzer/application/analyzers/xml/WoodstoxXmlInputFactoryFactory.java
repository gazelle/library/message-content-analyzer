package net.ihe.gazelle.mca.contentanalyzer.application.analyzers.xml;

import com.ctc.wstx.api.WstxInputProperties;
import com.ctc.wstx.stax.WstxInputFactory;

import javax.xml.stream.XMLInputFactory;

public final class WoodstoxXmlInputFactoryFactory implements XmlInputFactoryFactory {

    /**
     * Sets the properties of an XMLInputFactory and returns it to create a XML parser
     * @return xmlInputFactory : configured factory to use on XML files with large texts
     */
    public XMLInputFactory configureAndGetXmlInputFactory() {
        XMLInputFactory xmlInputFactory = new WstxInputFactory();
        xmlInputFactory.setProperty(WstxInputProperties.P_MIN_TEXT_SEGMENT, Integer.MAX_VALUE);
        return xmlInputFactory;
    }
}
