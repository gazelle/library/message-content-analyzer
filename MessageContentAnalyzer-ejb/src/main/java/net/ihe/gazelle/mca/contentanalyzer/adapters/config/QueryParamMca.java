package net.ihe.gazelle.mca.contentanalyzer.adapters.config;

import net.ihe.gazelle.evsclient.interlay.gui.QueryParam;

public class QueryParamMca extends QueryParam {

    public static final String START_OFFSET = "startOffset";
    public static final String END_OFFSET = "endOffset";
    public static final String FILE_PATH = "filePath";
    public static final String VALIDATION_TYPE = "validationType";
    public static final String MESSAGE_CONTENT_ANALYZER_OID = "oid";

    protected QueryParamMca() {
        super();
    }
}
